clearListener()
gc()

fn saveMeshToFile obj vtxs = ( 
	--Determine the path of the file
	local outpath = "..\\Engine\\data\\meshes\\"
	
	-- Determinar el nombre del fichero
	local filename = outpath + obj.name + ".mesh"
	
	--Check if the directory exist
	if not doesFileExist outpath then(
		dosCommand ("mkdir " +  outpath)
	)
	
	-- Abrir el fichero en modo binario
	local f = fopen filename "wb"
	if f == undefined then throw ( "Can't create output mesh file " + filename )
	
	local num_faces = getNumFaces obj
	
	-- Cada float ocupa 4 bytes. Cojo el 1er vtx como referencia
	-- de cuantos attributos tiene cada vertice de esta malla
	local bytes_per_vtx = vtxs[ 1 ].count * 4
	
	-- Escribir una cabecera
	WriteLong f 0x33887755	-- Magic 
	WriteLong f 1			-- version number
	WriteLong f vtxs.count	-- # vertices
	WriteLong f num_faces	-- # faces
	WriteLong f 0        	-- # indices
	WriteLong f 1000       	-- tipo de vertice (empezamos en 1000)
	WriteLong f bytes_per_vtx -- # bytes_per_vertex
	WriteLong f 2  			-- # bytes_per_index
		-- AABB de la malla
		-- # de submateriales
		-- informacion de cada submaterial
		-- # de bones de la malla
		-- bytes para los bones
	WriteLong f 0x33887755	-- Magic 
	
	-- Escribir indices
	-- Escribir vertices
	for v in vtxs do (
		for c in v do (
			writeFloat f c
		)
	)
	
	-- Cerrar el fichero
	fclose f
)


fn exportMesh orig_obj = (
	local obj = orig_obj
	
	-- Comprobar si el objecto es tipo de editableMesh?
	if classof orig_obj != Editable_mesh then (
		-- Si no lo es, hacer una copia y convertirlo a editable mesh.
		obj = copy orig_obj
		convertToMesh obj
		obj.name = orig_obj.name
	)
	
	
	-- En este punto sabemos que tenemos un editable mesh en obj
	local channelA = 1
	local all_vtxs = #()
	
	-- Un bucle por el # de caras.
	local num_faces = getNumFaces obj
	local fid
	local faceculling = #(1, 3, 2 )
	for fid = 1 to num_faces do (
	
		-- Pedir la face para las posiciones.  = [4,1,7]
		local face = getFace obj fid
	
		-- Pedir la face para el mapChannel A
		local fmapA = meshop.getMapFace obj channelA fid
	
		-- Para los 3 vertices de la cara
		for item in faceCulling do (
			--Calculate the local coordinates of the object
			local inverse_world = inverse(obj.transform)
			obj.transform = obj.transform * inverse_world
			
			--Convert the coordinate system form the max to directx
			newRot = obj.rotation.x_rotation - 90
			obj.rotation.x_rotation = newRot
			
			-- La posicion x,y,z
			local pos = getVert obj face[ item ]
	   
			-- El 1er juego de coordenadas de textura
			local uvA = meshop.getMapVert obj channelA fmapA[ item ]
	   
			-- ... otras cosas a exportar por vertice ...
			
			-- Construir un vertice completo
			local v = #( pos.x, pos.y, pos.z, uvA.x, 1 - uvA.y )
			
			-- A�adirlo como vertice a la lista de vertices 
			-- ( Cuando indexemos lo haremos de otro modo )
			append all_vtxs v
		)
	)

	-- 
	saveMeshToFile obj all_vtxs

	-- Si hemos hecho una copia, borrarla..
	if obj != orig_obj then (
		delete obj
	)
)

exportMesh $