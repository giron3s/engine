#include "McvPlatform.h"
#include "Life.h"

void CLife::setLife(float life){
	this->life = life;
}

void CLife::applyDamage(float damage){
	this->life -= damage;
}
