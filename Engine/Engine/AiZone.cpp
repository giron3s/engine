#include "McvPlatform.h"
#include "bt.h"
#include "AiZone.h"

/**
  * Add the controller to the zone
  */

void CAiZone::addController(bt *aiController){
    aiControllers.push_back(aiController);
}


/**
  * Update the whole controllers into zone
  */
void CAiZone::update(float delta){
    for( size_t i = 0; i < aiControllers.size(); i++){
        bt * aiCtrl = (bt *)aiControllers.at( i );

        if(aiCtrl->isActive()){
            aiCtrl->recalc(delta);
        }
    }
}

/* 
 *  Destroy the ai controllers
 */
void CAiZone::destroy(){
    for ( size_t i = 0; i < aiControllers.size(); i++ ) {
      delete aiControllers[i];
   }
   aiControllers.clear();
}
