#ifndef INC_UTILS_H_
#define INC_UTILS_H_

const bool DEBUG = true;

bool isDebug();
void dbg( const char *fmt, ... );
bool isKeyPressed( int key );

int getRandomInt(int min, int max);

typedef unsigned char u8;

#endif
