#include "McvPlatform.h"
#include "DrawUtils.h"
#include "Mesh.h"
#include "VertexDeclarations.h"
#include "Angular.h"
#include "Camera.h"

ID3D11Buffer*           g_pConstantBufferCamera = NULL;
ID3D11Buffer*           g_pConstantBufferObject = NULL;

namespace DrawUtils {

	bool createConstantBuffers()  {
		HRESULT hr;
		// Create the constant buffers
		// One for the camera parameters
		D3D11_BUFFER_DESC bd;
		ZeroMemory( &bd, sizeof(bd) );
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(ConstantBufferCamera);
		bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bd.CPUAccessFlags = 0;
		hr = App.d3dDevice->CreateBuffer( &bd, NULL, &g_pConstantBufferCamera );
		if( FAILED( hr ) ){
			return false;
		}
		// Another for the object parameters
		ZeroMemory( &bd, sizeof(bd) );
		bd.Usage = D3D11_USAGE_DEFAULT;  
		bd.ByteWidth = sizeof(ConstantBufferObject);
		bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bd.CPUAccessFlags = 0;
		hr = App.d3dDevice->CreateBuffer( &bd, NULL, &g_pConstantBufferObject );
		if( FAILED( hr ) ){
			return false;
		}
		return true;
	}

	CRenderMesh* createPerceiveArea(float hearing_radius, float vision_angle, float vision_distance){
		//Declare the variables
		int num_vision_points = 8, num_hearing_points = 9;
		int nindices = num_vision_points + (num_hearing_points * 2); // 8 vertex the vision angle & 8 vertex hearing_radius
		CVertexSolid *vs = new CVertexSolid[nindices];
		unsigned short *idxs = new unsigned short[nindices];
		CVertexSolid *iterator = vs;
		float half_vision_angle = vision_angle / 2;

		//Hearing area
		float wedge_angle = XM_2PI / ((float)num_hearing_points - 1.f);
		for(int i = 0; i < num_hearing_points; i++){

			float theta = i * wedge_angle;
			iterator->Pos = XMFLOAT3( hearing_radius * cos(theta) ,  0.f, -hearing_radius * sin(theta) );
			iterator->Color = XMFLOAT4(1.f, 0.f, 1.f, 0.f);
			iterator++;

			theta = (i  + 1 ) * wedge_angle;
			iterator->Pos = XMFLOAT3( hearing_radius * cos(theta) , 0.f , -hearing_radius * sin(theta) );
			iterator->Color = XMFLOAT4(1.f, 0.f, 1.f, 0.f);
			iterator++;
		}

		//HEAD
		iterator->Pos = XMFLOAT3( 0.f, 0.f, 0.f );
		iterator->Color = XMFLOAT4( 1.f, 0.f, 1.f, 0.f );
		iterator++;
		iterator->Pos =  XMFLOAT3( 0.f, 1.f, 0.f );
		iterator->Color = XMFLOAT4( 1.f, 0.f, 1.f, 0.f );

		//Vision angle - Right side
		iterator++;
		iterator->Pos = XMFLOAT3( 0.f, 0.f, 0.f );
		iterator->Color = XMFLOAT4( 1.f, 0.f, 1.f, 0.f );
		iterator++;
		iterator->Pos = XMFLOAT3( vision_distance * sin ( deg2rad (half_vision_angle)) ,  0.f,  vision_distance * cos( deg2rad (half_vision_angle)) );
		iterator->Color = XMFLOAT4( 1.f, 0.f, 1.f, 0.f );
		iterator++;
		//Vision angle - Left side
		iterator->Pos = XMFLOAT3( 0.f, 0.f, 0.f );
		iterator->Color = XMFLOAT4( 1.f, 0.f, 1.f, 0.f );
		iterator++;
		iterator->Pos =  XMFLOAT3( vision_distance * sin ( deg2rad (-half_vision_angle)) ,  0.f,  vision_distance * cos( deg2rad (-half_vision_angle)) );
		iterator->Color = XMFLOAT4( 1.f, 0.f, 1.f, 0.f );
		iterator++;
		//Vision angle - Middle side
		iterator->Pos =  XMFLOAT3( vision_distance * sin ( deg2rad (half_vision_angle)) ,  0.f,  vision_distance * cos( deg2rad (half_vision_angle)));
		iterator->Color = XMFLOAT4( 1.f, 0.f, 1.f, 0.f );
		iterator++;
		iterator->Pos =  XMFLOAT3( vision_distance * sin ( deg2rad (-half_vision_angle)) ,  0.f,  vision_distance * cos( -deg2rad (half_vision_angle)) );
		iterator->Color = XMFLOAT4( 1.f, 0.f, 1.f, 0.f );

		//Indixes
		for( int i=0; i < nindices; ++i )
			idxs[ i ] = i;


		// Llamar a m.createmesh( .. )
		CRenderMesh *mesh = new CRenderMesh;
		bool is_ok = mesh->createMesh( vs, nindices, idxs, nindices, CRenderMesh::LINE_LIST);

		// Liberar la memoria de c++
		delete [] idxs;
		delete [] vs;

		return mesh;
	}

	CRenderMesh* createCube(float size){

		XMFLOAT4 colorOther = XMFLOAT4(1.0f,1.0f,0.0f,1.0f);
		XMFLOAT4 colorFront = XMFLOAT4(1.0f,1.0f,0.0f,1.0f);
		CVertexSolid verticesCubo[] = {
			//vertices base
			{ XMFLOAT3( -size/2, 0.0f, -size/2 ), colorOther },
			{ XMFLOAT3(  size/2, 0.0f, -size/2 ), colorOther },
			{ XMFLOAT3(  size/2, 0.0f,  size/2 ), colorFront },
			{ XMFLOAT3( -size/2, 0.0f,  size/2 ), colorFront },

			//vertices top
			{ XMFLOAT3( -size/2, size, -size/2 ), colorOther },
			{ XMFLOAT3(  size/2, size, -size/2 ), colorOther },
			{ XMFLOAT3(  size/2, size,  size/2 ), colorFront },
			{ XMFLOAT3( -size/2, size,  size/2 ), colorFront }
		};

		CRenderMesh::TIndex indicesCubo[] = { 0,2,1, 0,3,2, 0,5,4, 0,1,5, 1,2,6, 1,6,5, 2,7,6, 2,3,7, 0,7,3, 0,4,7, 4,5,7, 5,6,7 };
		CRenderMesh *mesh = new CRenderMesh;
		mesh->createMesh( verticesCubo, 8, indicesCubo, 3 * 12, CRenderMesh::TRIANGLE_LIST );
		return mesh;
	}

	CRenderMesh* createCircle(float radious){
		CRenderMesh* circle = new CRenderMesh;
		int nvertex = 15;
		float deltaAngle = 2*XM_PI / nvertex;
		CVertexSolid *vertices = new CVertexSolid[nvertex];
		CVertexSolid *vIterator = vertices;

		for(float currentAng=0; currentAng<2*XM_PI; currentAng += deltaAngle){

			vIterator->Pos = XMFLOAT3(sin(currentAng) * radious, 0.f,cos(currentAng) * radious);
			vIterator->Color = XMFLOAT4(1.0f,1.0f,0.0f,1.0f);;
			vIterator++;

		}

		// Crear un index buffer con datos 0,1,2,3... nvtxs
		int nindx = nvertex + 1;
		CRenderMesh::TIndex *idxs = new CRenderMesh::TIndex[ nindx]; //+1 para cerrar el ultimo segmento
		for( int i=0; i<nvertex; ++i ){
			idxs[ i ] = i;
		}
		//a�adimos el indice del primer vertice para cerrar el circulo
		idxs[ nvertex ] = 0;

		bool is_ok = circle->createMesh( vertices, nvertex, idxs, nindx, CRenderMesh::LINE_STRIP );

		//FIX:
		delete [] vertices;
		delete [] idxs;

		return circle;
	}


	CRenderMesh* createFrustum(float size){
		XMFLOAT4 color = XMFLOAT4( 1.0f, 1.0f, 1.0f, 1.0f );
		CVertexSolid verticesCubo[] = {
			//vertices near
			{ XMFLOAT3( -size, -size, 0.f ), color },
			{ XMFLOAT3(  size, -size, 0.f ), color },
			{ XMFLOAT3(  size,  size, 0.f ), color },
			{ XMFLOAT3( -size,  size, 0.f ), color },

			//vertices far
			{ XMFLOAT3( -size, -size, size ), color },
			{ XMFLOAT3(  size, -size, size ), color },
			{ XMFLOAT3(  size,  size, size ), color },
			{ XMFLOAT3( -size,  size, size ), color }

		};

		CRenderMesh::TIndex indicesCubo[] = { 0,1, 1,2, 2,3, 3,0, 4,5, 5,6, 6,7, 7,4, 0,4, 1,5, 2,6, 3,7};
		CRenderMesh *mesh = new CRenderMesh;
		bool is_ok = mesh->createMesh( verticesCubo, 8, indicesCubo, 2 * 12, CRenderMesh::LINE_LIST );
		return mesh;
	}


	CRenderMesh* createGrid(int nbigs, int nsmalls ) {

		// Calcular # de vertices que voy a necesitar
		int n_per_half_side = nbigs * nsmalls;
		int n_per_side = n_per_half_side * 2 + 1;
		int nvertexs = n_per_side * 4;
		int nindices = nvertexs;

		// Pedir la memoria
		CVertexSolid *vs = new CVertexSolid[ nvertexs ];

		XMFLOAT4 Color2( 0.3f, 0.3f, 0.3f, 0.3f );
		XMFLOAT4 Color1( 0.5f, 0.5f, 0.5f, 0.5f );

		CVertexSolid *v = vs;
		// Generar lineas cada 2 vertices
		for( int i=-n_per_half_side; i<=n_per_half_side; ++i ) {
			XMFLOAT4 clr = ( i % nsmalls ) == 0 ? Color1 : Color2;
			v->Color = clr; v->Pos = XMFLOAT3( i, 0,  n_per_half_side ); ++v;
			v->Color = clr; v->Pos = XMFLOAT3( i, 0, -n_per_half_side ); ++v;
			v->Color = clr; v->Pos = XMFLOAT3(  n_per_half_side, 0, i ); ++v;
			v->Color = clr; v->Pos = XMFLOAT3( -n_per_half_side, 0, i ); ++v;
		}

		// Crear un index buffer con datos 0,1,2,3... nvtxs
		CRenderMesh::TIndex *idxs = new CRenderMesh::TIndex[ nindices ];
		for( int i=0; i<nindices; ++i )
			idxs[ i ] = i;

		// Llamar a m.createVertex( .. )
		CRenderMesh *mesh = new CRenderMesh;
		mesh->createMesh( vs, nvertexs, idxs, nindices, CRenderMesh::LINE_LIST );

		// Liberar la memoria de c++
		delete [] idxs;
		delete [] vs;

		return mesh;
	}

	CRenderMesh* createAxis(){
		CVertexSolid vertices[] = {
			{ XMFLOAT3( 0.0f, 0.0f, 0.0f ), XMFLOAT4( 1.0f, 0.0f, 0.0f, 1.0f ) },
			{ XMFLOAT3( 1.0f, 0.0f, 0.0f ), XMFLOAT4( 1.0f, 0.0f, 0.0f, 1.0f ) },

			{ XMFLOAT3( 0.0f, 0.0f, 0.0f ), XMFLOAT4( 0.0f, 1.0f, 0.0f, 1.0f ) },
			{ XMFLOAT3( 0.0f, 2.0f, 0.0f ), XMFLOAT4( 0.0f, 1.0f, 0.0f, 1.0f ) },

			{ XMFLOAT3( 0.0f, 0.0f, 0.0f ), XMFLOAT4( 0.0f, 0.0f, 1.0f, 1.0f ) },
			{ XMFLOAT3( 0.0f, 0.0f, 3.0f ), XMFLOAT4( 0.0f, 0.0f, 1.0f, 1.0f ) },
		};
		WORD indices[] = { 0,1,2, 3,4,5, };

		CRenderMesh *mesh = new CRenderMesh;
		mesh->createMesh(vertices, 6, indices, 6, CRenderMesh::LINE_LIST);
		return mesh;
	}

	void destroyDrawUtils( ) {
		if( g_pConstantBufferCamera ) { g_pConstantBufferCamera->Release(); }
		if( g_pConstantBufferObject ) { g_pConstantBufferObject->Release(); }
	}

	void drawFrustum( const CCamera &frustumCam ){

		/*XMVECTOR determinant;
		XMMATRIX frustum_cam_world =  XMMatrixInverse( &determinant , frustumCam.getViewProjection() );
		setWorldMatrix(frustum_cam_world);
		mesh_frustum.render();*/
	}

	void activateCamera( const CCamera &camera ) {
		ConstantBufferCamera cb;
		cb.mView = XMMatrixTranspose( camera.getView() );
		cb.mProjection = XMMatrixTranspose( camera.getProjection());
		App.immediateContext->UpdateSubresource( g_pConstantBufferCamera, 0, NULL, &cb, 0, 0 );
	}

	void setWorldMatrix( const XMMATRIX new_world ) {
		ConstantBufferObject cb;
		cb.mWorld = XMMatrixTranspose( new_world );
		App.immediateContext->UpdateSubresource( g_pConstantBufferObject, 0, NULL, &cb, 0, 0 );
	}

	void activateDefaultConstantBuffers( ) {
		// el primer argumento 0 y 1 tiene que coincidir con el 
		// : register( b0 ) y : register( b1 ) de los shaders!!!!
		App.immediateContext->VSSetConstantBuffers( 0, 1, &g_pConstantBufferCamera );
		App.immediateContext->VSSetConstantBuffers( 1, 1, &g_pConstantBufferObject );
	}
}

