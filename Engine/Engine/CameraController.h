#ifndef INC_CAM_CONTROLLER_H_
#define INC_CAM_CONTROLLER_H_

const float CAMERA_FOLLOW_MIN_TARGET_XZ_DISTANCE		=  1.f;
const float CAMERA_FOLLOW_MAX_TARGET_XZ_DISTANCE		=  2.5f;
const float CAMERA_TARGET_Y								=  2.f;

const float CAMERA_COMBAT_DISTANCE						=  4.f;
const float CAMERA_COMBAT_DEFAULT_PITCH					=  0.7853f;
const float CAMERA_COMBAT_YAW							=  3.14f;

const float CAMERA_INTERPOLATION_TIME					=  0.15f;


class CCamera;
class CEntity3D;

// INTERFACE
class ICameraController {
  protected:
    bool interpolating;
    XMVECTOR initialLerpPosition,finalLerpPosition;
    DWORD interpolationInitialTicks;

  public:
    virtual ~ICameraController(){}
    virtual void updateCamera(CCamera *cam, float delta) = 0;

    virtual void initInterpolation(CCamera *cam, XMVECTOR finalLerpPosition) = 0;
    virtual void interpolate(CCamera *cam) = 0;

    bool isInterpolating(){ return interpolating; }
};


class CCameraDevController : public ICameraController {
  float unitsPerPixel;
  
  public:
    CCameraDevController();
    void updateCamera(CCamera *cam, float delta);

    void initInterpolation(CCamera *cam, XMVECTOR finalLerpPosition);
    void interpolate(CCamera *cam);
};


class CCameraCombatController : public ICameraController {
  
  float yaw_to_target;
  float pitch_to_target;

  public:
    CEntity3D *targetEntity;
    CCameraCombatController();
    void init(CCamera*,CEntity3D *);
    void updateCamera(CCamera *cam, float delta);

    void initInterpolation(CCamera *cam, XMVECTOR finalLerpPosition);
    void interpolate(CCamera *cam);
};

class CCameraFollowController : public ICameraController {
  
  float distanceToTargetXZ;

  public:
    CEntity3D *targetEntity;
    CCameraFollowController();
    void init(CCamera*,CEntity3D *);
    void updateCamera(CCamera *, float);

    void initInterpolation(CCamera *cam, XMVECTOR finalLerpPosition);
    void interpolate(CCamera *cam);
};

extern CCameraDevController			cam_dev_controller;
extern CCameraCombatController      cam_combat_controller;
extern CCameraFollowController		cam_follow_controller;

#endif