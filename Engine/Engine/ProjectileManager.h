#ifndef _INC_PROJECTILE_MANAGER_
#define _INC_PROJECTILE_MANAGER_

#include "Projectile.h"
#include <vector>

using namespace std; 

class CProjectileManager{
private:
    //List of the projectile
    vector<CProjectile *> projectiles;

    //Instance of the projectile manager
    static CProjectileManager* pinstance;

public:
    void addProjectile( CProjectile *projectile );
    vector<CProjectile *> getProjectiles() const { return projectiles; }; 
    void update();
    void render( );
    void removeProjectile( );
    void destroy( );
    static CProjectileManager* getInstance( );

};
#endif