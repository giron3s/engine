#include "McvPlatform.h"
#include "Utils.h"
#include <cstdio>
#include <cstdarg>

/**
 * Return if the program visualize the hidden element or not.
 */
bool isDebug(){
    return DEBUG;
}

/**
 * Return if the key is pressed or not
 */
bool isKeyPressed( int key ) {
  return (::GetAsyncKeyState( key ) & 0x8000 ) != 0;
}

// --------------------------------------------
void dbg( const char *fmt, ... ) {
  va_list args;
  va_start(args, fmt);

  char buf[ 2048 ];
  int n = vsnprintf( buf, sizeof( buf ), fmt, args );

  // Confirm the msg fits in the given buffer
  if( n < 0 )
    buf[ sizeof( buf )-1 ] = 0x00;

  // Redirect to windows dbg console
  ::OutputDebugString( buf );
}

int getRandomInt(int min, int max)
{
    return (rand()%(max-min+1))+min;
}