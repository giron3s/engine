#include "McvPlatform.h"
#include "GraphicsController.h"
#include "VertexDeclarations.h"
#include "DataProvider.h"
#include "DrawUtils.h"
#include "Texture.h"
#include "Shader.h"
#include "Render.h"
#include "AiConstants.h"

//Initialize the instace
CGraphicsController* CGraphicsController::pInstance = 0;


/**
 * Constructor
 */
CGraphicsController::CGraphicsController(){ }

/**
 * Destructor
 */
CGraphicsController::~CGraphicsController(){ }

/**
 * Get the instance of the class
 */
CGraphicsController* CGraphicsController::getInstance(){
	if(pInstance == 0){
		pInstance = new CGraphicsController;
	}
	return pInstance;
}

/**
 * Initialize the shaders/ load the solid and complex meshes
 */
void CGraphicsController::init(){
	loadShaders();
	loadPrimitiveMesh();
	loadComplexMesh();
}

/**
 * Load the solid & textured shaders
 */
void CGraphicsController::loadShaders(){
	//Load the solid shaders
	vsSolid = new CVertexShader;
	psSolid = new CPixelShader;
	vsTextured = new CVertexShader;
	psTextured = new CPixelShader;

	psSolidMaskVision = new CPixelShader;
	psTexturedMaskVision = new CPixelShader;

	//Load the solid shaders
	vsSolid->compile( "data/shaders/vision.fx", "vs_general_vision", &vtx_dcl_solid );
	psSolid->compile( "data/shaders/vision.fx", "ps_general_vision" );

	//Load the textured shaders
	vsTextured->compile( "data/shaders/vision.fx", "vs_textured_general_vision", &vtx_dcl_textured );
	psTextured->compile( "data/shaders/vision.fx", "ps_textured_general_vision" );

	//Load the mask pixel shaders
	psSolidMaskVision->compile( "data/shaders/vision.fx", "ps_mask_vision" );
	psTexturedMaskVision->compile( "data/shaders/vision.fx", "ps_textured_mask_vision" );

	CShader::createSamplers();
}


/**
 * Load the primitive mesh
 */
void CGraphicsController::loadPrimitiveMesh(){
	// Ground
	CRender *ground = new CRender;
	ground->init(RenderType::NORMAL_STATIC_MESH, DrawUtils::createGrid(5, 2), NULL, vsSolid, psSolid, psSolidMaskVision);
	meshes["ground"] = ground;

	//Enemy perceive mesh
	CRender *perceiveArea = new CRender;
	CRenderMesh *perceiveMesh = DrawUtils::createPerceiveArea(ENEMY_HEARING_RADIUS, ENEMY_VISION_ANGLE, ENEMY_VISION_DISTANCE);
	perceiveArea->init(RenderType::PERCEIVE_AREA, perceiveMesh, NULL, vsSolid, psSolid, psSolidMaskVision);
	meshes["enemyPerceiveArea"] = perceiveArea;

	//Player projectile radious
	CRender *playerProjectile = new CRender;
	CRenderMesh *playerProjectileMesh = DrawUtils::createCircle(PLAYER_PROJECTILE_RADIOUS);
	playerProjectile->init(RenderType::PROJECTILE, playerProjectileMesh, NULL, vsSolid, psSolid, psSolidMaskVision);
	meshes["playerProjectile"] = playerProjectile;

	//Enemy projectile radious
	CRender *enemyProjectile = new CRender;
	CRenderMesh *enemyProjectileMesh = DrawUtils::createCircle(ENEMY_PROJECTILE_RADIOUS);
	enemyProjectile->init(RenderType::PROJECTILE, enemyProjectileMesh, NULL, vsSolid, psSolid, psSolidMaskVision);
	meshes["enemyProjectile"] = enemyProjectile;



	// Boss combat area radius
	CRender *enemyBossCombatArea = new CRender;
	CRenderMesh *enemyBossCombatAreaMesh = DrawUtils::createCircle(BOSS_COMBAT_AREA_RADIUS);
	enemyBossCombatArea->init(RenderType::PROJECTILE, enemyBossCombatAreaMesh, NULL, vsSolid, psSolid, psSolidMaskVision);
	meshes["enemyBossCombatArea"] = enemyBossCombatArea;

	// Boss invation area radius
	CRender *enemyBossInvationArea = new CRender;
	CRenderMesh *enemyBossInvationAreaMesh = DrawUtils::createCircle(BOSS_INVATION_AREA_RADIUS);
	enemyBossInvationArea->init(RenderType::PROJECTILE, enemyBossInvationAreaMesh, NULL, vsSolid, psSolid, psSolidMaskVision);
	meshes["enemyBossInvationArea"] = enemyBossInvationArea;



	//Hidden interruptor mesh
	CRender *cube = new CRender;
	CRenderMesh *cubeMesh = DrawUtils::createCube(1);
	cube->init(RenderType::HIDDEN_STATIC_MESH, cubeMesh, NULL, vsSolid, psSolid, psSolidMaskVision);
	meshes["interruptor"] = cube;

	//Target the mesh
	CRender *target = new CRender;
	CRenderMesh *targetMesh =  DrawUtils::createCircle(ENEMY_TARGET_RADIOUS);
	target->init(RenderType::NORMAL_STATIC_MESH, targetMesh, NULL, vsSolid, psSolid, psSolidMaskVision);
	meshes["target"] = target;
}

/**
 *  Load the complex mesh from the disk
 */
void CGraphicsController::loadComplexMesh(){
	bool result;

	//Load the texture
	CTexture *texture = new CTexture;
	result = texture->load( "seafloor.dds" );
	assert(result);

	//Load the player
	CRender *player = new CRender;
	CRenderMesh *playerMesh = new CRenderMesh;
	CFileDataProvider fdp1( "data/meshes/Player.mesh" );
	result = playerMesh->load( fdp1 );
	assert( result );
	player->init(RenderType::NORMAL_DYNAMIC_MESH, playerMesh, texture, vsTextured, psTextured, psTexturedMaskVision);
	meshes["player"] = player;

	//Load the enemy
	CRender *enemy = new CRender;
	CRenderMesh *enemyMesh = new CRenderMesh;
	CFileDataProvider fdp2( "data/meshes/Enemy.mesh" );
	result = enemyMesh->load( fdp2 );
	assert( result );
	enemy->init(RenderType::NORMAL_DYNAMIC_MESH, enemyMesh, texture, vsTextured, psTextured, psTexturedMaskVision);
	meshes["enemy"] = enemy;
}

/**
 *  Search the mesh on the map. If the mesh exist return with the mesh
 *  otherwise return null.
 */
CRender* CGraphicsController::getMesh(string meshName){
	//If the mesh not exist
	if(meshes.find(meshName) == meshes.end()){
		return NULL;
	}
	//If the mesh exist
	else{
		return meshes[meshName];
	}
}

void CGraphicsController::destroy(){
	//Destroy the meshes
	map<string, CRender*>::iterator mapIter;
	for (mapIter = meshes.begin(); mapIter != meshes.end(); ++mapIter){
		CRender* mesh = &( *mapIter->second );
		mesh->destroy();
	}

	// Destroy the shaders
	psSolid->destroy( );
	vsSolid->destroy( );
	psTextured->destroy( );
	vsTextured->destroy( );
	CShader::destroySamplers();

	//Destroy the instance
	pInstance = NULL;
}
