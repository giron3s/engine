#include "McvPlatform.h"
#include "Camera.h"
#include "Angular.h"


CCamera				    camera_1;
CCamera                 camera_2;

CCamera::CCamera()
  : fov( deg2rad( 90.0f ) )
  , aspect_ratio( 1.0f )
  , znear( 0.1f )
  , zfar( 100.f )
  , camType(FOLLOW)
{
  pos = XMVectorSet( 1, 0, 0, 1 );
  target = XMVectorSet( 0, 0, 0, 1 );
  up_aux = XMVectorSet( 0, 1, 0, 1 );
  lookAt( pos, target );
  updateProjectionMatrix( );
}

void CCamera::updateViewMatrix( ) {
  view = XMMatrixLookAtRH( pos, target, up_aux );
  view_projection = view * projection;
}


void CCamera::lookAt( XMVECTOR new_pos, XMVECTOR new_target ) {
  pos = new_pos;
  target = new_target;

  front = XMVector3Normalize( target - pos );
  left  = XMVector3Normalize( XMVector3Cross( up_aux, front ) );
  up    = XMVector3Cross( front, left );

  updateViewMatrix( );
}

void CCamera::strafe(XMVECTOR new_pos){
	pos = new_pos;
	target = new_pos + front;

	updateViewMatrix( );
}

void CCamera::setFront(XMVECTOR _front){
	front = _front;

	left  = XMVector3Normalize( XMVector3Cross( up_aux, front ) );
	up    = XMVector3Cross( front, left );

	//actualizamos target para no perder coherencia
	target = pos + front;

	//actualizamos la view 
	updateViewMatrix( );
}

  void CCamera::setCamType(CamType camType){
	  this->camType = camType;
  }

// ------------------------------------
void CCamera::setProjectionParams( float new_fov, float new_aspect_ratio
  , float new_znear, float new_zfar ) {

  fov          = new_fov;
  aspect_ratio = new_aspect_ratio;
  znear        = new_znear;
  zfar         = new_zfar;

  updateProjectionMatrix( );
}
  
void CCamera::updateProjectionMatrix( ) {
  projection = XMMatrixPerspectiveFovRH( fov, aspect_ratio, znear, zfar );
  view_projection = view * projection;
}

bool CCamera::getScreenCoords( XMVECTOR world_pos, float *screen_x, float *screen_y ) const {
  assert( screen_x );
  assert( screen_y );
  XMMATRIX vp = getViewProjection( );
  XMVECTOR homo_space_coords = XMVector3TransformCoord( world_pos, vp );
  // -1 .. 1    homo_space_coords
  // 0 ... 2    + 1.0f
  // 0 ... 1    * 0.5f
  // 0 ... 800  * App.xres
  *screen_x = ( XMVectorGetX( homo_space_coords ) + 1.0f ) * 0.5f * App.xres;
  *screen_y = ( 1.0f - XMVectorGetY( homo_space_coords ) ) * 0.5f * App.yres;

  // Return true if the z is inside the zrange of the camera
  float hz = XMVectorGetZ( homo_space_coords );
  return hz >= 0.f && hz <= 1.f;
}