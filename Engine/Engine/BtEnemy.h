#ifndef _BT_ENEMY_INC
#define _BT_ENEMY_INC

#include "Bt.h"
#include "Entity3D.h"

class CProjectile;

class CBtEnemy : public bt {
 
private:
  
  XMVECTOR pushDirection;
  XMVECTOR suspiciousPoint1,suspiciousPoint2;
  bool firstHitDone,secondHitDone,thirdHitDone;

  // The list of the waypoints
  std::vector<XMVECTOR> *wpts;  
  // The current waypoint
  int currentWpt;         

  bool projectileCreated;

  CEntity3D*   playerEntity;

  DWORD        prev_ticks;

  //combat probabilities
  float kickProbability;

public:
    void create(string enemyName, CEntity3D* owner, CEntity3D* playerEntity);
    
    bool conditionDamage();
    bool conditionDead();
    int actionDead(float delta);
    int actionImpact(float delta);

    bool conditionChase();
    bool conditionFollowPlayer();
    int actionFollowPlayer(float delta);
    
    bool conditionSuspicious();
    int actionSuspicious(float delta);

    bool conditionIdleWar();
    int actionIdleWar(float delta);

    bool conditionOrbit();
    bool conditionStepBack();
    int actionStepBack(float delta);
    bool conditionStepForward();
    int actionStepForward(float delta);
    int actionOrbitLeft(float delta);
    int actionOrbitRight(float delta);

    bool conditionCover();
    int actionCover(float delta);
    int actionUncover(float delta);

    bool conditionAttack();
    bool conditionKickSeq();
    int actionKick(float delta);

    bool conditionBasicAttack();
    int actionBasicAttack(float delta);

    bool conditionLightCombo();
    int actionPreparingLightCombo(float delta);
    int actionLight2Hits(float delta);
    int actionLight3Hits(float delta);

    //bool conditionStrongCombo(); not necessary at the moment
    int actionPreparingStrongCombo(float delta);
    int actionStrong2Hits(float delta);
    int actionStrong3Hits(float delta);

    bool conditionFollowWpt();
    int actionFollowWpt(float delta);
    bool conditionRest();
    int actionRest(float delta);
    int actionNextWpt(float delta);

    int actionProcessPush(float delta);
    int actionProcessShieldBreak(float delta);

    // Probability Functions
    bool probabilityActionAreaCombat(bool zone);
    bool probabilityRest();
    bool probabilityCover();
    bool booleanProbability();
    bool detectPlayerAttack();
    
    float getKickProb(){ return kickProbability; }
    //manage the damage/life
    void manageDamage(CProjectile *p);

    //smooth rotation to point
    bool rotateToPoint(float delta,XMVECTOR point, int rotationMultiplier);

    //Update the position of the enemy 
    void updatePosition(XMVECTOR displacement, float delta); 

   
};
#endif