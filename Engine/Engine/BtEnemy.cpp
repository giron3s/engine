#include "McvPlatform.h"
#include "BtEnemy.h"
#include "Projectile.h"
#include "ProjectileManager.h"
#include "PlayerController.h"
#include "AiManager.h"
#include "AiConstants.h"
#include "utils.h"
#include <stdlib.h>


void CBtEnemy::create(string enemyName, CEntity3D* owner, CEntity3D* playerEntity){
    
    //Set the variables
    name = enemyName;
    this->owner = owner;
    this->playerEntity = playerEntity;
    this->playerWasInSight = false;

    //to be removed (simulate impact)
    impactTime = 0;

    this->prev_ticks = 0;
    this->currentWpt = 0;
    this->targeted = false;
    this->targetable = false;
    this->active = true;

    //projectile generator controllers
    this->projectileCreated = false;
    this->firstHitDone = false;
    this->secondHitDone= false;
    this->thirdHitDone = false;
    
    //set the life
    setLife( 1000.f );

    //combat probabilities
    kickProbability = ENEMY_MIN_KICK_PROB;

    //initialize the wpt vectors
    wpts = new std::vector<XMVECTOR>;
    wpts->push_back(XMVectorSet( 0.f, 0.f, 0.0f, 0.f ));
    wpts->push_back(XMVectorSet( -7.f, 0.f, -7.0f, 0.f ));
    wpts->push_back(XMVectorSet( -7.f, 0.f, 7.0f, 0.f ));
    wpts->push_back(XMVectorSet( 7.f, 0.f, -7.0f, 0.f ));
    wpts->push_back(XMVectorSet( 7.f, 0.f, 7.0f, 0.f ));

    // Charge Behaviour Tree
    createRoot("enemy", PRIORITY, NULL, NULL);
    addChild("enemy", "damage", PRIORITY, (btcondition)&CBtEnemy::conditionDamage, NULL);
    addChild("enemy", "chase", PRIORITY, (btcondition)&CBtEnemy::conditionChase, NULL);
    addChild("enemy", "suspicious", ACTION, (btcondition)&CBtEnemy::conditionSuspicious, (btaction)&CBtEnemy::actionSuspicious);
    addChild("enemy", "patrol", PRIORITY, NULL, NULL);

    addChild("damage", "dead", ACTION, NULL, (btaction)&CBtEnemy::actionDead);
    addChild("damage", "processPush", ACTION, NULL, (btaction)&CBtEnemy::actionProcessPush);
    addChild("damage", "processShieldBreak", ACTION, NULL, (btaction)&CBtEnemy::actionProcessShieldBreak);
    //addChild("damage", "impact", ACTION, NULL, (btaction)&CBtEnemy::actionImpact);
    
    addChild("chase", "followPlayer", ACTION, (btcondition)&CBtEnemy::conditionFollowPlayer, (btaction)&CBtEnemy::actionFollowPlayer);
    addChild("chase", "fight", PRIORITY, NULL, NULL);    


    //FIGHT NODES
    addChild("fight", "cover", ACTION, (btcondition)&CBtEnemy::conditionCover, (btaction)&CBtEnemy::actionCover);    
    addChild("fight", "attack", PRIORITY, (btcondition)&CBtEnemy::conditionAttack, NULL);

    //addChild("attack", "kickSeq", SEQUENCE, (btcondition)&CBtEnemy::conditionKickSeq, NULL);
    //addChild("attack", "basicAttack", ACTION, (btcondition)&CBtEnemy::conditionBasicAttack, (btaction)&CBtEnemy::actionBasicAttack);
    addChild("attack", "lightComboSeq", SEQUENCE, (btcondition)&CBtEnemy::conditionLightCombo, NULL);
    addChild("attack", "strongComboSeq", SEQUENCE, NULL, NULL);

    /*addChild("kickSeq", "kick", ACTION, NULL, (btaction)&CBtEnemy::actionKick);
    addChild("kickSeq", "kBasicAttack", ACTION, NULL, (btaction)&CBtEnemy::actionBasicAttack);*/

    //LIGHT COMBO NODES
    addChild("lightComboSeq", "preparingLightCombo", ACTION, NULL, (btaction)&CBtEnemy::actionPreparingLightCombo);
    addChild("lightComboSeq", "lightCombo", RANDOM, NULL, NULL);
    
    addChild("lightCombo", "light2Hits", ACTION, NULL, (btaction)&CBtEnemy::actionLight2Hits);
    addChild("lightCombo", "light3Hits", ACTION, NULL, (btaction)&CBtEnemy::actionLight3Hits);

    //STRONG COMBO NODES
    addChild("strongComboSeq", "preparingStrongCombo", ACTION, NULL, (btaction)&CBtEnemy::actionPreparingStrongCombo);
    addChild("strongComboSeq", "strongCombo", RANDOM, NULL, NULL);

    addChild("strongCombo", "strong2Hits", ACTION, NULL, (btaction)&CBtEnemy::actionStrong2Hits);
    addChild("strongCombo", "strong3Hits", ACTION, NULL, (btaction)&CBtEnemy::actionStrong3Hits);
    
    //STEPS NODES
    addChild("fight", "stepForward", ACTION, (btcondition)&CBtEnemy::conditionStepForward, (btaction)&CBtEnemy::actionStepForward);
    addChild("fight", "stepBack", ACTION, (btcondition)&CBtEnemy::conditionStepBack, (btaction)&CBtEnemy::actionStepBack);
    
    //ORBIT NODES
    addChild("fight", "orbit", RANDOM, (btcondition)&CBtEnemy::conditionOrbit, NULL);
    addChild("orbit", "orbitLeft", ACTION, NULL, (btaction)&CBtEnemy::actionOrbitLeft);
    addChild("orbit", "orbitRight", ACTION, NULL, (btaction)&CBtEnemy::actionOrbitRight);

    addChild("fight", "idleWar", ACTION, NULL, (btaction)&CBtEnemy::actionIdleWar);

    //PATROL NODES
    addChild("patrol", "followWpt", ACTION, (btcondition)&CBtEnemy::conditionFollowWpt, (btaction)&CBtEnemy::actionFollowWpt);
    addChild("patrol", "rest", ACTION, (btcondition)&CBtEnemy::conditionRest, (btaction)&CBtEnemy::actionRest);
    addChild("patrol", "nextWpt", ACTION, NULL, (btaction)&CBtEnemy::actionNextWpt);
    
}


// DAMAGE ------------------------------------------------

bool CBtEnemy::conditionDamage() {

    return false;
}

bool CBtEnemy::conditionDead() {
    // Condition life == 0
    // Animation dead
    return false;
}

int CBtEnemy::actionDead(float delta){
    //dead pose
    owner->setPitch(deg2rad(90));

    //if this enemy was targeted we call targetEnemyEvaluation to manage the next targetable enemy
    if(targeted){
        if(!CAiManager::getInstance()->getPlayerController()->targetEnemyEvaluation() ){
            CAiManager::getInstance()->getPlayerController()->changeState("idle");
        }
    }

    owner->removeMesh("enemyPerceiveArea");

    //set active to false so no more recalcs are done to this IA
    active=false;

    // Animation Dead
    return false;
}

int CBtEnemy::actionImpact(float delta){
    printf("%s: action impact !! \n",name.c_str());
    // Animation Impact life --
    return false;
}

int CBtEnemy::actionProcessPush(float delta){

    if(prev_ticks == 0){
        prev_ticks = GetTickCount();
    }    
    float elapsed = ( GetTickCount() - prev_ticks ) / 1000.0f;
        
    owner->setPosition( owner->getPosition() - owner->getFront() * delta * ENEMY_PUSHED_SPEED );
    dbg("%f\n",elapsed);
    if(elapsed > ENEMY_PUSHED_DURATION_SECS){
        prev_ticks = 0;
        return true;
    }

    return false;
}

int CBtEnemy::actionProcessShieldBreak(float delta){

    if(prev_ticks == 0){
        prev_ticks = GetTickCount();
    }
    
    DWORD new_ticks = GetTickCount();
    float elapsed = ( new_ticks - prev_ticks ) / 1000.0f;

    if(elapsed > ENEMY_SHIELD_BREAK_DURATION_SECS){
        prev_ticks = 0;
        return true;        
    }
    return false;
}

// --------------------------------------------------------


// CHASE ------------------------------------------------

bool CBtEnemy::conditionChase() {
    //if player is inside perceive area go to chase
    if(owner->isPerceived(playerEntity->getPosition(),ENEMY_VISION_ANGLE,ENEMY_VISION_DISTANCE,ENEMY_HEARING_RADIUS)){
        playerWasInSight = true;
        return true;
    }
    return false;
}

bool CBtEnemy::conditionFollowPlayer() {
    if( distanceToPlayer > ENEMY_FIGHT_DISTANCE) {
        return true;
    }
    return false;
}

int CBtEnemy::actionFollowPlayer(float delta) {
  
  //face the player
    rotateToPoint(delta,playerEntity->getPosition(),ENEMY_CHASE_ANGULAR_SPEED_MULTIPLIER);

  //Move to the playerEntity direction
  owner->setPosition( owner->pos + (owner->getFront() * delta * ENEMY_CHASE_LINEAR_SPEED)  );
  return true;
}

bool CBtEnemy::conditionSuspicious() {
    if( playerWasInSight && !owner->isPerceived(playerEntity->getPosition(),ENEMY_VISION_ANGLE,ENEMY_VISION_DISTANCE,ENEMY_HEARING_RADIUS) ){
        playerWasInSight = false;
        return true;
    }

    return false;
}

int CBtEnemy::actionSuspicious(float delta) {
  
  
  //SUSPICIOUS ANIMATION
  if(prev_ticks == 0){
    prev_ticks = GetTickCount();

    suspiciousPoint1 = owner->getPosition() + owner->getFront() + owner->getLeft();
    suspiciousPoint2 = owner->getPosition() + owner->getFront() - owner->getLeft();

  }

  float elapsedTime = (GetTickCount() - prev_ticks) / 1000.f;
  float elapsedPercent = elapsedTime / ENEMY_SUSPICIOUS_DURATION;

  //take a look to my right
  if(elapsedPercent > 0.6){
      rotateToPoint(delta,suspiciousPoint2,ENEMY_CHASE_ANGULAR_SPEED_MULTIPLIER);
  }else if(elapsedPercent > 0.3){
  //take a look to my left
      rotateToPoint(delta,suspiciousPoint1,ENEMY_CHASE_ANGULAR_SPEED_MULTIPLIER);
  }

  //check for enemy in sight again to intterupt this action
  if(owner->isPerceived(playerEntity->getPosition(),ENEMY_VISION_ANGLE,ENEMY_VISION_DISTANCE,ENEMY_HEARING_RADIUS)){
      return true;
  }

  if (elapsedTime > ENEMY_SUSPICIOUS_DURATION ){
    prev_ticks=0;
    return true;
  }
  return false;
}

// --------------------------------------------------------


// FIGHT -------------------------------------------------

// IDLE WAR
bool CBtEnemy::conditionIdleWar() {
/*  if (probabilityActionAreaCombat (ZONE_EXTERIOR) ) {
    return true;
  }*/

  int prob = getRandomInt(0,99);
    //dbg("prob idleWar: %d\n",prob);

  if(prob < 20){
      return true;
  }
  return false;
}

int CBtEnemy::actionIdleWar(float delta) {


    // Anmation Idle War
  if(prev_ticks == 0){
    prev_ticks = GetTickCount();
  }

  //face the player
  rotateToPoint(delta,playerEntity->getPosition(),ENEMY_FAST_ANGULAR_SPEED_MULTIPLIER);

  //Actual time - Change state time > Rest animation
  float elapsedTime = (GetTickCount() - prev_ticks) / 1000.f;
  if (elapsedTime >  ENEMY_IDLEWAR_DURATION_SECS){
    prev_ticks=0;
    return true;
  }
  return false;
}

// MOVE
bool CBtEnemy::conditionOrbit() {
  /*if (probabilityActionAreaCombat(ZONE_EXTERIOR)) {
    return true;
  }*/
    int prob = getRandomInt(0,99);

    if(prob < 60){
        return true;
    }
  return false;
}

bool CBtEnemy::conditionStepBack() {
  // Calcutate distance to control not out of area combat using this movement
  if (distanceToPlayer < ENEMY_STEPBACK_LIMIT) {
    return true;
  }
  else { return false; }
}

int CBtEnemy::actionStepBack(float delta) {
  //STEP ANIMATION
  if(prev_ticks == 0){
    prev_ticks = GetTickCount();
  }

  owner->setPosition( owner->pos - owner->getFront() * delta * ENEMY_STEP_SPEED  );
  rotateToPoint(delta,playerEntity->getPosition(),ENEMY_FAST_ANGULAR_SPEED_MULTIPLIER);

  float elapsedTime = (GetTickCount() - prev_ticks) / 1000.f;
  if (elapsedTime >  ENEMY_STEP_DURATION){

    prev_ticks=0;
    return true;
    
  }
  return false;
}

bool CBtEnemy::conditionStepForward() {
  // Calcutate distance to control not out of area combat (enter too close) using this movement
  if (distanceToPlayer > ENEMY_STEPFORWARD_LIMIT){
    
    if(getRandomInt(0,99) < 60){
        return true;
    }
  }else if(distanceToPlayer - ENEMY_STEP_DURATION * ENEMY_STEP_SPEED > ENEMY_STEPBACK_LIMIT){
    //if we are close (but still far enough to make a fordward step) the probability of steping fordward decreases
    if(getRandomInt(0,99) < 10){
        return true;
    }
  }

  return false;
}

int CBtEnemy::actionStepForward(float delta) {
  //STEP ANIMATION
  if(prev_ticks == 0){
    prev_ticks = GetTickCount();
  }

  owner->setPosition( owner->pos + owner->getFront() * delta * ENEMY_STEP_SPEED  );
  //face the player
  rotateToPoint(delta,playerEntity->getPosition(),ENEMY_FAST_ANGULAR_SPEED_MULTIPLIER);
  
  float elapsedTime = (GetTickCount() - prev_ticks) / 1000.f;
  if (elapsedTime >  ENEMY_STEP_DURATION){
    prev_ticks=0;
    return true;
  }
  
  return false;
}

int CBtEnemy::actionOrbitLeft(float delta) {
  // ORBIT ANIMATION
  if(prev_ticks == 0){
    prev_ticks = GetTickCount();
  }

  owner->setPosition( owner->getPosition() + owner->getLeft() * delta * ENEMY_ORBIT_SPEED);

  //face the player
  rotateToPoint(delta,playerEntity->getPosition(),ENEMY_FAST_ANGULAR_SPEED_MULTIPLIER); 

  float elapsedTime = (GetTickCount() - prev_ticks) / 1000.f;
  if (elapsedTime >  ENEMY_ORBIT_DURATION_SECS){

    if( !(getRandomInt(0,99) < ENEMY_CONTINUE_ORBIT_PROB)){
        prev_ticks=0;
        return true;
    }
  }
  return false;
}

int CBtEnemy::actionOrbitRight(float delta) {

  // ORBIT ANIMATION
  if(prev_ticks == 0){
    prev_ticks = GetTickCount();
  }

  owner->setPosition( owner->getPosition() - owner->getLeft() * delta * ENEMY_ORBIT_SPEED);

  //face the player
  rotateToPoint(delta,playerEntity->getPosition(),ENEMY_FAST_ANGULAR_SPEED_MULTIPLIER);

  //Actual time - Change state time > Rest animation
  float elapsedTime = (GetTickCount() - prev_ticks) / 1000.f;
  if (elapsedTime >  ENEMY_ORBIT_DURATION_SECS){
    if( !(getRandomInt(0,99) < ENEMY_CONTINUE_ORBIT_PROB)){
        prev_ticks=0;
        return true;
    }
  }
  return false;
}


// COVER

bool CBtEnemy::conditionCover() {
  int prob = getRandomInt(0,99);

  if(detectPlayerAttack()){
      if(prob < 70){
        return true;
      }
  }else if(prob < 30){
      return true;
  }
  return false;
}

int CBtEnemy::actionCover(float delta) {

  // Animation Cover
  if(prev_ticks == 0){
    prev_ticks = GetTickCount();
  }

  //face the player
  rotateToPoint(delta,playerEntity->getPosition(),ENEMY_FAST_ANGULAR_SPEED_MULTIPLIER);

  //Actual time - Change state time > Rest animation
  float elapsedTime = (GetTickCount() - prev_ticks) / 1000.f;
  if (elapsedTime >  ENEMY_COVER_DURATION_SECS){
    prev_ticks=0;
    return true;
  }
  return false;

}

/*
int CBtEnemy::actionUncover(float delta) {
  
  if (!probabilityCover()) {
      float elapsedTime = (GetTickCount() - prev_ticks) / 1000.f;
      if(elapsedTime >  ENEMY_REST_DURATION_SECS){
            //TODO: ANIMATION REST
            return true;
      }
  }
  else {
        return false;
  }

}*/


// ATTACK -------------------------------------------------
bool CBtEnemy::conditionAttack() {


  int prob = getRandomInt(0,99);
  
  if(distanceToPlayer > ENEMY_STEPFORWARD_LIMIT){
      return false;
  }else if(prob < 70){

      //if we reach this line we are sure we're going to attack
      //check if the player is covering to modify de probabilty of launching a kick;
      if( CAiManager::getInstance()->getPlayerController()->getState().compare("block") == 0){
          //increase kick probability in 20% of (max probabilty - min probability)
          kickProbability += (ENEMY_MAX_KICK_PROB - ENEMY_MIN_KICK_PROB) * 0.05;
          if(kickProbability > ENEMY_MAX_KICK_PROB){ kickProbability = ENEMY_MAX_KICK_PROB; }
      }else{
          //decrease kick probability in 20% of (max probabilty - min probability)
          kickProbability -= (ENEMY_MAX_KICK_PROB - ENEMY_MIN_KICK_PROB) * 0.05;
          if(kickProbability < ENEMY_MIN_KICK_PROB){ kickProbability = ENEMY_MIN_KICK_PROB; }
      }
    return true;
  }

  return false;
}

bool CBtEnemy::conditionKickSeq() {
  if (getRandomInt(0,99) < kickProbability) {
    return true;
  }
  return false;
}


int CBtEnemy::actionKick(float delta) {

    //attack animation starts
    if(prev_ticks == 0){
        prev_ticks = GetTickCount();
    }
    
    float elapsed = ( GetTickCount() - prev_ticks ) / 1000.0f;

    //elapsed percent of the attack (in range [0,1])
    float elapsedPercent = elapsed / ENEMY_KICK_DURATION_SECS;

    //damage starts
    if(!projectileCreated && elapsed > ENEMY_KICK_HIT_DELAY_SECS){
        projectileCreated = true;
        //Create the projectile of the attack and add it to the projectile manager
        CProjectile *projectile = new CProjectile;
        projectile->create(owner, ENEMY_KICK_HIT_DURATION_SECS, ENEMY_PROJECTILE_RADIOUS, ProjectileType::SHIELD_BREAKER, ENEMY_KICK_DAMAGE, 0.f);
        CProjectileManager::getInstance()->addProjectile(projectile);
    }

    //attack ends
    if(elapsed > ENEMY_KICK_DURATION_SECS){
        //we launched a kick so we reset the kick probability to avoid launching kicks in loop
        kickProbability = ENEMY_MIN_KICK_PROB;

        prev_ticks = 0;
        projectileCreated = false;
        return true;
    }

    return false;
}

bool CBtEnemy::conditionBasicAttack() {
  if (getRandomInt(0,99) < 30) {
    return true;
  }
  return false;
}


int CBtEnemy::actionBasicAttack(float delta) {

    //attack animation starts
    if(prev_ticks == 0){
        prev_ticks = GetTickCount();
    }
    

    float elapsed = ( GetTickCount() - prev_ticks ) / 1000.0f;

    //elapsed percent of the attack (in range [0,1])
    float elapsedPercent = elapsed / ENEMY_BATTACK_DURATION_SECS;

    //damage starts
    if(!projectileCreated && elapsed > ENEMY_BATTACK_HIT_DELAY_SECS){
        projectileCreated = true;
        //Create the projectile of the attack and add it to the projectile manager
        CProjectile *projectile = new CProjectile;
        projectile->create(owner, ENEMY_BATTACK_HIT_DURATION_SECS, ENEMY_PROJECTILE_RADIOUS, ProjectileType::NORMAL, ENEMY_BATTACK_DAMAGE, 0.f);
        CProjectileManager::getInstance()->addProjectile(projectile);
    }

    //combo window
    /*if(elapsedPercent > 0.5 && elapsedPercent < 1){
        if(booleanProbability()){
            prev_ticks = 0;
            projectileCreated = false;
        }
    }*/

    //attack ends
    if(elapsed > ENEMY_BATTACK_DURATION_SECS){
        prev_ticks = 0;
        projectileCreated = false;
        return true;
    }

    return false;
}


bool CBtEnemy::conditionLightCombo() {
    if (getRandomInt(0,99) < 10) {
        return true;
    }
    return false;
}

int CBtEnemy::actionPreparingLightCombo(float delta) {

    //preparing light combo
    if(prev_ticks == 0){
        prev_ticks = GetTickCount();
    }
    float elapsed = ( GetTickCount() - prev_ticks ) / 1000.0f;

    //at the most we rotate with slower speed in this situation
    //rotateToPoint(delta, playerEntity->getPosition(), ENEMY_FAST_ANGULAR_SPEED_MULTIPLIER);
    
    if(elapsed > ENEMY_PREPARING_LIGHT_DURATION_SECS){
        prev_ticks = 0;
        return true;
    }
    return false;
}

int CBtEnemy::actionLight2Hits(float delta){
    //LIGHT COMBO 2 HITS ANIMATION
    if(prev_ticks == 0){
        prev_ticks = GetTickCount();
    }

    float elapsed = ( GetTickCount() - prev_ticks ) / 1000.0f;

    //elapsed percent of the attack (in range [0,1])
    float elapsedPercent = elapsed / ENEMY_LIGHT_2HITS_DURATION_SECS;

    
    if(elapsedPercent > ENEMY_LIGHT_FACE_PLAYER_DELAY_PERCENT){
        rotateToPoint(delta, playerEntity->getPosition(), ENEMY_FAST_ANGULAR_SPEED_MULTIPLIER);

    //window for the second hit of the combo
    }else if(elapsedPercent > ENEMY_LIGHT_COMBOWINDOW2_DELAY_PERCENT && elapsedPercent < ENEMY_LIGHT_COMBOWINDOW2_DURATION_PERCENT + ENEMY_LIGHT_COMBOWINDOW2_DELAY_PERCENT){
        
        //go fordward at combo speed
        owner->setPosition( owner->pos + (owner->getFront() * delta * ENEMY_COMBO_SPEED)  );


        if(!secondHitDone && elapsedPercent > ENEMY_LIGHT_SECOND_HIT_DELAY_PERCENT){
            //projectile of the second hit of this combo
            CProjectile *projectile = new CProjectile;

            //calculate pushDistance for projectile
            float pushDistance = ENEMY_COMBO_SPEED * ENEMY_LIGHT_COMBOWINDOW2_DURATION_PERCENT * ENEMY_LIGHT_2HITS_DURATION_SECS;

            //create projectile
            projectile->create(owner, ENEMY_LIGHT_SECOND_HIT_DURATION_SECS, ENEMY_PROJECTILE_RADIOUS, ProjectileType::PUSHER, ENEMY_LIGHT_SECOND_HIT_DAMAGE, pushDistance);
            CProjectileManager::getInstance()->addProjectile(projectile);

            secondHitDone = true;
        }


    //window for the first hit of the combo
    } else if(elapsedPercent > ENEMY_LIGHT_COMBOWINDOW1_DELAY_PERCENT && elapsedPercent < ENEMY_LIGHT_COMBOWINDOW1_DURATION_PERCENT + ENEMY_LIGHT_COMBOWINDOW1_DELAY_PERCENT){
        
        //go fordward at combo speed
        owner->setPosition( owner->pos + (owner->getFront() * delta * ENEMY_COMBO_SPEED)  );       

        //projectile of the first hit of this combo       
        if(!firstHitDone && elapsedPercent > ENEMY_LIGHT_FIRST_HIT_DELAY_PERCENT){
            //Create the projectile of the attack and add it to the projectile manager
            CProjectile *projectile = new CProjectile;
            
            //calculate pushDistance for projectile
            float pushDistance = ENEMY_COMBO_SPEED * ENEMY_LIGHT_COMBOWINDOW1_DURATION_PERCENT * ENEMY_LIGHT_2HITS_DURATION_SECS;

            //create projectile
            projectile->create(owner, ENEMY_LIGHT_FIRST_HIT_DURATION_SECS, ENEMY_PROJECTILE_RADIOUS, ProjectileType::PUSHER, ENEMY_LIGHT_FIRST_HIT_DAMAGE,pushDistance);
            CProjectileManager::getInstance()->addProjectile(projectile);

            firstHitDone = true;
        }
    }

    //COMBO ends
    if(elapsed > ENEMY_LIGHT_2HITS_DURATION_SECS){
        prev_ticks = 0;
        secondHitDone=false;
        firstHitDone=false;

        return true;
    }

    return false;
}

int CBtEnemy::actionLight3Hits(float delta){

    //LIGHT COMBO 3 HITS ANIMATION
    if(prev_ticks == 0){
        prev_ticks = GetTickCount();
    }

    float elapsed = ( GetTickCount() - prev_ticks ) / 1000.0f;

    //elapsed percent of the attack (in range [0,1])
    float elapsedPercent = elapsed / ENEMY_LIGHT_3HITS_DURATION_SECS;

    if(elapsedPercent > ENEMY_LIGHT_FACE_PLAYER_DELAY_PERCENT){
        rotateToPoint(delta, playerEntity->getPosition(), ENEMY_FAST_ANGULAR_SPEED_MULTIPLIER);

    //window for the third hit of the combo
    }else if(elapsedPercent > ENEMY_LIGHT_COMBOWINDOW3_DELAY_PERCENT && elapsedPercent < ENEMY_LIGHT_COMBOWINDOW3_DURATION_PERCENT + ENEMY_LIGHT_COMBOWINDOW3_DELAY_PERCENT){

        //go fordward at combo speed
        owner->setPosition( owner->pos + (owner->getFront() * delta * ENEMY_COMBO_SPEED)  );


        if(!thirdHitDone && elapsedPercent > ENEMY_LIGHT_THIRD_HIT_DELAY_PERCENT){
            //projectile of the third hit of this combo
            CProjectile *projectile = new CProjectile;
            
            //calculate pushDistance for projectile
            float pushDistance = ENEMY_COMBO_SPEED * ENEMY_LIGHT_COMBOWINDOW3_DURATION_PERCENT * ENEMY_LIGHT_3HITS_DURATION_SECS;

            //create projectile
            projectile->create(owner, ENEMY_LIGHT_THIRD_HIT_DURATION_SECS, ENEMY_PROJECTILE_RADIOUS, ProjectileType::PUSHER, ENEMY_LIGHT_THIRD_HIT_DAMAGE,pushDistance);
            CProjectileManager::getInstance()->addProjectile(projectile);

            thirdHitDone = true;
        }


    //window for the second hit of the combo
    }else if(elapsedPercent > ENEMY_LIGHT_COMBOWINDOW2_DELAY_PERCENT && elapsedPercent < ENEMY_LIGHT_COMBOWINDOW2_DURATION_PERCENT + ENEMY_LIGHT_COMBOWINDOW2_DELAY_PERCENT){
        
        //go fordward at combo speed
        owner->setPosition( owner->pos + (owner->getFront() * delta * ENEMY_COMBO_SPEED)  );


        if(!secondHitDone && elapsedPercent > ENEMY_LIGHT_SECOND_HIT_DELAY_PERCENT){
            //projectile of the second hit of this combo
            CProjectile *projectile = new CProjectile;
                        
            //calculate pushDistance for projectile
            float pushDistance = ENEMY_COMBO_SPEED * ENEMY_LIGHT_COMBOWINDOW2_DURATION_PERCENT * ENEMY_LIGHT_3HITS_DURATION_SECS;

            //create projectile
            projectile->create(owner, ENEMY_LIGHT_SECOND_HIT_DURATION_SECS, ENEMY_PROJECTILE_RADIOUS, ProjectileType::PUSHER, ENEMY_LIGHT_SECOND_HIT_DAMAGE,pushDistance);
            CProjectileManager::getInstance()->addProjectile(projectile);

            secondHitDone = true;
        }


      //window for the first hit of the combo
    }else if(elapsedPercent > ENEMY_LIGHT_COMBOWINDOW1_DELAY_PERCENT && elapsedPercent < ENEMY_LIGHT_COMBOWINDOW1_DURATION_PERCENT + ENEMY_LIGHT_COMBOWINDOW1_DELAY_PERCENT){
        
        //go fordward at combo speed
        owner->setPosition( owner->pos + (owner->getFront() * delta * ENEMY_COMBO_SPEED)  );
        
        //projectile of the first hit of this combo       
        if(!firstHitDone && elapsedPercent > ENEMY_LIGHT_FIRST_HIT_DELAY_PERCENT){
            //Create the projectile of the attack and add it to the projectile manager
            CProjectile *projectile = new CProjectile;
                        
            //calculate pushDistance for projectile
            float pushDistance = ENEMY_COMBO_SPEED * ENEMY_LIGHT_COMBOWINDOW1_DURATION_PERCENT * ENEMY_LIGHT_3HITS_DURATION_SECS;

            //create projectile
            projectile->create(owner, ENEMY_LIGHT_FIRST_HIT_DURATION_SECS, ENEMY_PROJECTILE_RADIOUS, ProjectileType::PUSHER, ENEMY_LIGHT_FIRST_HIT_DAMAGE,pushDistance);
            CProjectileManager::getInstance()->addProjectile(projectile);

            firstHitDone = true;
        }
    }


    //COMBO ends
    if(elapsed > ENEMY_LIGHT_3HITS_DURATION_SECS){
        prev_ticks = 0;
        secondHitDone=false;
        firstHitDone=false;
        thirdHitDone=false;

        return true;
    }

    return false;
}

int CBtEnemy::actionPreparingStrongCombo(float delta){
        //preparing STRONG combo
    if(prev_ticks == 0){
        prev_ticks = GetTickCount();
    }
    float elapsed = ( GetTickCount() - prev_ticks ) / 1000.0f;

    //at the most we rotate with slower speed in this situation
    //rotateToPoint(delta, playerEntity->getPosition(), ENEMY_FAST_ANGULAR_SPEED_MULTIPLIER);

    if(elapsed > ENEMY_PREPARING_STRONG_DURATION_SECS){
        prev_ticks = 0;
        return true;
    }
    return false;
}

int CBtEnemy::actionStrong2Hits(float delta){
    //STRONG COMBO 2 HITS ANIMATION
    if(prev_ticks == 0){
        prev_ticks = GetTickCount();
    }

    float elapsed = ( GetTickCount() - prev_ticks ) / 1000.0f;

    //elapsed percent of the attack (in range [0,1])
    float elapsedPercent = elapsed / ENEMY_STRONG_2HITS_DURATION_SECS;

    if(elapsedPercent > ENEMY_STRONG_FACE_PLAYER_DELAY_PERCENT){
        rotateToPoint(delta, playerEntity->getPosition(), ENEMY_FAST_ANGULAR_SPEED_MULTIPLIER);

    //window for the second hit of the combo
    }else if(elapsedPercent > ENEMY_STRONG_COMBOWINDOW2_DELAY_PERCENT && elapsedPercent < ENEMY_STRONG_COMBOWINDOW2_DURATION_PERCENT + ENEMY_STRONG_COMBOWINDOW2_DELAY_PERCENT){
        
        //go fordward at combo speed
        owner->setPosition( owner->pos + (owner->getFront() * delta * ENEMY_COMBO_SPEED)  );


        if(!secondHitDone && elapsedPercent > ENEMY_STRONG_SECOND_HIT_DELAY_PERCENT){
            //projectile of the second hit of this combo
            CProjectile *projectile = new CProjectile;
                        
            //calculate pushDistance for projectile
            float pushDistance = ENEMY_COMBO_SPEED * ENEMY_STRONG_COMBOWINDOW2_DURATION_PERCENT * ENEMY_STRONG_2HITS_DURATION_SECS;

            //create projectile
            projectile->create(owner, ENEMY_STRONG_SECOND_HIT_DURATION_SECS, ENEMY_PROJECTILE_RADIOUS, ProjectileType::PUSHER, ENEMY_STRONG_SECOND_HIT_DAMAGE,pushDistance);
            CProjectileManager::getInstance()->addProjectile(projectile);

            secondHitDone = true;
        }


      //window for the first hit of the combo
    }else if(elapsedPercent > ENEMY_STRONG_COMBOWINDOW1_DELAY_PERCENT && elapsedPercent < ENEMY_STRONG_COMBOWINDOW1_DURATION_PERCENT + ENEMY_STRONG_COMBOWINDOW1_DELAY_PERCENT){
        
        //go fordward at combo speed
        owner->setPosition( owner->pos + (owner->getFront() * delta * ENEMY_COMBO_SPEED)  );
        
        //projectile of the first hit of this combo       
        if(!firstHitDone && elapsedPercent > ENEMY_STRONG_FIRST_HIT_DELAY_PERCENT){
            //Create the projectile of the attack and add it to the projectile manager
            CProjectile *projectile = new CProjectile;
                                    
            //calculate pushDistance for projectile
            float pushDistance = ENEMY_COMBO_SPEED * ENEMY_STRONG_COMBOWINDOW1_DURATION_PERCENT * ENEMY_STRONG_2HITS_DURATION_SECS;

            //create projectile
            projectile->create(owner, ENEMY_STRONG_FIRST_HIT_DURATION_SECS, ENEMY_PROJECTILE_RADIOUS, ProjectileType::PUSHER, ENEMY_STRONG_FIRST_HIT_DAMAGE,pushDistance);
            CProjectileManager::getInstance()->addProjectile(projectile);

            firstHitDone = true;
        }
    }



    //COMBO ends
    if(elapsed > ENEMY_STRONG_2HITS_DURATION_SECS){
        prev_ticks = 0;
        secondHitDone=false;
        firstHitDone=false;

        return true;
    }

    return false;
}

int CBtEnemy::actionStrong3Hits(float delta){
    //STRONG COMBO 3 HITS ANIMATION
    if(prev_ticks == 0){
        prev_ticks = GetTickCount();
    }

    float elapsed = ( GetTickCount() - prev_ticks ) / 1000.0f;

    //elapsed percent of the attack (in range [0,1])
    float elapsedPercent = elapsed / ENEMY_STRONG_3HITS_DURATION_SECS;

    if(elapsedPercent > ENEMY_STRONG_FACE_PLAYER_DELAY_PERCENT){
        rotateToPoint(delta, playerEntity->getPosition(), ENEMY_FAST_ANGULAR_SPEED_MULTIPLIER);

    //window for the third hit of the combo
    }else if(elapsedPercent > ENEMY_STRONG_COMBOWINDOW3_DELAY_PERCENT && elapsedPercent < ENEMY_STRONG_COMBOWINDOW3_DURATION_PERCENT + ENEMY_STRONG_COMBOWINDOW3_DELAY_PERCENT){

        //go fordward at combo speed
        owner->setPosition( owner->pos + (owner->getFront() * delta * ENEMY_COMBO_SPEED)  );


        if(!thirdHitDone && elapsedPercent > ENEMY_STRONG_THIRD_HIT_DELAY_PERCENT){
            //projectile of the third hit of this combo
            CProjectile *projectile = new CProjectile;
                                    
            //calculate pushDistance for projectile
            float pushDistance = ENEMY_COMBO_SPEED * ENEMY_STRONG_COMBOWINDOW3_DURATION_PERCENT * ENEMY_STRONG_3HITS_DURATION_SECS;

            //create projectile
            projectile->create(owner, ENEMY_STRONG_THIRD_HIT_DURATION_SECS, ENEMY_PROJECTILE_RADIOUS, ProjectileType::PUSHER, ENEMY_STRONG_THIRD_HIT_DAMAGE,pushDistance);
            CProjectileManager::getInstance()->addProjectile(projectile);

            thirdHitDone = true;
        }


    //window for the second hit of the combo
    }else if(elapsedPercent > ENEMY_STRONG_COMBOWINDOW2_DELAY_PERCENT && elapsedPercent < ENEMY_STRONG_COMBOWINDOW2_DURATION_PERCENT + ENEMY_STRONG_COMBOWINDOW2_DELAY_PERCENT){
        
        //go fordward at combo speed
        owner->setPosition( owner->pos + (owner->getFront() * delta * ENEMY_COMBO_SPEED)  );


        if(!secondHitDone && elapsedPercent > ENEMY_STRONG_SECOND_HIT_DELAY_PERCENT){
            //projectile of the second hit of this combo
            CProjectile *projectile = new CProjectile;
                                                
            //calculate pushDistance for projectile
            float pushDistance = ENEMY_COMBO_SPEED * ENEMY_STRONG_COMBOWINDOW2_DURATION_PERCENT * ENEMY_STRONG_3HITS_DURATION_SECS;

            //create projectile
            projectile->create(owner, ENEMY_STRONG_SECOND_HIT_DURATION_SECS, ENEMY_PROJECTILE_RADIOUS, ProjectileType::PUSHER, ENEMY_STRONG_SECOND_HIT_DAMAGE,pushDistance);
            CProjectileManager::getInstance()->addProjectile(projectile);

            secondHitDone = true;
        }


      //window for the first hit of the combo
    }else if(elapsedPercent > ENEMY_STRONG_COMBOWINDOW1_DELAY_PERCENT && elapsedPercent < ENEMY_STRONG_COMBOWINDOW1_DURATION_PERCENT + ENEMY_STRONG_COMBOWINDOW1_DELAY_PERCENT){
        
        //go fordward at combo speed
        owner->setPosition( owner->pos + (owner->getFront() * delta * ENEMY_COMBO_SPEED)  );
        
        //projectile of the first hit of this combo       
        if(!firstHitDone && elapsedPercent > ENEMY_STRONG_FIRST_HIT_DELAY_PERCENT){
            //Create the projectile of the attack and add it to the projectile manager
            CProjectile *projectile = new CProjectile;
                                                
            //calculate pushDistance for projectile
            float pushDistance = ENEMY_COMBO_SPEED * ENEMY_STRONG_COMBOWINDOW1_DURATION_PERCENT * ENEMY_STRONG_3HITS_DURATION_SECS;

            //create projectile
            projectile->create(owner, ENEMY_STRONG_FIRST_HIT_DURATION_SECS, ENEMY_PROJECTILE_RADIOUS, ProjectileType::PUSHER, ENEMY_STRONG_FIRST_HIT_DAMAGE,pushDistance);
            CProjectileManager::getInstance()->addProjectile(projectile);

            firstHitDone = true;
        }
    }


    //COMBO ends
    if(elapsed > ENEMY_STRONG_3HITS_DURATION_SECS){
        prev_ticks = 0;
        secondHitDone=false;
        firstHitDone=false;
        thirdHitDone=false;

        return true;
    }

    return false;
}


// --------------------------------------------------------



// PATROL -------------------------------------------------

bool CBtEnemy::conditionFollowWpt() {
  
    XMVECTOR difference = owner->pos - wpts->at(currentWpt);
    float distance = XMVectorGetX(XMVector3Length(difference));
    if(distance > MIN_WPT_DISTANCE){
        return true;
    }
    else {
        //prev_ticks = GetTickCount();
        return false;
    }
}

int CBtEnemy::actionFollowWpt(float delta) {

    rotateToPoint(delta,wpts->at(currentWpt),ENEMY_ANGULAR_SPEED_MULTIPLIER); 
    owner->setPosition( owner->pos + (owner->getFront() * delta * ENEMY_LINEAR_SPEED)  );

    return true;
}

bool CBtEnemy::conditionRest() {
  if (probabilityRest()){ return true; }
  return false;
}

int CBtEnemy::actionRest(float delta){
  if(prev_ticks == 0){
      prev_ticks = GetTickCount();
  }
  //Actual time - Change state time > Rest animation
  float elapsedTime = (GetTickCount() - prev_ticks) / 1000.f;
  if (elapsedTime >  ENEMY_REST_DURATION_SECS){
    prev_ticks=0;
    //TODO: ANIMATION REST
    return true;
  }
  return false;
}

int CBtEnemy::actionNextWpt(float delta){
    currentWpt = (currentWpt + 1) % 5;
    return true;
}

//----------------------------------------------------------



// Probability Functions
// ------------------------------------------------------------

bool CBtEnemy::probabilityActionAreaCombat(bool actionZone) {
    
  int prob = getRandomInt(0,99);
  
  // 1 - In zone exterior and action zone exterior
  if ( (distanceToPlayer > MIDLE_AREA_COMBAT_DISTANCE && distanceToPlayer < ENEMY_FIGHT_DISTANCE) && actionZone == false ) {
    if (prob < PROB_ACTION_ZONE) {
      return true;
    }
    else { return false; }
  }

  // 2 - In zone exterior and action zone interior
  if ( (distanceToPlayer > MIDLE_AREA_COMBAT_DISTANCE && distanceToPlayer < ENEMY_FIGHT_DISTANCE) && actionZone == true ) {
    if (prob > PROB_ACTION_ZONE) {
      return true;
    }
    else { return false; }
  }

  // 3 - In zone interior and action zone exterior
  if ( ( distanceToPlayer > TOO_CLOSE_DISTANCE && distanceToPlayer < MIDLE_AREA_COMBAT_DISTANCE) && actionZone == false ) {
    if (prob > PROB_ACTION_ZONE) {
      return true;
    }
    else { return false; }
  }

  // 4 - In zone interior and action zone interior
  if ( ( distanceToPlayer > TOO_CLOSE_DISTANCE && distanceToPlayer < MIDLE_AREA_COMBAT_DISTANCE) && actionZone == true ) {
    if (prob < PROB_ACTION_ZONE) {
      return true;
    }
    else { return false; }
  }
}

bool CBtEnemy::probabilityRest() {
    return getRandomInt(0,99) < ENEMY_PROB_REST;
}

bool CBtEnemy::probabilityCover() {
    return getRandomInt(0,99) < ENEMY_COVER_PROB;
}

bool CBtEnemy::booleanProbability(){
    return getRandomInt(0,1) == 0;
}

bool CBtEnemy::detectPlayerAttack() {

    if (CAiManager::getInstance()->getPlayerController()->isAttacking()) { return true; }
    return false;
}


//util functions
// ------------------------------------------------------------
/**
* substracts life and change state if needed
*/
void CBtEnemy::manageDamage(CProjectile *p){
    
    if(!lastNode.empty() && lastNode.compare("dead") == 0) return;

    if(lastNode.compare("cover") != 0 && p->getType() != SHIELD_BREAKER){
        //simulate impact to the entity
        owner->setPitch(deg2rad(-20.f));
        impactTime = GetTickCount();
    }

    switch(p->getType()){

        case NORMAL:
            if(lastNode.compare("cover") == 0){
                //substract a percentage of the total damage
                applyDamage( p->getDamage() * ENEMY_BLOCKING_DAMAGE_MULTIPLIER );
            }else{
                //substract total damage
                applyDamage( p->getDamage() );
            }
            break;

        case PUSHER:
            if(lastNode.compare("cover") == 0){
                //substract a percentage of the total damage
                applyDamage( p->getDamage() * ENEMY_BLOCKING_DAMAGE_MULTIPLIER );

                pushDirection = XMVector3Normalize( owner->pos - p->getOwner()->getPosition() );
                setCurrent(findNode("processPush"));
            }else{
                //substract total damage
                applyDamage( p->getDamage() );

                pushDirection = XMVector3Normalize( owner->pos - p->getOwner()->getPosition() );
                setCurrent(findNode("processPush"));
            }
            prev_ticks = 0;
            
            break;

        case SHIELD_BREAKER:
            if(lastNode.compare("cover") == 0){                
                //no damage with this type of projectile if we are in cover state
                setCurrent(findNode("processShieldBreak"));

            }else{
                //substract total damage
                applyDamage( p->getDamage() );

                //no damage with this type of projectile if we are in cover state
                setCurrent(findNode("processShieldBreak"));
            }
            prev_ticks = 0;
            break;
    }

    if(isDead()){
        setCurrent(findNode("dead"));
    }
}


bool CBtEnemy::rotateToPoint(float delta,XMVECTOR point, int rotationMultiplier){

  //face the player
  float alfa = owner->getAngleTo(point);
    float angleDelta = alfa * delta * rotationMultiplier;
    
    if(fabs(alfa) < deg2rad(0.1f) ){
        owner->setYaw(owner->getYaw() + alfa);
        return true;
    }else{
        owner->setYaw(owner->getYaw() + angleDelta);
    }
    return false;
}

/**
  * Update the position of the enemy  
  */
void CBtEnemy::updatePosition(XMVECTOR movementDisplacement, float delta){

}