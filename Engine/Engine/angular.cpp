#include "McvPlatform.h"
#include "Angular.h"

float deg2rad( float degrees ) { 
  return degrees * XM_PI / 180.0f; 
}

float rad2deg( float radians ) { 
  return radians * 180.0f / XM_PI;
}

XMVECTOR getVectorFromYaw( float yaw ) {
  return XMVectorSet( sin( yaw ), 0.0f, cos( yaw ), 0.0f );
}

float getYawFromVector( XMVECTOR v ) {
  return atan2( XMVectorGetX( v ), XMVectorGetZ( v ) );
}

float getAngleDifferenceTo( XMVECTOR src, float yaw, XMVECTOR p) {
  XMVECTOR dir = p - src;
  XMVECTOR front = getVectorFromYaw( yaw );
  XMVECTOR left  = getVectorFromYaw( yaw + XM_PIDIV2 );
  XMVECTOR cos_angle = XMVector3Dot( dir, front );
  XMVECTOR sin_angle = XMVector3Dot( dir, left );
  float angle = atan2( XMVectorGetX( sin_angle ), XMVectorGetX( cos_angle ) );
  return angle;
}

XMVECTOR getVectorFromYawPitch( float yaw, float pitch ) {
  return XMVectorSet( sin( yaw ) * cos( pitch )
                    ,              sin( pitch )
                    , cos( yaw ) * cos( pitch )
                    , 0.0f );
}

void getYawPitchFromVector( XMVECTOR v, float *yaw, float *pitch ) {
  *yaw = atan2( XMVectorGetX( v ), XMVectorGetZ( v ) );

  // Project v in the plane XZ
  XMVECTOR v_yzero = XMVectorSet( XMVectorGetX( v )
                          , 0.0f
                          , XMVectorGetZ( v )
                          , 0 );
  XMVECTOR mdo = XMVector3Length( v_yzero );
  *pitch = atan2( XMVectorGetY( v ), XMVectorGetX( mdo ) );
}

float pixelsToRads( float pixels ){

    // 20 pixel = 1 degrees
    float degrees = pixels * 0.05;
    return deg2rad(degrees);
}
