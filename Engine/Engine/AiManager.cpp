#include "McvPlatform.h"
#include "AiManager.h"
#include "AiZone.h"
#include "Utils.h"
#include "AiController.h"
#include "AiConstants.h"
#include "PlayerController.h"
#include "ProjectileManager.h"
#include "EntityManager.h"

//TODO: REMOVE ONCE IMPELEMENTED THE BT
#include "GraphicsController.h"
#include "IoStatus.h"


//Initialize the instace and the projectileManager pointer
CAiManager* CAiManager::pInstance = 0;

/**
 * Constructor
 */
CAiManager::CAiManager() : iaZones(NULL), currectZoneId(0), player(NULL) { }

/**
 * Destructor
 */
CAiManager::~CAiManager(){ }


/**
 *  Get the instance of the AI manager
 */
CAiManager* CAiManager::getInstance(){
	 //First call, then create the instance
	if(pInstance == 0){
		pInstance = new CAiManager;
	}
	return pInstance;
}

/**
 * Initialize the AI manager
 */
void CAiManager::init(CCamera* cam){
	//Create the first gameplay zone
	addZone(); 
	//Init the player controller
	if(player == NULL){
		player = new CPlayerController;
	}

	//Ground
	CEntity3D *groundEntity = CEntityManager::getInstance()->addEntity();
	groundEntity->setPosition( XMVectorSet( 0.f, 0.f, 0.f, 0.f ) );
	groundEntity->addMesh("ground");

	//Interruptor
	CEntity3D *interruptorEntity = CEntityManager::getInstance()->addEntity();
	interruptorEntity->setPosition( XMVectorSet( 0.f, 0.f, 0.f, 0.f ) );
	interruptorEntity->addMesh("interruptor");

	//Player
	CEntity3D *playerEntity = CEntityManager::getInstance()->addEntity();
	playerEntity->setPosition( XMVectorSet( 0.f, 0.f, 10.0f, 0.f ) );
	playerEntity->setYaw( 210 * 3.14f / 180.0f );
	playerEntity->addMesh("player");
	player->init(playerEntity, cam);

	// Boss
	/*CEntity3D *enemyEntityBoss = CEntityManager::getInstance()->addEntity();
	enemyEntityBoss->setPosition( XMVectorSet( 0.f, 0.f, -11.0f, 0.f ) );
	enemyEntityBoss->addMesh("enemy");
	enemyEntityBoss->addMesh("enemyBossCombatArea");
	enemyEntityBoss->addMesh("enemyBossInvationArea");

	btEnemyBoss.create("enemyBoss", enemyEntityBoss, playerEntity);
	addAiControllerToZone(0, &btEnemyBoss);*/





	//Bot 1
	CEntity3D *enemyEntity = CEntityManager::getInstance()->addEntity();
	enemyEntity->setPosition( XMVectorSet( 0.f, 0.f, -9.0f, 0.f ) );
	enemyEntity->setYaw( XM_PI );
	enemyEntity->addMesh("enemy");
	enemyEntity->addMesh("enemyPerceiveArea");
	btEnemy.create("enemy", enemyEntity, playerEntity);
	addAiControllerToZone(0, &btEnemy);
	/*
	//Bot 2
	CEntity3D *enemyEntity2 = CEntityManager::getInstance()->addEntity();
	enemyEntity2->setPosition( XMVectorSet( 0.f, 0.f, -2.0f, 0.f ) );
	enemyEntity2->addMesh("enemy");		
	btEnemy2.create("enemy2", enemyEntity2, playerEntity);
	addAiControllerToZone(0, &btEnemy2);
	
	//Bot 3
	CEntity3D *enemyEntity3 = CEntityManager::getInstance()->addEntity();
	enemyEntity3->setPosition( XMVectorSet( -4.f, 0.f, -2.f, 0.f ) );
	enemyEntity3->addMesh("enemy");		
	btEnemy3.create("enemy3", enemyEntity3, playerEntity);
	addAiControllerToZone(0, &btEnemy3);*/

	//Bot 2
	/*CEntity3D *enemyEntity2 = CEntityManager::getInstance()->addEntity();
	CEnemyController *enemy2 = new CEnemyController;
	enemyEntity2->setPosition( XMVectorSet( 3.f, 0.f, 2.0f, 0.f ) );
	enemyEntity2->setYaw( 210 * 3.14f / 180.0f );
	enemyEntity2->addMesh("enemy");
	enemyEntity2->addMesh("enemyPerceiveArea");
	enemy2->init(enemyEntity2, playerEntity);
	addAiControllerToZone(0, enemy2);
	*/
	//TODO: INITIT CORRECTLY ONCE IMPELEMENTED THE BT 
	//Init the behaviour tree
	/*btEnemyEntity.setPosition( XMVectorSet( 0.f, 0.f, 9.0f, 0.f ) );
	btEnemyEntity.addMesh("enemy");
	btEnemy.create("enemy", &btEnemyEntity, playerEntity);*/
}


/**
 * Add the new zone
 * zoneId: Return with the id of the zone
 */
int CAiManager::addZone(){
	CAiZone *newZone = new CAiZone;
	iaZones.push_back(newZone);

	int zoneId = (int) iaZones.size() - 1; 
	return zoneId;
}

/**
 * Add the IA controller to the zone 
 */
void CAiManager::addAiControllerToZone(int zoneId, bt* iaController){
	//Invalid zoneid
	assert(zoneId <= 0);
	assert(zoneId >= iaZones.size() - 1);

	//Add the IA to the zone
	iaZones[zoneId]->addController(iaController);
}

/**
 * Return with the current zone controller
 */
vector<bt *> CAiManager::getCurrentZoneController(){
	return iaZones[0]->getControllers();
}


/**
 * Recalculate the active IAs, projectiles, hits and players
 */
void CAiManager::update(float delta){


	// TODO: do not forget the colon
	//  set the active zone
	iaZones[0]->update(delta);

	//Recalcule the player
	player->update(delta);

	//Update the projectiles
	CProjectileManager::getInstance()->update();

	//Calculate the hits
	computeHits();

}

/**
 * Compute the hits
 */
void CAiManager::computeHits(){

	//For each in projectiles
	vector<CProjectile *> projectiles = CProjectileManager::getInstance()->getProjectiles();
	for (vector<CProjectile *>::iterator projectileIter = projectiles.begin() ; projectileIter != projectiles.end(); ++projectileIter){
		CProjectile* projectile = ( *projectileIter );
		
		//1. Projectile vs player
		if(projectile->isInside( player->getPosition()) && (projectile->getOwner() != player->getOwner()) ){

			player->manageDamage(projectile);
			projectile->setActive(false);			
			//pushState(encode_aid(current_ai_zone, i), "IMPACT", optional_argument);
		}

		//2. Projectile vs enemies
		// For each in AI on the currect zone
		vector<bt *> aiList =  iaZones[ currectZoneId ]->getControllers();
		for ( vector<bt *>::iterator aiIter = aiList.begin(); aiIter != aiList.end();  ++aiIter) {

			//If any enemy inside the projectile
			bt* ai = *aiIter;
			if(ai->isActive() && projectile->isInside( ai->getPosition()) && (projectile->getOwner() != ai->getOwner()) ){
				
				ai->manageDamage(projectile);
				projectile->setActive(false);
				//pushState(encode_aid(current_ai_zone, i), "IMPACT", optional_argument);
			}
		}
	}
}

/**
 *  Determines occured any collision at the entityPosition. 
 *  If occured any collision, then calculate the collisionDisplacement
 */
XMVECTOR CAiManager::hasCollide(XMVECTOR nextEntityPosition, CEntity3D* entity, float collisionRadious, XMVECTOR movementDisplacement){ 
	//Get the current zone controller
	XMVECTOR newMovement = movementDisplacement;
	vector<bt *> controllers = iaZones[currectZoneId]->getControllers();
	
	for (vector<bt *>::iterator ctrlIter = controllers.begin() ; ctrlIter != controllers.end(); ++ctrlIter){
		bt* controller = *ctrlIter;
		//only check for active enemies
		if(!controller->isActive()){ continue; }
		
		//If collide the objects, then calculate the collision vector
		if(isCollided( nextEntityPosition , collisionRadious , controller-> getPosition() , collisionRadious)){
			
			//Calculate well the collision vector
			newMovement = calculateCollisionVector(player->getOwner(), movementDisplacement, controller->getOwner());
			break;
		}
	}
	return newMovement;
}

/**
 * TODO Correct the function because it doesn't WORKS
 * Calculate the collision vector
 */
XMVECTOR CAiManager::calculateCollisionVector(CEntity3D* entity1, XMVECTOR speed_entity1, CEntity3D* entity2){
	float mass_entity1 = 1.f;
	float mass_entity2 = 1.f;

	XMVECTOR diff = entity1->getPosition() - entity2->getPosition();
	float alfa  = atan2(  XMVectorGetZ(diff), XMVectorGetX(diff));

	float v1x = XMVectorGetX(speed_entity1) * cos(XMVectorGetX(entity1->getPosition()) - alfa);
	float v1y = XMVectorGetZ(speed_entity1) * sin(XMVectorGetX(entity1->getPosition()) - alfa);
	float v2x = 0 * cos( XMVectorGetX(entity2->getPosition()) - alfa);
	float v2y = 0 * sin(XMVectorGetX(entity2->getPosition()) - alfa);

	//Calculate the collision displacement vector
	float newVelX1 = ( v1x * (mass_entity1 - mass_entity2) + (2 * mass_entity2 * v2x)) / (mass_entity1 + mass_entity2);
	float newVelZ1 = ( v1y * (mass_entity1 - mass_entity2) + (2 * mass_entity2 * v2y)) / (mass_entity1 + mass_entity2);
	float newVelX2 = ( v2x * (mass_entity2 - mass_entity1) + (2 * mass_entity1 * v1x)) / (mass_entity1 + mass_entity2);
	float newVelZ2 = ( v2y * (mass_entity2 - mass_entity1) + (2 * mass_entity1 * v1y)) / (mass_entity1 + mass_entity2);

	//Save the collision vector
	XMVECTOR collisionDisplacement;
	collisionDisplacement = XMVectorSet(newVelX1, 0., newVelZ1, 0.f);
	return collisionDisplacement;
}

/**
 * Determine if occured collision or not
 */
bool CAiManager::isCollided(XMVECTOR posEntity1, float radEntity1, XMVECTOR posEntity2, float radEntity2){
	bool isCollisioned;
	XMVECTOR distance = posEntity1 - posEntity2; 
	float dist = XMVectorGetY( XMVector3Length(distance) ); 

	//If the two object collesioned
	if(fabs( dist )  <  radEntity1 + radEntity2){
		isCollisioned = true;
	}
	//If the two object NOT collesioned
	else{
		isCollisioned = false;
	}
	return isCollisioned;
}

void CAiManager::destroy(){
	//Destroy the aiZone
	for ( size_t i = 0; i < iaZones.size(); i++ ) {
		iaZones[i]->destroy();
		delete iaZones[i];
	}
	iaZones.clear();

	//Destroy the player
	player = NULL;
	delete player;

	//Destroy the projectile manager
	CProjectileManager::getInstance()->destroy();

	//Destroy the instance
	pInstance = NULL;
	delete pInstance;
}