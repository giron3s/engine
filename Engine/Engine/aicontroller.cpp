#include "McvPlatform.h"
#include "AiController.h"

void CAiController::init(){}

void CAiController::update(float delta){
    //to be removed (simulate impact)
    if(impactTime != 0 && (GetTickCount() - impactTime)/1000.f > 0.1f){
        player->setPitch(0.0f);
        impactTime = 0;
    }


    // this is a trusted jump as we've tested for coherence in ChangeState
    (this->*statemap[state])(delta);
}

void CAiController::changeState(std::string newstate){
    // try to find a state with the suitable name
    if (statemap.find(newstate) == statemap.end()){
        // the state we wish to jump to does not exist. we abort
        exit(-1);
    }
    state=newstate;
}

void CAiController::addState(std::string name, statehandler sh){
    // try to find a state with the suitable name
    if (statemap.find(name) != statemap.end()){
        // the state we wish to jump to does exist. we abort
        exit(-1);
    }
    statemap[name]=sh;
}
