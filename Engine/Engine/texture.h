#ifndef INC_TEXTURE_H_
#define INC_TEXTURE_H_

#include "McvPlatform.h"

class CTexture {
public:
  ID3D11ShaderResourceView*  texture_dx11;
  CTexture( );
  void destroy( );
  bool load( const char *filename );
};

#endif
