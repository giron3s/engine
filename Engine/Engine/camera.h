#ifndef INC_CAMERA_H_
#define INC_CAMERA_H_

#include "McvPlatform.h"
#include "Mesh.h"

enum CamType {
    COMBAT,
    FOLLOW,
    DEV,
    CAMS_COUNT
};

class CCamera {


  // View info
  XMVECTOR pos;
  XMVECTOR target;
  XMVECTOR up_aux;        // Defaults to 0,1,0
  XMVECTOR front, left, up;
  XMMATRIX view;
  CamType camType;

  void updateViewMatrix();
    
  // Prespective info
  float    fov;           // en radianes
  float    aspect_ratio;  // w / h
  float    znear, zfar;
  XMMATRIX projection;
  void updateProjectionMatrix();

  XMMATRIX view_projection;

public:
  CCamera();
  void lookAt( XMVECTOR new_pos, XMVECTOR new_target );
  void setProjectionParams( float new_fov, float new_aspect_ratio
                          , float new_znear, float new_zfar );

  const XMMATRIX getView()       const { return view; }
  const XMMATRIX getProjection() const { return projection; }
  const XMMATRIX getViewProjection() const { return view_projection; }
  
  XMVECTOR getPosition() const { return pos; }
  XMVECTOR getLeft() const { return left; }
  XMVECTOR getFront() const { return front; }
  XMVECTOR getUp() const { return up; }

  const CamType  getCamType() const { return camType; }
  void setCamType(CamType camType);

  void strafe(XMVECTOR _pos);
  void setFront(XMVECTOR _front);

  // Returns true if the world_pos is inside the z range of the view frustum 
  bool getScreenCoords( XMVECTOR world_pos, float *x, float *y ) const;

};

extern CCamera camera_1;
extern CCamera camera_2;

#endif

