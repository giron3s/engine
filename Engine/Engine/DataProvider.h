#ifndef INC_DATA_PROVIDER_H_
#define INC_DATA_PROVIDER_H_

#include "McvPlatform.h"

class CDataProvider {
  public:
    virtual ~CDataProvider() { }
    virtual bool isValid() const = 0;
    virtual void read( void *dest_buffer, size_t nbytes ) = 0;
    
    template< class TPOD >
    void read( TPOD &p ) {
      read( &p, sizeof( TPOD ) );
    }
};

class CFileDataProvider : public CDataProvider {
  FILE *f;
  public:
    CFileDataProvider( const char *filename ) {
      f = fopen( filename, "rb" );
    }
    ~CFileDataProvider( ) {
      if ( f ) { fclose( f ), f = NULL; }
    }
    bool isValid() const { return f != NULL; }
    void read( void *dest_buffer, size_t nbytes ) {
      assert( isValid() );
      assert( dest_buffer );
      size_t nbytes_read = fread( dest_buffer, 1, nbytes, f );
      assert( nbytes_read == nbytes );
    }
};

#endif