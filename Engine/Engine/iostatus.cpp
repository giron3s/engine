#include "McvPlatform.h"
#include "IoStatus.h"
#include "Utils.h"

// XInput for the XBOX controller
// http://msdn.microsoft.com/en-us/library/windows/desktop/ee417001(v=vs.85).aspx

CIOStatus io;

CIOStatus::CIOStatus( ) {

  buttons[ START ].key = '1';
  
  //shift
  buttons[ CIRCLE ].key = 16;

  //control
  buttons[ CROSS ].key = 17;
  //space
  buttons[ TRIANGLE ].key = ' ';

  //tab
  buttons[ LB ].key = 9;
  buttons[ LT ].key = 'Z';
  buttons[ RT ].key = 'E';

  buttons[ DIGITAL_LEFT].key = 'A';
  buttons[ DIGITAL_RIGHT].key = 'D';
  buttons[ DIGITAL_UP].key = 'W';
  buttons[ DIGITAL_DOWN].key = 'S';

}

void CIOStatus::TButton::setPressed( bool how, float elapsed ) {

  is_pressed = how;

  // The key was and is pressed
  if( was_pressed && is_pressed )
    time_pressed += elapsed;

  // The key was and is NOT pressed anymore -> has been released
  if( was_pressed && !is_pressed )
    time_released = 0.f;
   
  // ... 
  if( !was_pressed && !is_pressed )
    time_released += elapsed;
  
  // The key was NOT pressed and now IS pressed -> has been pressed
  if( !was_pressed && is_pressed ) 
    time_pressed = 0.f;

}

// ---------
void CIOStatus::update( float elapsed ) {
    
  //mouse
  mouse.update();

  // update buttons
  for( int i=0; i<BUTTONS_COUNT; ++i ) {
    TButton &b = buttons[ i ];

    b.was_pressed = b.is_pressed;
    
    bool now_is_pressed = false;
    // Is the key right now pressed?
    if( b.key ){
      now_is_pressed = (::GetAsyncKeyState( b.key ) & 0x8000 ) != 0;
    }
    // if( xinput_state.buttons & b.xflag ) now_is_pressed |= true;

    /*if( i == DIGITAL_LEFT ){
      dbg( "DOWN is pressed %d\n",now_is_pressed );
    }*/

    if( !(i == MOUSE_LEFT || i == MOUSE_RIGHT || i == MOUSE_MIDDLE) ){
        
      b.setPressed( now_is_pressed, elapsed );
    }

    /*if( i == 0 ){
      dbg( "X is pressed %d\n", b.is_pressed );
    }*/
  }

  // update joysticks. Check the XInput lib
}

void CIOStatus::TMouse::update( ) {

  if( !App.has_focus ) {
    dx = dy = 0;
    return;
  }

  POINT mid_screen = { App.xres / 2, App.yres / 2 };
  ::ClientToScreen( App.hWnd, &mid_screen );

  POINT cursor_screen;
  ::GetCursorPos( &cursor_screen );

  dx = cursor_screen.x - mid_screen.x;
  dy = cursor_screen.y - mid_screen.y;


  ::SetCursorPos( mid_screen.x, mid_screen.y );

  /*
  prev_x = screen_x;
  prev_y = screen_y;

  screen_x = last_os_x;
  screen_y = last_os_y;

  dx = screen_x - prev_x;
  dy = screen_y - prev_y;
  */
  /*if( dx != 0 || dy != 0 )
    dbg( "TMouse::update %d,%d\n", dx, dy );*/

 

  //last_os_x = App.xres / 2;
  //last_os_y = App.yres / 2;
}

