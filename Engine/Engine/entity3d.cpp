#include "McvPlatform.h"
#include "Entity3d.h"
#include "GraphicsController.h"

CEntity3D::CEntity3D( ) : yaw( 0.0f ), pitch( 0.0f ){ 
  pos = XMVectorSet( 0,0,0,0 );
}

/**
 * Add the mesh to the entity
 */
bool CEntity3D::addMesh(string meshKey){
	CRender* renderMesh = CGraphicsController::getInstance()->getMesh(meshKey);
	if(renderMesh != NULL){
		meshes[meshKey] = renderMesh;
		return true;
	}
	return false;
}

/**
 * Remove mesh from the entity
 */
bool CEntity3D::removeMesh(string meshKey){
	//If the mesh not exist
	if(meshes.find(meshKey) == meshes.end()){
		return false;
	}
	else{
		meshes.erase ( meshKey );
		return true;
	}
}

void CEntity3D::updateWorld() {
  world =  XMMatrixRotationRollPitchYaw(pitch, yaw, 0.f) * XMMatrixTranslationFromVector( pos );
}

void CEntity3D::setYaw( float new_yaw ) {
  yaw = new_yaw;
  updateWorld( );
}

void CEntity3D::setPitch( float new_pitch ){
  pitch = new_pitch;
  updateWorld( );
}

void CEntity3D::setPosition( XMVECTOR new_pos ) {
  pos = new_pos;
  updateWorld( );
}

float CEntity3D::getAngleTo( XMVECTOR p) const {
  return getAngleDifferenceTo( pos, yaw, p );
}

bool CEntity3D::isInsideCone( XMVECTOR p, float cone_angle, float cone_distance) {
  float angleDiv2 = cone_angle / 2;

  bool insideConeAngle, insideConeDistance;
  XMVECTOR dir = XMVector3Normalize(p - pos);
  XMVECTOR front = XMVector3Normalize(getFront());

  XMVECTOR dot = XMVector3Dot( dir, front );
  insideConeAngle = XMVectorGetX( dot ) > cos( deg2rad(angleDiv2));

  XMVECTOR distance = XMVector3Length(p - pos);
  insideConeDistance = XMVectorGetX(distance) < cone_distance;
  return insideConeAngle && insideConeDistance;
}

bool CEntity3D::isInsideCircle(XMVECTOR p, float radius){
  XMVECTOR distance = XMVector3Length(p - pos);
  return XMVectorGetX(distance) < radius;
}

bool CEntity3D::isPerceived(XMVECTOR p, float visionConeAngle, float visionConeDistance, float hearRadius){
	return isInsideCone(p,visionConeAngle,visionConeDistance) || isInsideCircle(p,hearRadius);
}

bool CEntity3D::isInFront( XMVECTOR p) const {
  XMVECTOR dir = p - pos;
  XMVECTOR dot = XMVector3Dot( dir, getFront() );
  return XMVectorGetX( dot ) > 0.0f;
}

bool CEntity3D::isInLeft( XMVECTOR p) const {
  XMVECTOR dir = p - pos;
  XMVECTOR dot = XMVector3Dot( dir, getLeft() );
  return XMVectorGetX( dot ) > 0.0f;
}