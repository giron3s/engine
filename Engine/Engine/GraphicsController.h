#ifndef _GRAPHICS_CONTROLLER_
#define _GRAPHICS_CONTROLLER_

#include "McvPlatform.h"
#include "Render.h"

#include <string>
#include <map>

using namespace std;

class CGraphicsController{
	private:
	//Instance of the class
	static CGraphicsController*   pInstance;

	//List of the meshes
	map<string, CRender* >        meshes;

	CVertexShader                *vsSolid;
	CPixelShader                 *psSolid;
	CVertexShader                *vsTextured;
	CPixelShader                 *psTextured;

	CPixelShader                 *psSolidMaskVision;
	CPixelShader                 *psTexturedMaskVision;

public:
	CGraphicsController();
	~CGraphicsController();
	static CGraphicsController* getInstance();

	void init();
	CRender* getMesh(string meshName);
	void getMesh(); 
	void destroy();

private:
	void loadShaders();
	void loadPrimitiveMesh();
	void loadComplexMesh();
};
#endif