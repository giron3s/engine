#include "McvPlatform.h"
#include "mesh.h"
#include "shader.h"
#include "utils.h"
#include "VertexDeclarations.h"

/**
 * Costructor of the class
 */
CRenderMesh::CRenderMesh() : vb( NULL ), ib( NULL ), primitive_type( D3D_PRIMITIVE_TOPOLOGY_UNDEFINED ), nvertexs( 0 ), nindices( 0 ), bytes_per_vertex( 0 )
{ }

/**
 * Destroy of the class
 */
void CRenderMesh::destroy( ) {
  if( vb ) vb->Release(), vb = NULL;
  if( ib ) ib->Release(), ib = NULL;
}

bool CRenderMesh::createRawMesh( const void *  new_vertexs, unsigned new_nvertexs, unsigned new_nbytes_per_vertex, const TIndex *new_indices
								, unsigned     new_nindices , ePrimitiveType new_type , const CVertexDeclaration *adecl  ) {

  assert( vb == NULL );

  // Save params
  nvertexs = new_nvertexs;
  nindices = new_nindices;
  bytes_per_vertex = new_nbytes_per_vertex;
  vertex_decl = adecl;

  // Translate primitive type from our system to dx
  switch( new_type ) {
  case POINT_LIST: primitive_type = D3D11_PRIMITIVE_TOPOLOGY_POINTLIST; break;
  case LINE_LIST:  primitive_type = D3D11_PRIMITIVE_TOPOLOGY_LINELIST; break;
  case TRIANGLE_LIST: primitive_type = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST; break;
  case LINE_STRIP: primitive_type = D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP; break;
  default:
    /// error!!!
    break;
  }
  
  HRESULT hr;

  // Create the vertex buffer
  D3D11_BUFFER_DESC bd;
  ZeroMemory( &bd, sizeof(bd) );
  bd.Usage = D3D11_USAGE_DEFAULT;
  bd.ByteWidth = new_nbytes_per_vertex * new_nvertexs;
  bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
  bd.CPUAccessFlags = 0;
  D3D11_SUBRESOURCE_DATA InitData;
  ZeroMemory( &InitData, sizeof(InitData) );
  InitData.pSysMem = new_vertexs;
  hr = App.d3dDevice->CreateBuffer( &bd, &InitData, &vb );
  if( FAILED( hr ) )
    return false;

  if( new_nindices > 0 ) {
    assert( new_indices );

  // Create index buffer
  bd.Usage = D3D11_USAGE_DEFAULT;
  bd.ByteWidth = sizeof( TIndex ) * new_nindices;
  bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
  bd.CPUAccessFlags = 0;
  InitData.pSysMem = new_indices;
  hr = App.d3dDevice->CreateBuffer( &bd, &InitData, &ib );
  if( FAILED( hr ) )
    return false;
  } else {
    ib = NULL;
  }

  return true;
}

// -----------------------------------------------------
void CRenderMesh::render( ) const {

  assert( vb != NULL );

  // El vertex shader activo tiene que usar la misma vertex
  // declaration que estoy mandando yo
  assert( CVertexShader::current->vertex_decl == vertex_decl );
 
  // Set vertex buffer
  UINT stride = bytes_per_vertex;
  UINT offset = 0;
  App.immediateContext->IASetVertexBuffers( 0, 1, &vb, &stride, &offset );

  // Set primitive topology
  App.immediateContext->IASetPrimitiveTopology( primitive_type );
  
  if( ib ) {
    // Set index buffer
    DXGI_FORMAT fmt = (sizeof( TIndex ) == 2 ) ? DXGI_FORMAT_R16_UINT : DXGI_FORMAT_R32_UINT;
    App.immediateContext->IASetIndexBuffer( ib, fmt, 0 );

    // Set primitive topology
    App.immediateContext->IASetPrimitiveTopology( primitive_type );
  
    App.immediateContext->DrawIndexed( nindices, 0, 0 );
  } else {
    // A non-indexed primitive
    App.immediateContext->Draw( nvertexs, 0 );
  }
}

// ------------------------------------------------
bool CRenderMesh::load( CDataProvider &dp ) {
	//Load the header
	THeader h;
	dp.read( h );
	if( !h.isValid() ) {
		dbg( "RenderMesh header is not valid\n" );
		return false;
	}

	// Convertir el h.vertex_type en el equivalente
	// vertex_declaration
	const CVertexDeclaration* file_vertex_decl = NULL;
	if( h.vertex_type == 1000 )
		file_vertex_decl = CVertexTextured::getDecl();
	else {
		dbg( "Invalid vertex type %d read from file\n", h.vertex_type );
		return false;
	}

	// read indices
	// ...

	// read vertexs
	size_t bytes_for_vtxs = h.bytes_per_vertex * h.nvertexs;

	// Pedir memoria para los vertices
	u8 *vtxs = new u8[ bytes_for_vtxs ];

	// Leerlos del dp
	dp.read( vtxs, bytes_for_vtxs );

	bool is_ok = createRawMesh( vtxs, h.nvertexs, h.bytes_per_vertex, NULL, h.nindices, TRIANGLE_LIST, file_vertex_decl );

	delete [] vtxs;

	return is_ok;
}