#ifndef _AI_CONSTANTS_H_
#define _AI_CONSTANTS_H_
/*-----------------------GENERAL constants----------------------*/
static const float AVATAR_COLLISION_RADIOUS					=  0.25f;

//PLAYER
static const float PLAYER_PROJECTILE_RADIOUS				=  4.0f;
static const float PLAYER_TARGET_DISTANCE					=  20.f;
static const float PLAYER_UNTARGET_DISTANCE					=  25.f;

static const float PLAYER_TARGET_SPEED						=  10.f;
static const float PLAYER_BLOCKING_SPEED					=  2.f;
static const float PLAYER_BLOCKING_DAMAGE_MULTIPLIER		=  0.2f;
static const float PLAYER_WALK_SPEED						=  4.f;
static const float PLAYER_RUN_SPEED							=  10.f;
static const float PLAYER_ORBIT_SPEED						=  3.f;

static const float PLAYER_DODGE_DURATION_SECS				=  0.3f;
static const float PLAYER_DODGE_DISTANCE					=  3.f;

static const float PLAYER_SHIELD_BREAK_DURATION_SECS		=  2.f;
static const float PLAYER_PUSHED_DURATION_SECS				=  0.2f;
static const float PLAYER_PUSHED_DISTANCE					=  3.f;
 
 
//ATTACK CONSTANTS
//kick
static const float PLAYER_KICK_DURATION_SECS				=  0.5f;
static const float PLAYER_KICK_HIT_DURATION_SECS			=  0.2f;
static const float PLAYER_KICK_HIT_DELAY_SECS				=  0.2f;
static const float PLAYER_KICK_DAMAGE						=  0.1;
 
//left
static const float PLAYER_ATTACK_LEFT1_DURATION_SECS		=  0.5f;
static const float PLAYER_ATTACK_LEFT1_HIT_DURATION_SECS	=  0.1f;
static const float PLAYER_ATTACK_LEFT1_HIT_DELAY_SECS		=  0.2f;
static const float PLAYER_ATTACK_LEFT1_DAMAGE				=  10;
 
static const float PLAYER_ATTACK_LEFT2_DURATION_SECS		=  0.2f;
static const float PLAYER_ATTACK_LEFT2_HIT_DURATION_SECS	=  0.1f;
static const float PLAYER_ATTACK_LEFT2_HIT_DELAY_SECS		=  0.1f;
static const float PLAYER_ATTACK_LEFT2_DAMAGE				=  10;
 
//right
static const float PLAYER_ATTACK_RIGHT1_DURATION_SECS		=  0.8f;
static const float PLAYER_ATTACK_RIGHT1_HIT_DURATION_SECS	=  0.3f;
static const float PLAYER_ATTACK_RIGHT1_HIT_DELAY_SECS		=  0.2f;
static const float PLAYER_ATTACK_RIGHT1_DAMAGE				=  10;
 
static const float PLAYER_ATTACK_RIGHT2_DURATION_SECS		=  0.8f;
static const float PLAYER_ATTACK_RIGHT2_HIT_DURATION_SECS	=  0.3f;
static const float PLAYER_ATTACK_RIGHT2_HIT_DELAY_SECS		=  0.2f;
static const float PLAYER_ATTACK_RIGHT2_DAMAGE				=  10;

//up
static const float PLAYER_ATTACK_UP1_DURATION_SECS			=  1.0f;
static const float PLAYER_ATTACK_UP1_HIT_DURATION_SECS		=  0.3f;
static const float PLAYER_ATTACK_UP1_HIT_DELAY_SECS			=  0.2f;
static const float PLAYER_ATTACK_UP1_DAMAGE					=  10;

static const float PLAYER_ATTACK_UP2_DURATION_SECS			=  0.8f;
static const float PLAYER_ATTACK_UP2_HIT_DURATION_SECS		=  0.3f;
static const float PLAYER_ATTACK_UP2_HIT_DELAY_SECS			=  0.2f;
static const float PLAYER_ATTACK_UP2_DAMAGE					=  10;

//down
static const float PLAYER_ATTACK_DOWN1_DURATION_SECS		=  0.4f;
static const float PLAYER_ATTACK_DOWN1_HIT_DURATION_SECS	=  0.1f;
static const float PLAYER_ATTACK_DOWN1_HIT_DELAY_SECS		=  0.1f;
static const float PLAYER_ATTACK_DOWN1_DAMAGE				=  10;

static const float PLAYER_ATTACK_DOWN2_DURATION_SECS		=  0.5f;
static const float PLAYER_ATTACK_DOWN2_HIT_DURATION_SECS	=  0.1f;
static const float PLAYER_ATTACK_DOWN2_HIT_DELAY_SECS		=  0.2f;
static const float PLAYER_ATTACK_DOWN2_DAMAGE				=  10;

//basic
static const float PLAYER_ATTACK_BASIC_DURATION_SECS		=  0.3f;
static const float PLAYER_ATTACK_BASIC_HIT_DURATION_SECS	=  0.1f;
static const float PLAYER_ATTACK_BASIC_HIT_DELAY_SECS		=  0.1f;
static const float PLAYER_ATTACK_BASIC_DAMAGE				=  10;


//-------------------------------------
//-------------  ENEMY  ---------------
//-------------------------------------

//PERCEIVE CONSTANTS
static const float ENEMY_TARGET_RADIOUS						=  0.8f; 

static const float ENEMY_HEARING_RADIUS						=  6.f;
static const float ENEMY_VISION_ANGLE						=  90.0f;
static const float ENEMY_VISION_DISTANCE					=  10.0f;

//PROPERTY CONSTANTS
static const float ENEMY_LINEAR_SPEED						=  1.5f;
static const float ENEMY_ANGULAR_SPEED_RADS					=  50.f*XM_PI /180.f;
static const float ENEMY_ANGULAR_SPEED_MULTIPLIER			=  1.5f;
static const float ENEMY_REST_DURATION_SECS				    =  2.f;

static const float ENEMY_CHASE_LINEAR_SPEED					=  3.f;
static const float ENEMY_CHASE_ANGULAR_SPEED				=  180.f*XM_PI /180.f;
static const float ENEMY_CHASE_ANGULAR_SPEED_MULTIPLIER		=  3.f;
static const float ENEMY_FAST_ANGULAR_SPEED_MULTIPLIER		=  10.f;

static const float ENEMY_SUSPICIOUS_DURATION				=  6.f;

static const float ENEMY_STEP_SPEED							=  3.f;
//static const float ENEMY_STEPBACK_SPEED					=  1.5f;
static const float ENEMY_STEP_DURATION					    =  0.3f;
static const float ENEMY_STEPBACK_DURATION					=  0.15f;
static const float ENEMY_STEPFORWARD_LIMIT				    =  3.7f;
static const float ENEMY_STEPBACK_LIMIT	            	    =  2.f;

static const float ENEMY_ORBIT_SPEED	            	    =  1.f;
static const float ENEMY_ORBIT_DURATION_SECS           	    =  0.5f;
static const int   ENEMY_CONTINUE_ORBIT_PROB				=  45;

static const float ENEMY_PROJECTILE_RADIOUS					=  4.0f;


//COMBAT CONSTANTS
//--attack
//basicAttack
static const float ENEMY_BATTACK_DURATION_SECS				=  1.6f;
static const float ENEMY_BATTACK_HIT_DURATION_SECS			=  0.1f;
static const float ENEMY_BATTACK_HIT_DELAY_SECS				=  0.3f;
static const float ENEMY_BATTACK_DAMAGE						=  10;

//kick
static const float ENEMY_KICK_DURATION_SECS					=  0.6f;
static const float ENEMY_KICK_HIT_DURATION_SECS				=  0.1f;
static const float ENEMY_KICK_HIT_DELAY_SECS				=  0.3f;
static const float ENEMY_KICK_DAMAGE						=  1;

static const float ENEMY_MIN_KICK_PROB						=  5;
static const float ENEMY_MAX_KICK_PROB						=  50;

//--cover
static const float ENEMY_BLOCKING_DAMAGE_MULTIPLIER			=  0.05f;

//--combos
static const float ENEMY_COMBO_SPEED						=  5.f;

//light
static const float ENEMY_PREPARING_LIGHT_DURATION_SECS		=  0.5f;
static const float ENEMY_LIGHT_3HITS_DURATION_SECS			=  1.8f;
static const float ENEMY_LIGHT_2HITS_DURATION_SECS			=  1.f;

static const float ENEMY_LIGHT_COMBOWINDOW1_DELAY_PERCENT	=  0.f;
static const float ENEMY_LIGHT_COMBOWINDOW1_DURATION_PERCENT		=  0.3f;
static const float ENEMY_LIGHT_FIRST_HIT_DELAY_PERCENT		=  0.05f;
static const float ENEMY_LIGHT_FIRST_HIT_DURATION_SECS		=  0.2f;
static const float ENEMY_LIGHT_FIRST_HIT_DAMAGE				=  5.f;

static const float ENEMY_LIGHT_COMBOWINDOW2_DELAY_PERCENT	=  0.4f;
static const float ENEMY_LIGHT_COMBOWINDOW2_DURATION_PERCENT		=  0.2f;
static const float ENEMY_LIGHT_SECOND_HIT_DELAY_PERCENT		=  0.5f;
static const float ENEMY_LIGHT_SECOND_HIT_DURATION_SECS		=  0.2f;
static const float ENEMY_LIGHT_SECOND_HIT_DAMAGE			=  5.f;

static const float ENEMY_LIGHT_COMBOWINDOW3_DELAY_PERCENT	=  0.7f;
static const float ENEMY_LIGHT_COMBOWINDOW3_DURATION_PERCENT		=  0.2f;
static const float ENEMY_LIGHT_THIRD_HIT_DELAY_PERCENT		=  0.8f;
static const float ENEMY_LIGHT_THIRD_HIT_DURATION_SECS		=  0.2f;
static const float ENEMY_LIGHT_THIRD_HIT_DAMAGE				=  10.f;

static const float ENEMY_LIGHT_FACE_PLAYER_DELAY_PERCENT	=  0.9f;

//strong
static const float ENEMY_PREPARING_STRONG_DURATION_SECS		=  0.9f;
static const float ENEMY_STRONG_3HITS_DURATION_SECS			=  2.2f;
static const float ENEMY_STRONG_2HITS_DURATION_SECS			=  1.4f;

static const float ENEMY_STRONG_COMBOWINDOW1_DELAY_PERCENT	=  0.f;
static const float ENEMY_STRONG_COMBOWINDOW1_DURATION_PERCENT		=  0.3f;
static const float ENEMY_STRONG_FIRST_COMBO_WINDOW_PERCENT	=  0.f;
static const float ENEMY_STRONG_FIRST_HIT_DELAY_PERCENT		=  0.05f;
static const float ENEMY_STRONG_FIRST_HIT_DURATION_SECS		=  0.2f;
static const float ENEMY_STRONG_FIRST_HIT_DAMAGE			=  10.f;

static const float ENEMY_STRONG_COMBOWINDOW2_DELAY_PERCENT	=  0.4f;
static const float ENEMY_STRONG_COMBOWINDOW2_DURATION_PERCENT		=  0.2f;
static const float ENEMY_STRONG_SECOND_COMBO_WINDOW_PERCENT	=  0.4f;
static const float ENEMY_STRONG_SECOND_HIT_DELAY_PERCENT	=  0.5f;
static const float ENEMY_STRONG_SECOND_HIT_DURATION_SECS	=  0.2f;
static const float ENEMY_STRONG_SECOND_HIT_DAMAGE			=  20.f;

static const float ENEMY_STRONG_COMBOWINDOW3_DELAY_PERCENT	=  0.7f;
static const float ENEMY_STRONG_COMBOWINDOW3_DURATION_PERCENT		=  0.2f;
static const float ENEMY_STRONG_THIRD_COMBO_WINDOW_PERCENT	=  0.7f;
static const float ENEMY_STRONG_THIRD_HIT_DELAY_PERCENT		=  0.8f;
static const float ENEMY_STRONG_THIRD_HIT_DURATION_SECS		=  0.2f;
static const float ENEMY_STRONG_THIRD_HIT_DAMAGE			=  20.f;

static const float ENEMY_STRONG_FACE_PLAYER_DELAY_PERCENT	=  0.9f;


//--push / shieldbreak
static const float ENEMY_PUSHED_DURATION_SECS				=  1.f;
static const float ENEMY_SHIELD_BREAK_DURATION_SECS			=  1.f;
static const float ENEMY_PUSHED_SPEED						=  3.f;

static const float ENEMY_IDLEWAR_DURATION_SECS				=  1.f;
static const float ENEMY_COVER_DURATION_SECS				=  1.f;

static const int   ENEMY_COVER_PROB					        =  85.f;
static const int   ENEMY_PROB_REST					        =  40;
static const int   PROB_ACTION_ZONE          				=  85;
static const float MIN_WPT_DISTANCE							=  0.5f;

static const float ENEMY_FIGHT_DISTANCE						=  7.0f;
static const float TOO_CLOSE_DISTANCE						=  1.0f;
static const float MIDLE_AREA_COMBAT_DISTANCE				=  3.0f;



static const int ZONE_EXTERIOR                              = 0;
static const int ZONE_INTERIOR                              = 1;


//-------------------------------------
//-------------  BOSS   ---------------
//-------------------------------------

static const float BOSS_PROJECTILE_RADIOUS					=  4.0f;

static const float BOSS_CHASE_LINEAR_SPEED					=  3.f;
static const float BOSS_LINEAR_SPEED						=  1.5f;



static const float BOSS_CHASE_ANGULAR_SPEED_MULTIPLIER      =  4.5f;
static const float BOSS_FAST_ANGULAR_SPEED_MULTIPLIER       =  13.0f;

static const float BOSS_ATTACK_DURATION_SECS				=  3.0f;
static const float BOSS_ATTACK_HIT_DURATION_SECS			=  0.5f;
static const float BOSS_ATTACK_HIT_DELAY_SECS				=  0.5f;

static const float BOSS_ATTACK_DAMAGE_CA1					=  10;
static const float BOSS_ATTACK_DAMAGE_CA2					=  15;
static const float BOSS_ATTACK_DAMAGE_CA3					=  12;

static const float BOSS_ATTACK_CA1_DURATION_SECS			=  0.75f;
static const float BOSS_ATTACK_CA2_DURATION_SECS			=  1.2f;
static const float BOSS_ATTACK_CA3_DURATION_SECS			=  0.9f;

static const float BOSS_SPECIAL_ATTACK_DAMAGE				=10.f;

static const float BOSS_BLOCKING_DAMAGE_MULTIPLIER			=  0.05f;
static const int   BOSS_COVER                               =  95;
static const int   BOSS_NUMBER_PROJETILE_SPECIAL_ATTACK     =  9;
static const float BOSS_INVATION_AREA_RADIUS				=  12.0f;
static const float BOSS_COMBAT_AREA_RADIUS			        =  3.5f;
static const float BOSS_PROJECTILE_DAMAGE_RADIUS		    =  1.5f;

static const float BOSS_COVER_DURATION_SECS                 =  0.95f;
static const float BOSS_IDLEWAR_DURATION_SECS               =  1.0f;
static const float BOSS_UNCOVER_DURATION_SECS               =  1.2f;
static const float BOSS_TAUNT_DURATION_SECS                 =  3.0f;

#endif  