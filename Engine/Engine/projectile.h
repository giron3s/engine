#ifndef _INC_PROJECTILE_H_
#define _INC_PROJECTILE_H_

#include "McvPlatform.h"
#include "Entity3d.h"
#include "Camera.h"
#include "DrawUtils.h"

enum ProjectileType {
    NORMAL,
    SHIELD_BREAKER,
    PUSHER,
};

class CProjectile{

private:
    ProjectileType type;
    float	  damage;
    float	  pushDistance;

	XMVECTOR     pos;
    DWORD birthTicks;	//Birth time
    float ttl;			//Time to live
    float radious;		//Radious of the projectile
    bool  active;		//Active/deactive the projectile

    CRenderMesh *projectileMesh;
    CEntity3D *owner;

public:
    CProjectile();
    ~CProjectile();

    void create(CEntity3D* owner, float ttl, float radious, ProjectileType type, float damage, float pushDistance);
    void update();
    void render();
    void destroy();

    CEntity3D* getOwner( ) const { return owner; }
    bool isInside( XMVECTOR obj_position );
    bool isActive( ) { return active; }
    ProjectileType getType(){ return type; }
    float getPushDistance(){ return pushDistance; }
    float getDamage(){return damage;}
    void setActive(bool active);

	void setPos(XMVECTOR point) { pos = point;}
};

#endif