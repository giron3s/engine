#include "McvPlatform.h"
#include "AiConstants.h"
#include "BtEnemyBoss.h"
#include "Projectile.h"
#include "ProjectileManager.h"
#include "PlayerController.h"
#include "AiManager.h"
#include "AiConstants.h"
#include "utils.h"
#include <stdlib.h>
#include <time.h>

void CBtEnemyBoss::create(string enemyName, CEntity3D* owner, CEntity3D* playerEntity){
    
    //Set the variables
    name = enemyName;
    this->owner = owner;
    this->playerEntity = playerEntity;

    this->prev_ticks = 0;
    this->targeted = false;
    this->targetable = false;
    this->projectileCreated = false;
    this->active = true;
    
    //set the life
    setLife( 200.f );

    // Charge Behaviour Tree
    createRoot("enemyBoss", PRIORITY, NULL, NULL);
    addChild("enemyBoss", "damage", PRIORITY, (btcondition)&CBtEnemyBoss::conditionDamage, NULL);
    addChild("enemyBoss", "idle", RANDOM, (btcondition)&CBtEnemyBoss::conditionIdle, NULL);
    addChild("enemyBoss", "chase", PRIORITY, NULL, NULL);
   
    addChild("damage", "dead", ACTION, (btcondition)&CBtEnemyBoss::conditionDead, (btaction)&CBtEnemyBoss::actionDead);
    addChild("damage", "impactReact", SEQUENCE, NULL, NULL);
    
    addChild("impactReact", "impact", ACTION, NULL, (btaction)&CBtEnemyBoss::actionImpact);
    addChild("impactReact", "escape", ACTION, NULL, (btaction)&CBtEnemyBoss::actionEscape);

    addChild("idle", "taunt", ACTION, NULL, (btaction)&CBtEnemyBoss::actionTaunt);
    addChild("idle", "specialAttack", ACTION, NULL, (btaction)&CBtEnemyBoss::actionSpecialAttack);

    addChild("chase", "followPlayer", ACTION, (btcondition)&CBtEnemyBoss::conditionFollowPlayer, (btaction)&CBtEnemyBoss::actionFollowPlayer);
    addChild("chase", "combat", SEQUENCE, NULL, NULL);

    addChild("combat", "attack", RANDOM, NULL, NULL);
    addChild("combat", "cover", ACTION, (btcondition)&CBtEnemyBoss::conditionCover, (btaction)&CBtEnemyBoss::actionCover);
    addChild("combat", "idleWar", ACTION, NULL, (btaction)&CBtEnemyBoss::actionIdleWar);
    
    addChild("attack", "comboA1", ACTION, NULL, (btaction)&CBtEnemyBoss::actionComboA1);
    
    addChild("attack", "comboA2", SEQUENCE, NULL, NULL);
    addChild("comboA2", "comboA2Uno", ACTION, NULL, (btaction)&CBtEnemyBoss::actionComboA2Uno);
    addChild("comboA2", "comboA2Dos", ACTION, NULL, (btaction)&CBtEnemyBoss::actionComboA2Dos);
    addChild("comboA2", "uncover", ACTION, NULL, (btaction)&CBtEnemyBoss::actionUncover);
    
    addChild("attack", "comboA3", ACTION, NULL, (btaction)&CBtEnemyBoss::actionComboA3);

    srand (time(NULL));
}

// ACTIONS AND CONDITIONS

// DAMAGE ------------------------------------------------

bool CBtEnemyBoss::conditionDamage() {
    
    // �? Enemy is inside raduis circle of projectile player
    return false;
}

bool CBtEnemyBoss::conditionDead() {
    // Condition life == 0
    // Animation dead
    return false;
}

int CBtEnemyBoss::actionDead(float delta) {
    
    //dead pose
    owner->setPitch(deg2rad(90));

    //if this enemy was targeted we call targetEnemyEvaluation to manage the next targetable enemy
    if(targeted){
        if(!CAiManager::getInstance()->getPlayerController()->targetEnemyEvaluation() ){
            CAiManager::getInstance()->getPlayerController()->changeState("idle");
        }
    }

    owner->removeMesh("enemyPerceiveArea");

    //set active to false so no more recalcs are done to this IA
    active=false;

    // Animation Dead
    return false;
}

int CBtEnemyBoss::actionImpact(float delta) {
    
    return true;
}

int CBtEnemyBoss::actionEscape(float delta) {
    
    return true;
}

// ------------------------------------------------------

// IDLE ------------------------------------------------

bool CBtEnemyBoss::conditionIdle(){
    printf("%s: Condition Keep Idle  \n",name.c_str());
    if( XMVectorGetX( XMVector3LengthEst(playerEntity->getPosition() - owner->getPosition())) > BOSS_INVATION_AREA_RADIUS) {
        return true;
    }
    return false;
}

int CBtEnemyBoss::actionTaunt(float delta) {
   
  if (prev_ticks == 0) {
    prev_ticks = GetTickCount();
  }

  // Check player access to invation area while the animation is on
  if (XMVectorGetX( XMVector3LengthEst(playerEntity->getPosition() - owner->getPosition())) < BOSS_INVATION_AREA_RADIUS){
    return true;
  }

  //Actual time - Change state time > Rest animation
  float elapsedTime = (GetTickCount() - prev_ticks) / 1000.f;
  if (elapsedTime >  BOSS_TAUNT_DURATION_SECS) {
    //TODO: ANIMATION TAUNT / PROVOKE
    prev_ticks=0;
    return true;
  }
  else {
    return false;
  }
}

int CBtEnemyBoss::actionSpecialAttack (float delta) {
   
  std::vector<XMVECTOR> distanceAttack;
  // This function return a xmvector with the points that contain damage area around the player include his position
  distanceAttack = generateRandomPoint();

  if(prev_ticks == 0){
    prev_ticks = GetTickCount();
  }
    
  //damage starts
  float elapsed = (GetTickCount() - prev_ticks) / 1000.f;

  if (!projectileCreated && elapsed > BOSS_ATTACK_HIT_DELAY_SECS) {
    for (int i = 0; i < BOSS_NUMBER_PROJETILE_SPECIAL_ATTACK; i++) {
      projectileCreated = true;
      //Create the projectile of the attack and add it to the projectile manager
      CProjectile *projectile = new CProjectile;
      projectile->create(owner, BOSS_ATTACK_HIT_DURATION_SECS, BOSS_PROJECTILE_RADIOUS, ProjectileType::NORMAL, BOSS_SPECIAL_ATTACK_DAMAGE,0.f);
      projectile->setPos(distanceAttack[i]);
      CProjectileManager::getInstance()->addProjectile(projectile);
    }
  }
     
  //attack ends
  if(elapsed > BOSS_ATTACK_CA1_DURATION_SECS){
    prev_ticks = 0;
    projectileCreated = false;
    return true;
  }

  return false;
}

// ------------------------------------------------------


// CHASE ------------------------------------------------

bool CBtEnemyBoss::conditionFollowPlayer(){
    printf("%s: Condition Keep Idle  \n",name.c_str());
    if( XMVectorGetX( XMVector3LengthEst(playerEntity->getPosition() - owner->getPosition())) > BOSS_COMBAT_AREA_RADIUS) {
        return true;
    }
    return false;
}

int CBtEnemyBoss::actionFollowPlayer(float delta) {

  float alfa = owner->getAngleTo(playerEntity->getPosition());
  float angleDelta = alfa * delta * BOSS_CHASE_ANGULAR_SPEED_MULTIPLIER;
  
  if(alfa < deg2rad(0.1) ){
      owner->setYaw(owner->getYaw() + alfa);
  }else{
      owner->setYaw(owner->getYaw() + angleDelta);
  }
  
  //Move to the playerEntity direction
  owner->setPosition( owner->pos + (owner->getFront() * delta * BOSS_CHASE_LINEAR_SPEED)  );
  return true;
}

// ------------------------------------------------------


// COMBAT ------------------------------------------------

bool CBtEnemyBoss::conditionCover(){
  if (detectPlayerAttack()) {
    return true;
  }
  return false;
}

int CBtEnemyBoss::actionCover(float delta) {
    
  // Animation Cover
  if(prev_ticks == 0){
    prev_ticks = GetTickCount();
  }

  //face the player
  rotateToPoint(delta,playerEntity->getPosition(),BOSS_FAST_ANGULAR_SPEED_MULTIPLIER);

  //Actual time - Change state time > Rest animation
  float elapsedTime = (GetTickCount() - prev_ticks) / 1000.f;
  if (elapsedTime >  BOSS_COVER_DURATION_SECS){
    prev_ticks=0;
    return true;
  }
  return false;
}

int CBtEnemyBoss::actionIdleWar(float delta) {

    // Anmation Idle War
  if(prev_ticks == 0){
    prev_ticks = GetTickCount();
  }
  
  //face the player
  float alfa = owner->getAngleTo(playerEntity->getPosition());
  float angleDelta = alfa * delta * BOSS_CHASE_ANGULAR_SPEED_MULTIPLIER;
    
  if (alfa < deg2rad(0.1) ){
    owner->setYaw(owner->getYaw() + alfa);
  } else {
    owner->setYaw(owner->getYaw() + angleDelta);
  }

  if(detectPlayerAttack()) { return true; }

  //Actual time - Change state time > Rest animation
  float elapsedTime = (GetTickCount() - prev_ticks) / 1000.f;
  if (elapsedTime >  BOSS_IDLEWAR_DURATION_SECS) {
    //TODO: ANIMATION IDLE WAR
    prev_ticks=0;
    return true;
  }
  else {
    return false;
  }
}

// ------------------------------------------------------


// ATTACK ------------------------------------------------

int CBtEnemyBoss::actionComboA1(float delta) {

  if (prev_ticks == 0){
    prev_ticks = GetTickCount();
  }
    
    owner->setPosition( owner->pos + owner->getFront() * 0.002 * BOSS_LINEAR_SPEED  );

    float elapsed = ( GetTickCount() - prev_ticks ) / 1000.0f;

    //elapsed percent of the attack (in range [0,1])
    //float elapsedPercent = elapsed / BOSS_ATTACK_DURATION_SECS;

   //damage starts
  if (!projectileCreated && elapsed > BOSS_ATTACK_HIT_DELAY_SECS){
    projectileCreated = true;
    //Create the projectile of the attack and add it to the projectile manager
    CProjectile *projectile = new CProjectile;
    projectile->create(owner, BOSS_ATTACK_HIT_DURATION_SECS, BOSS_PROJECTILE_RADIOUS, ProjectileType::NORMAL, BOSS_ATTACK_DAMAGE_CA1,0.f);
    CProjectileManager::getInstance()->addProjectile(projectile);
  }

   //if(detectPlayerAttack()) { return true; }

   //attack ends
    if(elapsed > BOSS_ATTACK_CA1_DURATION_SECS){
        prev_ticks = 0;
        projectileCreated = false;
        return true;
    }

    return false;
}

int CBtEnemyBoss::actionComboA3(float delta) {

  if (prev_ticks == 0){
    prev_ticks = GetTickCount();
  }
   
   float elapsed = ( GetTickCount() - prev_ticks ) / 1000.0f;

    //elapsed percent of the attack (in range [0,1])
    float elapsedPercent = elapsed / BOSS_ATTACK_DURATION_SECS;

  //damage starts
  if (!projectileCreated && elapsed > BOSS_ATTACK_HIT_DELAY_SECS){
    projectileCreated = true;
    //Create the projectile of the attack and add it to the projectile manager
    CProjectile *projectile = new CProjectile;
    projectile->create(owner, BOSS_ATTACK_HIT_DURATION_SECS, BOSS_PROJECTILE_RADIOUS, ProjectileType::PUSHER, BOSS_ATTACK_DAMAGE_CA3,0.f);
    CProjectileManager::getInstance()->addProjectile(projectile);
  }

  if(detectPlayerAttack()) { return true; }

   //attack ends
    if(elapsed > BOSS_ATTACK_CA1_DURATION_SECS){
        prev_ticks = 0;
        projectileCreated = false;
        return true;
    }

    return false;
}

int CBtEnemyBoss::actionComboA2Uno(float delta) {

  int prob = getRandomInt(1,100);
 // if (prob<60) { return true; }

  if (prev_ticks == 0){
    prev_ticks = GetTickCount();
  }
   
  float elapsed = ( GetTickCount() - prev_ticks ) / 1000.0f;

  //elapsed percent of the attack (in range [0,1])
  float elapsedPercent = elapsed / BOSS_ATTACK_DURATION_SECS;

  //owner->setPosition( owner->pos + owner->getFront() * 0.002 * BOSS_LINEAR_SPEED  );

  //damage starts
  if (!projectileCreated && elapsed > BOSS_ATTACK_HIT_DELAY_SECS){
    projectileCreated = true;
    //Create the projectile of the attack and add it to the projectile manager
    CProjectile *projectile = new CProjectile;
    projectile->create(owner, BOSS_ATTACK_HIT_DURATION_SECS, BOSS_PROJECTILE_RADIOUS, ProjectileType::SHIELD_BREAKER, BOSS_ATTACK_DAMAGE_CA2,0.f);
    CProjectileManager::getInstance()->addProjectile(projectile);
  }

  //attack ends
  if(elapsed > BOSS_ATTACK_CA2_DURATION_SECS){
    prev_ticks = 0;
    projectileCreated = false;
    return true;
  }
  return false;
}

int CBtEnemyBoss::actionComboA2Dos(float delta) {

  if (prev_ticks == 0){
    prev_ticks = GetTickCount();
  }
   
   float elapsed = ( GetTickCount() - prev_ticks ) / 1000.0f;

    //elapsed percent of the attack (in range [0,1])
    float elapsedPercent = elapsed / BOSS_ATTACK_DURATION_SECS;

  //owner->setPosition( owner->pos + owner->getFront() * 0.002 * BOSS_LINEAR_SPEED  );

  //damage starts
  if (!projectileCreated && elapsed > BOSS_ATTACK_HIT_DELAY_SECS){
    projectileCreated = true;
    //Create the projectile of the attack and add it to the projectile manager
    CProjectile *projectile = new CProjectile;
    projectile->create(owner, BOSS_ATTACK_HIT_DURATION_SECS, BOSS_PROJECTILE_RADIOUS, ProjectileType::NORMAL, BOSS_ATTACK_DAMAGE_CA1,0.f);
    CProjectileManager::getInstance()->addProjectile(projectile);
  }

  //attack ends
  if(elapsed > BOSS_ATTACK_CA1_DURATION_SECS){
    prev_ticks = 0;
    projectileCreated = false;
    return true;
  }
  return false;
}

int CBtEnemyBoss::actionUncover(float delta) {

  if (prev_ticks == 0){
    prev_ticks = GetTickCount();
  }

  //Actual time - Change state time > Rest animation
  float elapsedTime = (GetTickCount() - prev_ticks) / 1000.f;
  if (elapsedTime >  BOSS_UNCOVER_DURATION_SECS) {
    //TODO: ANIMATION IDLE WAR
    prev_ticks=0;
    return true;
  }
  else {
    return false;
  }
}

// ------------------------------------------------------


bool CBtEnemyBoss::probabilityCover() {
    
    return (rand() % 100) < BOSS_COVER;
}

bool CBtEnemyBoss::detectPlayerAttack() {

    if (CAiManager::getInstance()->getPlayerController()->isAttacking()) { return true; }
    return false;
}

bool CBtEnemyBoss::booleanProbability() { return rand() % 2 == 0; }


std::vector<XMVECTOR> CBtEnemyBoss::generateRandomPoint() {

    std::vector<XMVECTOR> points;

    // Cardinals
    float north = (rand()%3)+2;
    float south = (rand()%3)+2;
    float west = (rand()%3)+2;
    float east = (rand()%3)+2;

    XMVECTOR playerPos = playerEntity->getPosition();
    points.push_back(playerEntity->getPosition());

    points.push_back(XMVectorSet(0,0,north*-1,0)+playerPos);                // N
    points.push_back(XMVectorSet(east-1,0,(north*-1)+1,0)+playerPos);       // NE
    points.push_back(XMVectorSet(east,0,0,0)+playerPos);                    // E
    points.push_back(XMVectorSet(east-1,0,south-1,0)+playerPos);            // SE

    points.push_back(XMVectorSet(0,0,south,0)+playerPos);                   // S
    points.push_back(XMVectorSet((west*-1)+1,0,south-1,0)+playerPos);       // SW
    points.push_back(XMVectorSet(west*-1,0,0,0)+playerPos);                 // W
    points.push_back(XMVectorSet((west*-1)+1,0,(north*-1)+1,0)+playerPos);  // NW

    return points;

}


void CBtEnemyBoss::manageDamage(CProjectile *p){
    
    if(getCurrent() !=NULL && getCurrent()->getName().compare("dead") == 0) return;

    switch(p->getType()){

        case NORMAL:
            if(lastNode.compare("cover") == 0){
                //substract a percentage of the total damage
                applyDamage( p->getDamage() * BOSS_BLOCKING_DAMAGE_MULTIPLIER );
            }else{
                //substract total damage
                applyDamage( p->getDamage() );
            }
            break;

        case PUSHER:
            if(lastNode.compare("cover") == 0){
                //substract a percentage of the total damage
                applyDamage( p->getDamage() * BOSS_BLOCKING_DAMAGE_MULTIPLIER );

                pushDirection = XMVector3Normalize( owner->pos - p->getOwner()->getPosition() );
                setCurrent(findNode("processPush"));
            }else{
                //substract total damage
                applyDamage( p->getDamage() );

                pushDirection = XMVector3Normalize( owner->pos - p->getOwner()->getPosition() );
                setCurrent(findNode("processPush"));
            }
            prev_ticks = 0;
            
            break;

        case SHIELD_BREAKER:
            if(lastNode.compare("cover") == 0){                
                //no damage with this type of projectile if we are in cover state
                setCurrent(findNode("processShieldBreak"));

            }else{
                //substract total damage
                applyDamage( p->getDamage() );

                pushDirection = XMVector3Normalize( owner->pos - p->getOwner()->getPosition() );                
                setCurrent(findNode("processPush"));
            }
            prev_ticks = 0;
            break;
    }

    if(isDead()){
        setCurrent(findNode("dead"));
    }

}


bool CBtEnemyBoss::rotateToPoint(float delta,XMVECTOR point, int rotationMultiplier){

  //face the player
  float alfa = owner->getAngleTo(point);
    float angleDelta = alfa * delta * rotationMultiplier;
    
    if(alfa < deg2rad(0.1f) ){
        owner->setYaw(owner->getYaw() + alfa);
        return true;
    }else{
        owner->setYaw(owner->getYaw() + angleDelta);
    }
    return false;
}


void CBtEnemyBoss::updatePosition(XMVECTOR movementDisplacement, float delta){

}