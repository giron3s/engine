#include "McvPlatform.h"
#include "Entity3D.h"
#include "IoStatus.h"
#include "PlayerController.h"
#include "AiConstants.h"
#include "AiManager.h"
#include "entity3d.h"
#include "Projectile.h"
#include "ProjectileManager.h"
#include "AiZone.h"
#include "angular.h"
#include "CameraController.h"
#include "Bt.h"
#include <algorithm>   //std::find

void CPlayerController::init(CEntity3D *_player, CCamera *_cam){
  //set player and enemy
  player = _player;
  targetedEnemy = NULL;

  //to be removed(simulate ipact)
  impactTime = 0;

  //set the life
  setLife(100.f);

  //set camera
  cam = _cam;

  prev_ticks = 0;
  projectileCreated = false;

  // insert all states in the map
  addState("dead",(statehandler)&CPlayerController::deadState);

  //idle states
  addState("idle",(statehandler)&CPlayerController::idleState);
  addState("walk",(statehandler)&CPlayerController::walkState);
  addState("run",(statehandler)&CPlayerController::runState);

  addState("basicIdleAttack",(statehandler)&CPlayerController::basicIdleAttackState);
  addState("blockIdle",(statehandler)&CPlayerController::blockIdleState);
  addState("kickIdle",(statehandler)&CPlayerController::kickIdleState);
  addState("dodgeIdle",(statehandler)&CPlayerController::dodgeIdleState);


  //war states
  addState("idle_war",(statehandler)&CPlayerController::idleWarState);
  addState("orbit",(statehandler)&CPlayerController::orbitState);
  addState("dodge",(statehandler)&CPlayerController::dodgeState);
  addState("block",(statehandler)&CPlayerController::blockState);
  addState("kick",(statehandler)&CPlayerController::kickState);

  //attack states
  addState("basicAttack",(statehandler)&CPlayerController::basicAttackState);
  addState("attackLeft1",(statehandler)&CPlayerController::attackLeft1State);
  addState("attackLeft2",(statehandler)&CPlayerController::attackLeft2State);
  addState("attackRight1",(statehandler)&CPlayerController::attackRight1State);
  addState("attackRight2",(statehandler)&CPlayerController::attackRight2State);
  addState("attackUp1",(statehandler)&CPlayerController::attackUp1State);
  addState("attackUp2",(statehandler)&CPlayerController::attackUp2State);
  addState("attackDown1",(statehandler)&CPlayerController::attackDown1State);
  addState("attackDown2",(statehandler)&CPlayerController::attackDown2State);


  //impactprocess states
  addState("processPush",(statehandler)&CPlayerController::processPushState);
  addState("processShieldBreak",(statehandler)&CPlayerController::processShieldBreakState);

  // reset the state
  changeState("idle");
}


void CPlayerController::deadState(float){

    //dead pose
    player->setPitch(deg2rad(90));

    //no return
}

void CPlayerController::idleState(float delta){

    //run idle animation

    //state transitions
    if(io.isPressed(io.DIGITAL_UP) || io.isPressed(io.DIGITAL_DOWN) || io.isPressed(io.DIGITAL_LEFT) || io.isPressed(io.DIGITAL_RIGHT)){
        changeState("walk");
    }

    //press TAB -> target enemy
    if(io.becomesPressed(io.LB)){
        if(targetEnemyEvaluation()){
            changeState("idle_war");
        }
    }
    
    if(io.isPressed(io.CROSS)){
        changeState("blockIdle");
}

    if(io.becomesPressed(io.TRIANGLE)){
        changeState("kickIdle");
    }

    //TODO: add transitions to all attacks left,right,up,down,basic
    if(io.becomesPressed(io.MOUSE_LEFT)){
        changeState("basicIdleAttack");
    }
    
}


void CPlayerController::walkState(float delta){

    //run walk animation
    XMVECTOR walkDirection;
    //take walk direction
    if( io.isPressed(io.DIGITAL_UP) && io.isPressed(io.DIGITAL_RIGHT) ){
        walkDirection = XMVector3Normalize( cam->getFront() -cam->getLeft() );
    }else if(  io.isPressed(io.DIGITAL_RIGHT) && io.isPressed(io.DIGITAL_DOWN) ){
       walkDirection = XMVector3Normalize( -cam->getFront() -cam->getLeft() );
    }else if(  io.isPressed(io.DIGITAL_LEFT) && io.isPressed(io.DIGITAL_DOWN) ){
       walkDirection = XMVector3Normalize( -cam->getFront() + cam->getLeft() );
    }else if(  io.isPressed(io.DIGITAL_LEFT) && io.isPressed(io.DIGITAL_UP) ){
       walkDirection = XMVector3Normalize( cam->getFront() + cam->getLeft() );
    }else if( io.isPressed(io.DIGITAL_UP) ){
        walkDirection = cam->getFront();      
    }else if( io.isPressed(io.DIGITAL_RIGHT) ) {
       walkDirection = -cam->getLeft();
    }else if( io.isPressed(io.DIGITAL_DOWN) ){
       walkDirection = -cam->getFront();
    }else if( io.isPressed(io.DIGITAL_LEFT) ) {
       walkDirection = cam->getLeft();

    }

    //aim Player to direction
    float angleDiff = player->getAngleTo( player->getPosition() + walkDirection );  
    float rotate_delta = angleDiff * delta * PLAYER_TARGET_SPEED;
    player->setYaw( player->getYaw() + rotate_delta );

        updatePosition( player->getFront() * PLAYER_WALK_SPEED , delta );


    //state transitions
    if(!(io.isPressed(io.DIGITAL_UP) || io.isPressed(io.DIGITAL_DOWN) || io.isPressed(io.DIGITAL_LEFT) || io.isPressed(io.DIGITAL_RIGHT))){
        changeState("idle");
    }

    if(io.isPressed(io.CROSS)){
        changeState("blockIdle");
    }

    if(io.isPressed(io.CIRCLE)){
        changeState("run");
    }

    if(io.isPressed(io.MOUSE_RIGHT)){
        changeState("dodgeIdle");
    }

    if(io.becomesPressed(io.TRIANGLE)){
        changeState("kickIdle");
    }

    if(io.becomesPressed(io.MOUSE_LEFT)){
        changeState("basicIdleAttack");
    }

    //press TAB -> target enemy
    if(io.becomesPressed(io.LB)){
        if(targetEnemyEvaluation()){
            changeState("idle_war");
        }
    }

}

void CPlayerController::runState(float delta){

    //run animation
    XMVECTOR walkDirection;
    //take walk direction
    if( io.isPressed(io.DIGITAL_UP) && io.isPressed(io.DIGITAL_RIGHT) ){
        walkDirection = XMVector3Normalize( cam->getFront() -cam->getLeft() );
    }else if(  io.isPressed(io.DIGITAL_RIGHT) && io.isPressed(io.DIGITAL_DOWN) ){
       walkDirection = XMVector3Normalize( -cam->getFront() -cam->getLeft() );
    }else if(  io.isPressed(io.DIGITAL_LEFT) && io.isPressed(io.DIGITAL_DOWN) ){
       walkDirection = XMVector3Normalize( -cam->getFront() + cam->getLeft() );
    }else if(  io.isPressed(io.DIGITAL_LEFT) && io.isPressed(io.DIGITAL_UP) ){
       walkDirection = XMVector3Normalize( cam->getFront() + cam->getLeft() );
    }else if( io.isPressed(io.DIGITAL_UP) ){
       walkDirection = cam->getFront();      
    }else if( io.isPressed(io.DIGITAL_RIGHT) ) {
       walkDirection = -cam->getLeft();
    }else if( io.isPressed(io.DIGITAL_DOWN) ){
       walkDirection = -cam->getFront();
    }else if( io.isPressed(io.DIGITAL_LEFT) ) {
       walkDirection = cam->getLeft();
    }

    //aim Player to direction
    float angleDiff = player->getAngleTo( player->getPosition() + walkDirection );  
    float rotate_delta = angleDiff * delta * PLAYER_TARGET_SPEED;
    player->setYaw( player->getYaw() + rotate_delta );

    //if(fabs( player->getAngleTo( player->getPosition() + walkDirection )) < XM_PIDIV4 ){
        //player->setPosition( player->pos + player->getFront() * delta * PLAYER_RUN_SPEED );
         updatePosition( player->getFront() * PLAYER_RUN_SPEED , delta ); 
    //}

    //state transitions
    if(io.isPressed(io.MOUSE_RIGHT)){
        changeState("dodgeIdle");
    }

    if(!(io.isPressed(io.DIGITAL_UP) || io.isPressed(io.DIGITAL_DOWN) || io.isPressed(io.DIGITAL_LEFT) || io.isPressed(io.DIGITAL_RIGHT))){
        changeState("idle");
    }else if(io.becomesReleased(io.CIRCLE)){
        changeState("walk");
    }

}

void CPlayerController::dodgeIdleState(float delta){

    //calculate init position(before dodge) and final position (after dodge)
    if(prev_ticks == 0){
        prev_ticks = GetTickCount();

        initLerpPos = player->pos;
        finalLerpPos =  player->pos + player->getFront() * PLAYER_DODGE_DISTANCE;    
    }
    
    //update time counter
    DWORD new_ticks = GetTickCount();
    float elapsed = ( new_ticks - prev_ticks ) / 1000.0f;

    //interpolate init and final positions 
    float timePercent = elapsed / PLAYER_DODGE_DURATION_SECS;
    player->setPosition( (1-timePercent)*initLerpPos + timePercent*finalLerpPos );    
    //updatePosition( (1-timePercent)*initLerpPos + timePercent*finalLerpPos , 1.);

    //dodge ends -> aim and changestate.
    if(elapsed > PLAYER_DODGE_DURATION_SECS){

        prev_ticks = 0;       
        changeState("idle");
    }

}

void CPlayerController::blockIdleState(float delta){

    //state transitions--------
    if(!io.isPressed(io.CROSS)){
        changeState("idle");
    }

    //press TAB -> target enemy
    if(io.becomesPressed(io.LB)){
        if(targetEnemyEvaluation()){
            changeState("idle_war");
        }
    }

    //run block animation
    XMVECTOR walkDirection;
    //take walk direction
    if( io.isPressed(io.DIGITAL_UP) && io.isPressed(io.DIGITAL_RIGHT) ){
        walkDirection = XMVector3Normalize( cam->getFront() -cam->getLeft() );
    }else if(  io.isPressed(io.DIGITAL_RIGHT) && io.isPressed(io.DIGITAL_DOWN) ){
       walkDirection = XMVector3Normalize( -cam->getFront() -cam->getLeft() );
    }else if(  io.isPressed(io.DIGITAL_LEFT) && io.isPressed(io.DIGITAL_DOWN) ){
       walkDirection = XMVector3Normalize( -cam->getFront() + cam->getLeft() );
    }else if(  io.isPressed(io.DIGITAL_LEFT) && io.isPressed(io.DIGITAL_UP) ){
       walkDirection = XMVector3Normalize( cam->getFront() + cam->getLeft() );
    }else if( io.isPressed(io.DIGITAL_UP) ){
       walkDirection = cam->getFront();      
    }else if( io.isPressed(io.DIGITAL_RIGHT) ) {
       walkDirection = -cam->getLeft();
    }else if( io.isPressed(io.DIGITAL_DOWN) ){
       walkDirection = -cam->getFront();
    }else if( io.isPressed(io.DIGITAL_LEFT) ) {
       walkDirection = cam->getLeft();
    }else{
        return;
    }

    //aim Player to direction
    float angleDiff = player->getAngleTo( player->getPosition() + walkDirection );  
    float rotate_delta = angleDiff * delta * PLAYER_TARGET_SPEED;
    player->setYaw( player->getYaw() + rotate_delta );
    
    updatePosition( player->getFront() * PLAYER_BLOCKING_SPEED , delta );


    }

void CPlayerController::kickIdleState(float delta){
    
    //Create the projectile
    if(prev_ticks == 0){
        prev_ticks = GetTickCount();
    }

    DWORD new_ticks = GetTickCount();
    float elapsed = ( new_ticks - prev_ticks ) / 1000.0f;

    if(!projectileCreated && elapsed > PLAYER_KICK_HIT_DELAY_SECS){
        projectileCreated = true;
        //Create the projectile of the attack and add it to the projectile manager
        CProjectile *projectile = new CProjectile;
        projectile->create(player, PLAYER_KICK_HIT_DURATION_SECS, PLAYER_PROJECTILE_RADIOUS, SHIELD_BREAKER, PLAYER_KICK_DAMAGE,0.f);
        CProjectileManager::getInstance()->addProjectile(projectile);
    }

    if(elapsed > PLAYER_KICK_DURATION_SECS){
        prev_ticks = 0;
        projectileCreated = false;
        changeState("idle");
    }
}

void CPlayerController::basicIdleAttackState(float delta){
    if(prev_ticks == 0){
        prev_ticks = GetTickCount();
    }

    DWORD new_ticks = GetTickCount();
    float elapsed = ( new_ticks - prev_ticks ) / 1000.0f;

    if(!projectileCreated && elapsed > PLAYER_ATTACK_BASIC_HIT_DELAY_SECS){
        projectileCreated = true;
        //Create the projectile of the attack and add it to the projectile manager
        CProjectile *projectile = new CProjectile;
        projectile->create(player, PLAYER_ATTACK_BASIC_HIT_DURATION_SECS, PLAYER_PROJECTILE_RADIOUS, NORMAL, PLAYER_ATTACK_BASIC_DAMAGE,0.f);
        CProjectileManager::getInstance()->addProjectile(projectile);
}

    if(elapsed > PLAYER_ATTACK_BASIC_DURATION_SECS){
        prev_ticks = 0;
        projectileCreated = false;
        changeState("idle");
    }
}

//********************************
//*********  WAR STATES  *********
//********************************
void CPlayerController::idleWarState(float delta){
    
    //aim Player to targeted enemy
    float angleDiff = player->getAngleTo( targetedEnemy->getPosition() );  
    //float rotate_delta = angleDiff * delta * PLAYER_TARGET_SPEED;
    player->setYaw( player->getYaw() + angleDiff );

    //run idle_war animation
    
    if(io.becomesPressed(io.LB)){
        targetEnemyEvaluation();
    }

    //state transitions------------------------------------------
    if(io.isPressed(io.DIGITAL_UP) || io.isPressed(io.DIGITAL_DOWN) || io.isPressed(io.DIGITAL_LEFT) || io.isPressed(io.DIGITAL_RIGHT)){
        changeState("orbit");
    }

    if(io.isPressed(io.CROSS)){
        changeState("block");
    }

    if(io.becomesPressed(io.TRIANGLE)){
        changeState("kick");
    }



    //TODO: add transitions to all attacks left,right,up,down,basic
    if(io.becomesPressed(io.MOUSE_LEFT)){
        changeState("basicAttack");
    }

    if(io.isPressed(io.LT)){
        if(targetedEnemy != NULL){
            targetedEnemy->setTargeted(false);
            targetedEnemy->getOwner()->removeMesh("target");
            targetedEnemy = NULL;

            changeState("idle");
        }
    }

}


void CPlayerController::orbitState(float delta){
    //run orbit animation
    
    // orbit left/right
    if( io.isPressed(io.DIGITAL_LEFT) ) {
        //player->setPosition( player->pos + player->getLeft() * delta * PLAYER_ORBIT_SPEED );
        updatePosition( player->getLeft() * PLAYER_ORBIT_SPEED , delta);
    }else if( io.isPressed(io.DIGITAL_RIGHT) ) {
        //player->setPosition( player->pos - player->getLeft() * delta * PLAYER_ORBIT_SPEED );
        updatePosition( -1 * player->getLeft() * PLAYER_ORBIT_SPEED , delta );
    }

    //re-aim Player to targeted enemy
    float angleDiff = player->getAngleTo( targetedEnemy->getPosition() );  
    float rotate_delta = angleDiff * delta * PLAYER_TARGET_SPEED;
    player->setYaw( player->getYaw() + rotate_delta );


    //forward / backward
    if( io.isPressed(io.DIGITAL_UP) ){
        //player->setPosition( player->pos + player->getFront() * delta * PLAYER_WALK_SPEED );
        updatePosition( player->getFront()  * PLAYER_WALK_SPEED,  delta );
    }else if( io.isPressed(io.DIGITAL_DOWN) ){
        //player->setPosition( player->pos - player->getFront() * delta * PLAYER_WALK_SPEED );
        updatePosition( -1* player->getFront() * PLAYER_WALK_SPEED, delta );
    }


    //state transitions------------------------------------------------
    if(io.becomesPressed(io.LB)){
        targetEnemyEvaluation();
    }

    if(io.isPressed(io.CROSS)){
        changeState("block");
    }

    if(io.becomesPressed(io.TRIANGLE)){
        changeState("kick");
    }

    //attack transitions FIX(hacer un estado intermedio entre idle_war y los ataques donde esperamos a ver si se pulsan mas teclas y asi enviar al estado correcto)
    if(io.becomesPressed(io.MOUSE_LEFT)){
        if(io.isPressed(io.DIGITAL_LEFT)){
            changeState("attackLeft1");
        }else if(io.isPressed(io.DIGITAL_RIGHT)){
            changeState("attackRight1");
        }else if(io.isPressed(io.DIGITAL_UP)){
            changeState("attackUp1");
        }else if(io.isPressed(io.DIGITAL_DOWN)){
            changeState("attackDown1");
        }
    }

    if(io.becomesPressed(io.MOUSE_RIGHT)){
        changeState("dodge");
    }

    if(!(io.isPressed(io.DIGITAL_UP) || io.isPressed(io.DIGITAL_DOWN) || io.isPressed(io.DIGITAL_LEFT) || io.isPressed(io.DIGITAL_RIGHT))){
        changeState("idle_war");
    }    

}


void CPlayerController::dodgeState(float delta){

    //calculate init position(before dodge) and final position (after dodge)
    if(prev_ticks == 0){
        initLerpPos = player->pos;
        initLerpAngle = player->getYaw();

        //calculate player polar coordinates based on enemy position
        XMVECTOR targetToPlayer = player->pos - targetedEnemy->getPosition();
        float distanceToTarget = XMVectorGetX( XMVector3LengthEst(targetToPlayer));
        float yawToTarget = getYawFromVector(targetToPlayer);

        // orbit left/right
        if( io.isPressed(io.DIGITAL_LEFT) ) {
            //get angle from circumference arc formula
            float angleToMove = PLAYER_DODGE_DISTANCE / distanceToTarget;

            //update player to target yaw
            yawToTarget = yawToTarget - angleToMove ;
      
            //final pos and yaw for linear interpolation
            finalLerpPos = targetedEnemy->getPosition() + getVectorFromYaw(yawToTarget) * distanceToTarget;
            finalLerpAngle = initLerpAngle - angleToMove;


        }else if( io.isPressed(io.DIGITAL_RIGHT) ) {
            //get angle from circumference arc formula
            float angleToMove = PLAYER_DODGE_DISTANCE / distanceToTarget;

            //update yaw
            yawToTarget = yawToTarget + angleToMove;
      
            //final pos for linear interpolation
            finalLerpPos = targetedEnemy->getPosition() + getVectorFromYaw(yawToTarget) * distanceToTarget;
            finalLerpAngle = initLerpAngle + angleToMove;
        }

         //forward / backward
        if( io.isPressed(io.DIGITAL_UP) ){
            finalLerpPos =  player->pos + player->getFront() * PLAYER_DODGE_DISTANCE;
            finalLerpAngle = initLerpAngle;
        }else if( io.isPressed(io.DIGITAL_DOWN) ){
            finalLerpPos =  player->pos - player->getFront() * PLAYER_DODGE_DISTANCE;
            finalLerpAngle = initLerpAngle;
    }
    
        prev_ticks = GetTickCount();
    }
    
    //update time counter
    DWORD new_ticks = GetTickCount();
    float elapsed = ( new_ticks - prev_ticks ) / 1000.0f;

    //interpolate init and final positions 
    float timePercent = elapsed / PLAYER_DODGE_DURATION_SECS;
    player->setPosition( (1-timePercent)*initLerpPos + timePercent*finalLerpPos );
    player->setYaw((1-timePercent)*initLerpAngle + timePercent*finalLerpAngle);
    //updatePosition( (1-timePercent)*initLerpPos + timePercent*finalLerpPos , 1.);

    //dodge ends -> aim and changestate.
    if(elapsed > PLAYER_DODGE_DURATION_SECS){

        prev_ticks = 0;       
        changeState("idle_war");
    }

}

void CPlayerController::blockState(float delta){

    //block animation
    // orbit left/right
    if( io.isPressed(io.DIGITAL_LEFT) ) {
         player->setPosition( player->pos + player->getLeft() * delta * PLAYER_BLOCKING_SPEED);
    }else if( io.isPressed(io.DIGITAL_RIGHT) ) {
         player->setPosition( player->pos - player->getLeft() * delta * PLAYER_BLOCKING_SPEED );
    }

    //re-aim Player to targeted enemy
    float angleDiff = player->getAngleTo( targetedEnemy->getPosition() );  
    float rotate_delta = angleDiff * delta * PLAYER_TARGET_SPEED;
    player->setYaw( player->getYaw() + rotate_delta );
    
    //forward / backward
    if( io.isPressed(io.DIGITAL_UP) ){
        player->setPosition( player->pos + player->getFront() * delta * PLAYER_BLOCKING_SPEED );
    }else if( io.isPressed(io.DIGITAL_DOWN) ){
        player->setPosition( player->pos - player->getFront() * delta * PLAYER_BLOCKING_SPEED );
    }

    
    //state transitions-------------------------------------
    if(!io.isPressed(io.CROSS)){
        changeState("orbit");
    }

}

void CPlayerController::kickState(float delta){
    //Create the projectile
    if(prev_ticks == 0){
        prev_ticks = GetTickCount();
    }
    
    DWORD new_ticks = GetTickCount();
    float elapsed = ( new_ticks - prev_ticks ) / 1000.0f;

    if(!projectileCreated && elapsed > PLAYER_KICK_HIT_DELAY_SECS){
        projectileCreated = true;
        //Create the projectile of the attack and add it to the projectile manager
        CProjectile *projectile = new CProjectile;
        projectile->create(player, PLAYER_KICK_HIT_DURATION_SECS, PLAYER_PROJECTILE_RADIOUS, SHIELD_BREAKER, PLAYER_KICK_DAMAGE,0.f);
        CProjectileManager::getInstance()->addProjectile(projectile);
    }

    if(elapsed > PLAYER_KICK_DURATION_SECS){
        prev_ticks = 0;
        projectileCreated = false;
        changeState("idle_war");
    }
}


//**************************//
//****** ATTACK STATES *****//
//**************************//
void CPlayerController::attackLeft1State(float delta){

    //attack animation starts
    if(prev_ticks == 0){
        prev_ticks = GetTickCount();
    }
    
    DWORD new_ticks = GetTickCount();
    float elapsed = ( new_ticks - prev_ticks ) / 1000.0f;

    //elapsed percent of the attack (in range [0,1])
    float elapsedPercent = elapsed / PLAYER_ATTACK_LEFT1_DURATION_SECS;

    //damage starts
    if(!projectileCreated && elapsed > PLAYER_ATTACK_LEFT1_HIT_DELAY_SECS){
        projectileCreated = true;
        
        //Create the projectile of the attack and add it to the projectile manager
        CProjectile *projectile = new CProjectile;
        projectile->create(player, PLAYER_ATTACK_LEFT1_HIT_DURATION_SECS, PLAYER_PROJECTILE_RADIOUS, NORMAL, PLAYER_ATTACK_LEFT1_DAMAGE,0.f);
        CProjectileManager::getInstance()->addProjectile(projectile);
    }

    //combo window
    if(elapsedPercent > 0.5 && elapsedPercent < 1){
        if(io.becomesPressed(io.MOUSE_LEFT) && io.isPressed(io.DIGITAL_LEFT)){
            prev_ticks = 0;
            projectileCreated = false;
            changeState("attackLeft2");
        }
    }

    //attack ends
    if(elapsed > PLAYER_ATTACK_LEFT1_DURATION_SECS){
        prev_ticks = 0;
        projectileCreated = false;
        changeState("idle_war");
    }
}

void CPlayerController::attackLeft2State(float delta){

    if(prev_ticks == 0){
        prev_ticks = GetTickCount();
    }

    DWORD new_ticks = GetTickCount();
    float elapsed = ( new_ticks - prev_ticks ) / 1000.0f;

    if(!projectileCreated && elapsed > PLAYER_ATTACK_LEFT2_HIT_DELAY_SECS){
        projectileCreated = true;
        //Create the projectile of the attack and add it to the projectile manager
        CProjectile *projectile = new CProjectile;
        projectile->create(player, PLAYER_ATTACK_LEFT2_HIT_DURATION_SECS, PLAYER_PROJECTILE_RADIOUS, NORMAL, PLAYER_ATTACK_LEFT2_DAMAGE,0.f);
        CProjectileManager::getInstance()->addProjectile(projectile);
    }

    if(elapsed > PLAYER_ATTACK_LEFT2_DURATION_SECS){
        prev_ticks = 0;
        projectileCreated = false;
        changeState("idle_war");
    }
}


void CPlayerController::attackRight1State(float delta){
    //attack animation starts
    if(prev_ticks == 0){
        prev_ticks = GetTickCount();
    }
    
    DWORD new_ticks = GetTickCount();
    float elapsed = ( new_ticks - prev_ticks ) / 1000.0f;

    //elapsed percent of the attack (in range [0,1])
    float elapsedPercent = elapsed / PLAYER_ATTACK_RIGHT1_DURATION_SECS;

    //damage starts
    if(!projectileCreated && elapsed > PLAYER_ATTACK_RIGHT1_HIT_DELAY_SECS){
        projectileCreated = true;
        
        //Create the projectile of the attack and add it to the projectile manager
        CProjectile *projectile = new CProjectile;
        projectile->create(player, PLAYER_ATTACK_RIGHT1_HIT_DURATION_SECS, PLAYER_PROJECTILE_RADIOUS, NORMAL,PLAYER_ATTACK_RIGHT1_DAMAGE,0.f);
        CProjectileManager::getInstance()->addProjectile(projectile);
    }

    //combo window
    if(elapsedPercent > 0.5 && elapsedPercent < 1){
        if(io.becomesPressed(io.MOUSE_LEFT) && io.isPressed(io.DIGITAL_RIGHT)){
            prev_ticks = 0;
            projectileCreated = false;
            changeState("attackRight2");
        }
    }

    //attack ends
    if(elapsed > PLAYER_ATTACK_RIGHT1_DURATION_SECS){
        prev_ticks = 0;
        projectileCreated = false;
    changeState("idle_war");
    }
}


void CPlayerController::attackRight2State(float delta){
    if(prev_ticks == 0){
        prev_ticks = GetTickCount();
    }
    
    DWORD new_ticks = GetTickCount();
    float elapsed = ( new_ticks - prev_ticks ) / 1000.0f;

    if(!projectileCreated && elapsed > PLAYER_ATTACK_RIGHT2_HIT_DELAY_SECS){
        projectileCreated = true;
        //Create the projectile of the attack and add it to the projectile manager
        CProjectile *projectile = new CProjectile;
        projectile->create(player, PLAYER_ATTACK_RIGHT2_HIT_DURATION_SECS, PLAYER_PROJECTILE_RADIOUS, NORMAL, PLAYER_ATTACK_RIGHT2_DAMAGE,0.f);
        CProjectileManager::getInstance()->addProjectile(projectile);
    }

    if(elapsed > PLAYER_ATTACK_RIGHT2_DURATION_SECS){
        prev_ticks = 0;
        projectileCreated = false;
        changeState("idle_war");
    }
}


void CPlayerController::attackUp1State(float delta){
        //attack animation starts
    if(prev_ticks == 0){
        prev_ticks = GetTickCount();
    }
    
    DWORD new_ticks = GetTickCount();
    float elapsed = ( new_ticks - prev_ticks ) / 1000.0f;

    //elapsed percent of the attack (in range [0,1])
    float elapsedPercent = elapsed / PLAYER_ATTACK_UP1_DURATION_SECS;

    //damage starts
    if(!projectileCreated && elapsed > PLAYER_ATTACK_UP1_HIT_DELAY_SECS){
        projectileCreated = true;
        
        //Create the projectile of the attack and add it to the projectile manager
        CProjectile *projectile = new CProjectile;
        projectile->create(player, PLAYER_ATTACK_UP1_HIT_DURATION_SECS, PLAYER_PROJECTILE_RADIOUS, NORMAL,PLAYER_ATTACK_UP1_DAMAGE,0.f);
        CProjectileManager::getInstance()->addProjectile(projectile);
    }

    //combo window
    if(elapsedPercent > 0.5 && elapsedPercent < 1){
        if(io.becomesPressed(io.MOUSE_LEFT) && io.isPressed(io.DIGITAL_UP)){
            prev_ticks = 0;
            projectileCreated = false;
            changeState("attackUp2");
        }
    }

    //attack ends
    if(elapsed > PLAYER_ATTACK_UP1_DURATION_SECS){
        prev_ticks = 0;
        projectileCreated = false;
    changeState("idle_war");
    }
}

void CPlayerController::attackUp2State(float delta){
        if(prev_ticks == 0){
        prev_ticks = GetTickCount();
    }
    
    DWORD new_ticks = GetTickCount();
    float elapsed = ( new_ticks - prev_ticks ) / 1000.0f;

    if(!projectileCreated && elapsed > PLAYER_ATTACK_UP2_HIT_DELAY_SECS){
        projectileCreated = true;
        //Create the projectile of the attack and add it to the projectile manager
        CProjectile *projectile = new CProjectile;
        projectile->create(player, PLAYER_ATTACK_UP2_HIT_DURATION_SECS, PLAYER_PROJECTILE_RADIOUS, NORMAL,PLAYER_ATTACK_UP2_DAMAGE,0.f);
        CProjectileManager::getInstance()->addProjectile(projectile);
    }

    if(elapsed > PLAYER_ATTACK_UP2_DURATION_SECS){
        prev_ticks = 0;
        projectileCreated = false;
        changeState("idle_war");
    }
}

void CPlayerController::attackDown1State(float delta){
        //attack animation starts
    if(prev_ticks == 0){
        prev_ticks = GetTickCount();
    }
    
    DWORD new_ticks = GetTickCount();
    float elapsed = ( new_ticks - prev_ticks ) / 1000.0f;

    //elapsed percent of the attack (in range [0,1])
    float elapsedPercent = elapsed / PLAYER_ATTACK_DOWN1_DURATION_SECS;

    //damage starts
    if(!projectileCreated && elapsed > PLAYER_ATTACK_DOWN1_HIT_DELAY_SECS){
        projectileCreated = true;
        
        //Create the projectile of the attack and add it to the projectile manager
        CProjectile *projectile = new CProjectile;
        projectile->create(player, PLAYER_ATTACK_DOWN1_HIT_DURATION_SECS, PLAYER_PROJECTILE_RADIOUS, NORMAL, PLAYER_ATTACK_DOWN1_DAMAGE,0.f);
        CProjectileManager::getInstance()->addProjectile(projectile);
    }

    //combo window
    if(elapsedPercent > 0.5 && elapsedPercent < 1 && io.isPressed(io.DIGITAL_DOWN)){
        if(io.becomesPressed(io.MOUSE_LEFT)){
            prev_ticks = 0;
            projectileCreated = false;
            changeState("attackDown2");
        }
    }

    //attack ends
    if(elapsed > PLAYER_ATTACK_DOWN1_DURATION_SECS){
        prev_ticks = 0;
        projectileCreated = false;
    changeState("idle_war");
    }
}


void CPlayerController::attackDown2State(float delta){
    if(prev_ticks == 0){
        prev_ticks = GetTickCount();
    }
    
    DWORD new_ticks = GetTickCount();
    float elapsed = ( new_ticks - prev_ticks ) / 1000.0f;

    if(!projectileCreated && elapsed > PLAYER_ATTACK_DOWN2_HIT_DELAY_SECS){
        projectileCreated = true;
        //Create the projectile of the attack and add it to the projectile manager
        CProjectile *projectile = new CProjectile;
        projectile->create(player, PLAYER_ATTACK_DOWN2_HIT_DURATION_SECS, PLAYER_PROJECTILE_RADIOUS, NORMAL, PLAYER_ATTACK_DOWN2_DAMAGE,0.f);
        CProjectileManager::getInstance()->addProjectile(projectile);
}

    if(elapsed > PLAYER_ATTACK_DOWN2_DURATION_SECS){
        prev_ticks = 0;
        projectileCreated = false;
        changeState("idle_war");
    }
}

void CPlayerController::basicAttackState(float delta){
    if(prev_ticks == 0){
        prev_ticks = GetTickCount();
    }
    
    DWORD new_ticks = GetTickCount();
    float elapsed = ( new_ticks - prev_ticks ) / 1000.0f;

    if(!projectileCreated && elapsed > PLAYER_ATTACK_BASIC_HIT_DELAY_SECS){
        projectileCreated = true;
        //Create the projectile of the attack and add it to the projectile manager
        CProjectile *projectile = new CProjectile;
        projectile->create(player, PLAYER_ATTACK_BASIC_HIT_DURATION_SECS, PLAYER_PROJECTILE_RADIOUS, NORMAL, PLAYER_ATTACK_BASIC_DAMAGE,0.f);
        CProjectileManager::getInstance()->addProjectile(projectile);
    }

    if(elapsed > PLAYER_ATTACK_BASIC_DURATION_SECS){
        prev_ticks = 0;
        projectileCreated = false;
        changeState("idle_war");
    }
}

void CPlayerController::processPushState(float delta){
    if(prev_ticks == 0){
        prev_ticks = GetTickCount();
    }
    
    DWORD new_ticks = GetTickCount();
    float elapsed = ( new_ticks - prev_ticks ) / 1000.0f;
        
    //interpolate init and final positions 
    float elapsedPercent = elapsed / PLAYER_PUSHED_DURATION_SECS;
    player->setPosition( (1-elapsedPercent)*initLerpPos + elapsedPercent*finalLerpPos );


    if(elapsed > PLAYER_PUSHED_DURATION_SECS){
        prev_ticks = 0;

        //state transitions--------
        if(targetedEnemy != NULL){
            changeState("idle_war");
        }else{
            changeState("idle");
        }
    }

}

void CPlayerController::processShieldBreakState(float delta){
    if(prev_ticks == 0){
        prev_ticks = GetTickCount();
    }
    
    DWORD new_ticks = GetTickCount();
    float elapsed = ( new_ticks - prev_ticks ) / 1000.0f;

    if(elapsed > PLAYER_SHIELD_BREAK_DURATION_SECS){
        prev_ticks = 0;

        //state transitions--------
        if(targetedEnemy != NULL){
            changeState("idle_war");
        }else{
            changeState("idle");
        }
    }
}

//***************************//
//****** UTIL FUNCTIONS *****//
//***************************//

/**
  * Evaluate enemy distances to mark them as targetable or not targetable. Then choose the enemy who is going to be targeted next
  */
bool CPlayerController::targetEnemyEvaluation(){
        
        vector<bt*> toRemove;

        vector<bt *> aicontrollers = CAiManager::getInstance()->getCurrentZoneController();
        vector<bt *>::const_iterator i  = aicontrollers.begin();

        while( i != aicontrollers.end() ) {

            bt *enemy = (*i);

            if(enemy->isDead()){
                if( enemy->isTargetable()){
                    toRemove.push_back( enemy );				
                    enemy->setTargetable(false);
                }
                if(enemy->isTargeted()){
                    targetedEnemy = NULL;
                    enemy->setTargeted(false);
                    enemy->getOwner()->removeMesh("target");
                }
            }else{

                //compute distance to enemy
                XMVECTOR distVector = enemy->getPosition() - player->pos;
                float estimatedDistance = XMVectorGetX( XMVector3LengthEst(distVector) );
            

                //if distance to current enemy is smaller than TARGET_DISTANCE add enemy to targetableEnemies
                if(estimatedDistance < PLAYER_TARGET_DISTANCE){
                

                    //if was not already targetable add it to the targetable vector
                    if( !enemy->isTargetable() ){
                        targetableEnemies.push_back(enemy);
                        enemy->setTargetable(true);
                    }
                
                }else if(estimatedDistance > PLAYER_UNTARGET_DISTANCE){
                    //if was targetable and is not anymore, and is not currently targeted ->  mark to remove
                    if( enemy->isTargetable() && !enemy->isTargeted()){
                        toRemove.push_back( enemy );
                        enemy->setTargetable(false);
                    }
                }
            }

            ++i;
        }

        //remove all not targetable enemies from targetable enemies vector
        if(!toRemove.empty()){
            vector<bt*>::const_iterator removeIt = toRemove.begin();
            while( removeIt != toRemove.end() ) {

                vector<bt*>::iterator currentRemove = std::find(targetableEnemies.begin(), targetableEnemies.end(), *removeIt);
                targetableEnemies.erase(currentRemove);
                ++removeIt;
            }
        }

        //return false if no close enemies
        if( targetableEnemies.empty() ){
            return false;
        }
        
        //if no enemy already targeted, target first enemy in range
        if(targetedEnemy == NULL){
            targetEnemy(*targetableEnemies.begin());
        }else{
            //else target next in the vector of targetable enemies

            //retrieve index of targetedEnemy
            std::vector<bt*>::iterator targetedEnemyIndex = std::find(targetableEnemies.begin(), targetableEnemies.end(), targetedEnemy);
        
            //point to the next enemy in the vector
            ++targetedEnemyIndex;

            //update targetedEnemy
            if(targetedEnemyIndex - targetableEnemies.begin() >= targetableEnemies.size()){
                targetEnemy(*targetableEnemies.begin());
            }else{
                targetEnemy(*targetedEnemyIndex);
            }
        }

        
        //say to camera Controller to init a interpolation
        //calculate final interpolation position
        XMVECTOR enemyPosition = targetedEnemy->getPosition();
        XMVECTOR playerPosition = player->getPosition();
        float enemyToPlayerYaw = getYawFromVector( playerPosition - enemyPosition );

        XMVECTOR finalTargetToCamera = getVectorFromYawPitch(enemyToPlayerYaw, CAMERA_COMBAT_DEFAULT_PITCH) * CAMERA_COMBAT_DISTANCE;
        //interpolate camera
        cam_combat_controller.initInterpolation(&camera_1, playerPosition + finalTargetToCamera);

        return true;

}

/**
 * Untarget any targeted enemy and target enemy passed by param
 */
void CPlayerController::targetEnemy(bt* enemy){

    if(targetedEnemy != NULL){
        targetedEnemy->setTargeted(false);
        targetedEnemy->getOwner()->removeMesh("target");
    }

    enemy->setTargeted(true);
    enemy->getOwner()->addMesh("target");
    targetedEnemy = enemy;

}

/**
* substracts life and change state if needed
*/
void CPlayerController::manageDamage(CProjectile *p){

    if(getState().compare("dead") == 0) return;

    if(getState().compare("block") != 0 && p->getType() != SHIELD_BREAKER){
        //simulate impact to the entity
        player->setPitch(deg2rad(-20.f));
        impactTime = GetTickCount();
    }

    XMVECTOR pushDirection;

    switch(p->getType()){

        case NORMAL:
            if(getState().compare("block") == 0){                
                //substract a percentage of the total damage
                applyDamage(p->getDamage() * PLAYER_BLOCKING_DAMAGE_MULTIPLIER);
            }else{
                //substract total damage
                applyDamage( p->getDamage() );
            }
            break;

        case PUSHER:
            /*if(getState().compare("block") == 0){
                //substract a percentage of the total damage
                applyDamage( p->getDamage() * PLAYER_BLOCKING_DAMAGE_MULTIPLIER );

                XMVECTOR pushDirection = XMVector3Normalize( player->pos - p->getOwner()->getPosition() );
                initLerpPos = player->pos;
                finalLerpPos = initLerpPos + pushDirection * PLAYER_PUSHED_DISTANCE;
                changeState("processPush");
            }else{*/
                //substract total damage
                applyDamage( p->getDamage() );

                pushDirection = XMVector3Normalize( player->pos - p->getOwner()->getPosition() );
                initLerpPos = player->pos;
                finalLerpPos = initLerpPos + pushDirection * p->getPushDistance();
                changeState("processPush");
            //}
            
            break;

        case SHIELD_BREAKER:
            if(getState().compare("block") == 0){                
                //substract a percentage of the total damage
                applyDamage( p->getDamage() * PLAYER_BLOCKING_DAMAGE_MULTIPLIER );

                changeState("processShieldBreak");
            }else{
                //substract total damage
                applyDamage( p->getDamage() );

                pushDirection = XMVector3Normalize( player->pos - p->getOwner()->getPosition() );
                initLerpPos = player->pos;
                finalLerpPos = initLerpPos + pushDirection * PLAYER_PUSHED_DISTANCE;
                changeState("processShieldBreak");
            }
            break;
    }

    if(isDead()){        
        changeState("dead");
    }
}


bool CPlayerController::isAttacking(){

    if( getState().compare("basicAttack") == 0 || 
        getState().compare("attackLeft1") == 0 || 
        getState().compare("attackLeft2") == 0 || 
        getState().compare("attackRight1") == 0 || 
        getState().compare("attackRight2") == 0 || 
        getState().compare("attackUp1") == 0 || 
        getState().compare("attackUp2") == 0 || 
        getState().compare("attackDown1") == 0 || 
        getState().compare("attackDown2") == 0 ||
        getState().compare("basicIdleAttack") == 0 ){
            return true;
    }

    return false;
}

/**
* Update the position of the player  
*/
void CPlayerController::updatePosition(XMVECTOR movementDisplacement, float delta){
    XMVECTOR collisionDisplacement = XMVectorSet(0.f, 0.f, 0.f, 0.f); 

    //Calculate the next position of the player
    XMVECTOR nextPosition = player->pos + movementDisplacement * delta; 

    //Is coolided with any enemies?
    collisionDisplacement = CAiManager::getInstance()->hasCollide( nextPosition, player, AVATAR_COLLISION_RADIOUS, movementDisplacement);

    //Calculate the next position of the player
    XMVECTOR totalDisplacement = collisionDisplacement * delta;
    player->pos = player->pos + totalDisplacement;
}
