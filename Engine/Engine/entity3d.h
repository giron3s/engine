#ifndef INC_ENTITY_3D_H_
#define INC_ENTITY_3D_H_

#include "Angular.h"
#include <string>
#include <map>

using namespace std; 

//Forward
class CRender;

class CEntity3D {
public:
  XMMATRIX     world;
  XMVECTOR     pos;
  float        yaw;
  float        pitch;
  //The entities meshes
  map<string, CRender* > meshes;

public:
  bool addMesh(string meshKey);
  bool removeMesh(string meshKey);

  void updateWorld();
  float getPitch() const { return pitch; }
  void setPitch( float new_pitch );
  float getYaw() const { return yaw; }
  void setYaw( float new_yaw );

  XMVECTOR getPosition( ) const { return pos; }
  void setPosition( XMVECTOR new_pos );

  CEntity3D( );
  XMVECTOR getFront() const { return getVectorFromYaw( yaw ); }
  XMVECTOR getLeft() const { return getVectorFromYaw( yaw + XM_PIDIV2 ); }

  float getAngleTo( XMVECTOR p) const;
  bool isInsideCone( XMVECTOR p, float cone_angle, float cone_distance );
  bool isInsideCircle(XMVECTOR p, float radius);
  bool isPerceived(XMVECTOR p, float visionConeAngle, float visionConeDistance, float hearRadius);

  bool isInFront( XMVECTOR p) const;
  bool isInLeft( XMVECTOR p) const;

  //Return with the map of the meshes
  map<string, CRender* > getMeshes() { return meshes; }
};
#endif