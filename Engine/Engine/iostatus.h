#ifndef INC_IO_STATUS_H_
#define INC_IO_STATUS_H_

class CIOStatus {

public:

  struct TButton {
    bool  is_pressed;
    bool  was_pressed;
    float time_pressed;
    float time_released;
    int   key;
    TButton( ) : is_pressed( false )
      , was_pressed( false )
      , time_pressed( 0.f )
      , time_released( 0.f )
    { }
    void setPressed( bool how, float elapsed );
  };

  enum TButtonID {
    CROSS
  , CIRCLE
  , TRIANGLE
  , SQUARE
  , RB
  , RT
  , LB
  , LT
  , DIGITAL_LEFT
  , DIGITAL_RIGHT
  , DIGITAL_UP
  , DIGITAL_DOWN
  , START
  , MOUSE_LEFT
  , MOUSE_RIGHT
  , MOUSE_MIDDLE
  , MOUSE_WHEEL_UP
  , MOUSE_WHEEL_DOWN
  , BUTTONS_COUNT
  };

  bool isPressed( TButtonID button_id ) const { return buttons[ button_id ].is_pressed; }
  bool isReleased( TButtonID button_id ) const { return !buttons[ button_id ].is_pressed; }
  bool becomesPressed( TButtonID button_id ) const { 
    return buttons[ button_id ].is_pressed && !buttons[ button_id ].was_pressed; 
  }
  bool becomesReleased( TButtonID button_id ) const { 
    return !buttons[ button_id ].is_pressed && buttons[ button_id ].was_pressed; 
  }
  float getTimePressed( TButtonID button_id ) const { return buttons[ button_id ].time_pressed; }
  float getTimeReleased( TButtonID button_id ) const { return buttons[ button_id ].time_released; }

  TButton* getButtons(){ return buttons; }

  // ---------------------------
  // Analog
  enum TJoystickID {
    LEFT_ANALOG
  , RIGHT_ANALOG
  , JOYSTICKS_COUNT
  };

  struct TJoystick {
    float x,y;
    TJoystick( ) : x(0.f), y(0.f) { }
  };

  TJoystick getJoystick( TJoystickID joy_id ) const;

  // ----------------------
  struct TMouse {
    int   screen_x;     // as given by windows in client space
    int   screen_y;
    float normalized_x;       // Vars normalized to screen -1..1
    float normalized_y;
    int   dx;
    int   dy;

    int   prev_x;
    int   prev_y;
    void update();
  };
  const TMouse &getMouse() const { return mouse; }

  void setMouseCoords( int x, int y );

  CIOStatus();
  void update( float elapsed );

protected:
  TButton   buttons[ BUTTONS_COUNT   ];
  TJoystick joys   [ JOYSTICKS_COUNT ];
  TMouse    mouse;
};

extern CIOStatus io;

/*
  if( io.becomesPressed( IO_CROSS ) ) {

  }

  if( io.button[ IO_LEFT_RIGHT ].value();

  if( io.button[ IO_CROSS ].becomesPressed() ) {
    // ...
  }
  if( io.keys[ 'A' ].becomesPressed() ) {
    // ...
  }

  */
#endif