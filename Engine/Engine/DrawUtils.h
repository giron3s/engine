#ifndef INC_DRAW_UTILS_H_
#define INC_DRAW_UTILS_H_

#include "Mesh.h"

class CCamera;

namespace DrawUtils {
  bool createConstantBuffers();
  CRenderMesh* createCircle(float radious);
  CRenderMesh* createPerceiveArea(float hearing_radius, float vision_angle, float vision_distance);
  CRenderMesh* createAxis();
  CRenderMesh* createCube(float size);
  CRenderMesh* createGrid(int nbigs, int nsmalls );
  CRenderMesh* createFrustum(const CCamera &frustumCam );
  void destroyDrawUtils( );
  void drawFrustum(const CCamera &frustumCam );
  void activateDefaultConstantBuffers( );
  void activateCamera( const CCamera &camera );
  void setWorldMatrix( const XMMATRIX new_world );  
}

// To be removed...
struct ConstantBufferCamera
{
  XMMATRIX mView;
  XMMATRIX mProjection;
};
extern ID3D11Buffer*           g_pConstantBufferCamera ;

struct ConstantBufferObject
{
  XMMATRIX mWorld;
};
extern ID3D11Buffer*           g_pConstantBufferObject ;
#endif
