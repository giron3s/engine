#include "McvPlatform.h"
#include <WindowsX.h>
#include "Utils.h"
#include "IoStatus.h"

//--------------------------------------------------------------------------------------
// Forward declarations
//--------------------------------------------------------------------------------------
LRESULT CALLBACK    WndProc( HWND, UINT, WPARAM, LPARAM );

//--------------------------------------------------------------------------------------
// Register class and create window
//--------------------------------------------------------------------------------------
bool InitWindow( CApplication &app, HINSTANCE hInstance, int nCmdShow )
{
  // Register class
  WNDCLASSEX wcex;
  wcex.cbSize = sizeof( WNDCLASSEX );
  wcex.style = CS_HREDRAW | CS_VREDRAW;
  wcex.lpfnWndProc = WndProc;
  wcex.cbClsExtra = 0;
  wcex.cbWndExtra = 0;
  wcex.hInstance = hInstance;
  wcex.hIcon = NULL; //LoadIcon( hInstance, ( LPCTSTR )IDI_TUTORIAL1 );
  wcex.hCursor = LoadCursor( NULL, IDC_ARROW );
  wcex.hbrBackground = ( HBRUSH )( COLOR_WINDOW + 1 );
  wcex.lpszMenuName = NULL;
  wcex.lpszClassName = "TutorialWindowClass";
  wcex.hIconSm = NULL; //LoadIcon( wcex.hInstance, ( LPCTSTR )IDI_TUTORIAL1 );
  if( !RegisterClassEx( &wcex ) )
    return false;

  // Create window
  RECT rc = { 0, 0, app.xres, app.yres };
  AdjustWindowRect( &rc, WS_OVERLAPPEDWINDOW, FALSE );
  app.hWnd = CreateWindow( "TutorialWindowClass", "Direct3D 11 Tutorial 4: 3D Spaces", WS_OVERLAPPEDWINDOW,
    CW_USEDEFAULT, CW_USEDEFAULT, rc.right - rc.left, rc.bottom - rc.top, NULL, NULL, hInstance,
    NULL );
  if( !app.hWnd )
    return false;

  ShowWindow( app.hWnd, nCmdShow );

  return true;
}

//--------------------------------------------------------------------------------------
// Called every time the application receives a message
//--------------------------------------------------------------------------------------
LRESULT CALLBACK WndProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
  PAINTSTRUCT ps;
  HDC hdc;

  switch( message )
  {
  case WM_PAINT:
    hdc = BeginPaint( hWnd, &ps );
    EndPaint( hWnd, &ps );
    break;

  case WM_DESTROY:
    PostQuitMessage( 0 );
    break;
     
  // --------------------------
  case WM_MOUSEHWHEEL:
      break;
  case WM_LBUTTONUP:
    //::ReleaseCapture( );
      io.getButtons()[io.MOUSE_LEFT].setPressed(false,0.f);
    break;
  case WM_LBUTTONDOWN:
    // Capturar el raton para que los eventos vengan a mi ventana
    // incluso cuando el raton esta fuera del area cliente
    //::SetCapture( hWnd );
      io.getButtons()[io.MOUSE_LEFT].setPressed(true,0.f);
    break;
  case WM_RBUTTONUP:
      io.getButtons()[io.MOUSE_RIGHT].setPressed(false,0.f);
    break;
  case WM_RBUTTONDOWN:
      io.getButtons()[io.MOUSE_RIGHT].setPressed(true,0.f);
    break;
  case WM_KILLFOCUS:
    App.has_focus = false;
    break;
  case WM_SETFOCUS:
    App.has_focus = true;
    break;

  default:
    return DefWindowProc( hWnd, message, wParam, lParam );
  }

    return 0;
}

//--------------------------------------------------------------------------------------
// Entry point to the program. Initializes everything and goes into a message processing 
// loop. Idle time is used to render the scene.
//--------------------------------------------------------------------------------------
int WINAPI wWinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow )
{
    UNREFERENCED_PARAMETER( hPrevInstance );
    UNREFERENCED_PARAMETER( lpCmdLine );
    App.loadConfig();
    if( InitWindow( App, hInstance, nCmdShow ) 
        &&
        App.createDevice() ) {
      App.mainLoop();
    }
    App.destroyDevice();
    return 0;
}
