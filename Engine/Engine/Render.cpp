#include "McvPlatform.h"
#include "Render.h"
#include "DrawUtils.h"
#include "Shader.h"
#include "Texture.h"

void CRender::init(RenderType meshType, CRenderMesh *mesh, CTexture *texture, CVertexShader *vs, CPixelShader *general_ps, CPixelShader *mask_ps ){
	this->meshType = meshType;
	this->mesh = mesh;
	this->vs = vs;
	this->general_ps = general_ps;
	this->mask_ps = mask_ps;
	this->texture = texture;
}
/**
* Render the mesh
*/
void CRender::render(XMMATRIX world, bool maskVision){
	assert( vs );
	assert( general_ps );
	assert( mask_ps );

	//Set the
	if(maskVision){
		vs->activate();
		mask_ps->activate();
	}
	else{
		vs->activate();
		general_ps->activate();
	}

	//If the mesh has a texture
	if(texture != NULL){
		App.immediateContext->PSSetShaderResources( 0, 1, &texture->texture_dx11 );
		App.immediateContext->PSSetSamplers( 0, 1, &CShader::sampler_linear );
	}

	//Set the world matrix
	DrawUtils::setWorldMatrix( world );

	//Render the object
	mesh->render();
}

/**
* Destroy the object
*/
void CRender::destroy(){
	mesh->destroy();
	texture->destroy();
}