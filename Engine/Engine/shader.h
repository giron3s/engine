#ifndef INC_SHADER_H_
#define INC_SHADER_H_

#include "McvPlatform.h"

class CVertexDeclaration;

// ------------------------------------------
class CShader {

public:

  static bool createSamplers( );
  static void destroySamplers( );

  static ID3D11SamplerState* sampler_linear;
protected:
};

// ------------------------------------------
class CVertexShader : public CShader {
public:
  ID3D11VertexShader*        vs;
  ID3D11InputLayout*         vertex_layout;
  const CVertexDeclaration*  vertex_decl;

  CVertexShader( );
  void destroy( );
  void activate( );
  bool compile( const char *filename
              , const char *entry_point
              , const CVertexDeclaration *vtx_decl
              );

  static CVertexShader *current;
};

// ------------------------------------------
class CPixelShader : public CShader {

public:
  ID3D11PixelShader*   ps;
  CPixelShader( );
  void destroy( );
  void activate( );
  bool compile( const char *filename
              , const char *entry_point
              );
};





#endif
