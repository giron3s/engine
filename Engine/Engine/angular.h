#ifndef INC_ANGULAR_H_
#define INC_ANGULAR_H_


  XMVECTOR getVectorFromYawPitch( float yaw, float pitch );
  XMVECTOR getVectorFromYaw( float yaw );

  void     getYawPitchFromVector( XMVECTOR v, float *yaw, float *pitch );
  
  float    getAngleDifferenceTo( XMVECTOR src, float yaw, XMVECTOR p);
  float    getYawFromVector( XMVECTOR v );
  
  float    deg2rad( float degrees );
  float    rad2deg( float radians );
  float    pixelsToRads( float pixels );

#endif