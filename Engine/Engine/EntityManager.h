#ifndef _ENTITY_MANAGER_
#define _ENTITY_MANAGER_

#include <vector>
using namespace std; 

class CEntity3D;

class CEntityManager{
private:
	//Instance of the class
	static CEntityManager* pInstance;

	//Vector of the entities
	vector<CEntity3D *> entities;

public:
	static CEntityManager* getInstance();
	CEntity3D* addEntity();
	void render();
	void destroy();
};
#endif