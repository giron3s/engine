#include "McvPlatform.h"
#include "Camera.h"
#include "DrawUtils.h"
#include "Angular.h"
#include "Utils.h"
#include "Font.h"
#include "IoStatus.h"
#include "CameraController.h"
#include "PlayerController.h"
#include "AiManager.h"
#include "AiConstants.h"
#include "GraphicsController.h"
#include "EntityManager.h"
#include "time.h"

//--------------------------------------------------------------------------------------
// Global Variables
//--------------------------------------------------------------------------------------

TFont                   font;


//--------------------------------------------------------------------------------------
bool CApplication::onDeviceCreated() {
    //set the random number generator seed
    srand (time(NULL));


    //Graphics classes initialization
    DrawUtils::createConstantBuffers();
    CGraphicsController::getInstance()->init();

    //Initialize the AI manager
    CAiManager::getInstance()->init(&camera_1);

    // Camera initialization
    /*XMVECTOR eye = XMVectorSet( 6.0f, 5.0f, 7.0f, 0.0f );
    XMVECTOR target = XMVectorSet( 0.0f, 0.0f, 0.0f, 0.0f );
    XMVECTOR eye_2 = XMVectorSet( -6.0f, 5.0f, -7.0f, 0.0f );

    camera_1.lookAt( eye, target );
    camera_2.lookAt( eye_2, target );*/
    camera_2.setProjectionParams(deg2rad(30.f),1.0f,2.f,10.f);
    camera_2.setCamType(DEV);
    CEntity3D *playerEntity = CAiManager::getInstance()->getPlayerController()->getOwner();
    cam_combat_controller.init(&camera_1, playerEntity);
    cam_follow_controller.init(&camera_1, playerEntity);



    font.create( );
    font.camera = &camera_1;

    dbg( "init scene ok\n" );
    return true;
}

//--------------------------------------------------------------------------------------
void CApplication::onDeviceDestroyed() {
    // Destroy the font
    font.destroy( );

    // Destroy the drawutils
    DrawUtils::destroyDrawUtils( );
    CGraphicsController::getInstance()->destroy();

    // Destroy the IA manager
    CAiManager::getInstance()->destroy();

    // Destroy the entityManager
    CEntityManager::getInstance()->destroy();
}

//--------------------------------------------------------------------------------------
void CApplication::onUpdate( float delta ) {

    
    //get player state to know which camera to set
    std::string playerState = CAiManager::getInstance()->getPlayerController()->getState();
    if(playerState.compare("idle_war") ==0 && camera_1.getCamType() != COMBAT){
        camera_1.setCamType(COMBAT);
        
        //calculate final interpolation position
        XMVECTOR enemyPosition = CAiManager::getInstance()->getPlayerController()->getTargetedEnemy()->getPosition();
        XMVECTOR playerPosition = CAiManager::getInstance()->getPlayerController()->getPosition();
        float enemyToPlayerYaw = getYawFromVector( playerPosition - enemyPosition );

        XMVECTOR finalTargetToCamera = getVectorFromYawPitch(enemyToPlayerYaw, CAMERA_COMBAT_DEFAULT_PITCH) * CAMERA_COMBAT_DISTANCE;
        //interpolate camera
        cam_combat_controller.initInterpolation(&camera_1, playerPosition + finalTargetToCamera);

    }else if(playerState.compare("idle") == 0 && camera_1.getCamType() != FOLLOW){
        camera_1.setCamType(FOLLOW);
        //cam_follow_controller.setInterpolating(true);
    }

    // Update the IA manager
    CAiManager::getInstance()->update(delta);

    // Update camera_1
    if(camera_1.getCamType() == DEV){
        cam_dev_controller.updateCamera(&camera_1,delta);
    }else if(camera_1.getCamType() == COMBAT){
        cam_combat_controller.updateCamera(&camera_1, delta);
    }else if(camera_1.getCamType() == FOLLOW){
        cam_follow_controller.updateCamera(&camera_1, delta);
    }

    // Update camera_2
    if(camera_2.getCamType() == DEV){
        cam_dev_controller.updateCamera(&camera_2,delta);
    }else if(camera_2.getCamType() == COMBAT){
        cam_combat_controller.updateCamera(&camera_2, delta);
    }else if(camera_2.getCamType() == FOLLOW){
        cam_follow_controller.updateCamera(&camera_2, delta);
    }


    // Update the IO
    io.update( delta );
}

//--------------------------------------------------------------------------------------
void CApplication::onRender( ) {

    // Clear the back buffer
    float ClearColor[4] = { 0.0f, 0.0f, 0.0f, 1.0f }; // red, green, blue, alpha
    App.immediateContext->ClearRenderTargetView( App.renderTargetView, ClearColor );
    App.immediateContext->ClearDepthStencilView( App.depthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0 );

    DrawUtils::activateCamera( camera_1 );
    DrawUtils::setWorldMatrix( XMMatrixIdentity() );
    DrawUtils::activateDefaultConstantBuffers( );


    //Draw the camera frustum
    //DrawUtils::drawFrustum(camera_2);

    //Render the entities
    CEntityManager::getInstance()->render();

    //print screen data
    font.size = 20.f;
    float y = 10.0f;
    y += font.printf( 10, y , "PLAYER STATE: %s", CAiManager::getInstance()->getPlayerController()->getState().c_str() );
    y += font.printf( 10, y , "PLAYER LIFE: %.1f", CAiManager::getInstance()->getPlayerController()->getLife() );
    
    font.size = 10.f;
    
    
    vector<bt*> enemies = CAiManager::getInstance()->getCurrentZoneController();
    int cont=0;
    for(vector<bt*>::const_iterator it = enemies.begin(); it != enemies.end(); it++){

        //FIX: this cast should not be done
        CBtEnemy* enemyIt = (CBtEnemy*)*it;
        //bt* enemyIt = *it;

        XMVECTOR font3DPositionLife = XMVectorSetY( enemyIt->getPosition(),2.35f);
        XMVECTOR font3DPositionState = XMVectorSetY( enemyIt->getPosition(),2.55f);
        XMVECTOR font3DPositionkickProb = XMVectorSetY( enemyIt->getPosition(),2.75f);
            
        font.print3D( font3DPositionLife , "LIFE: %.1f",enemyIt->getLife() );		

        if(!enemyIt->lastNode.empty()){
            font.print3D( font3DPositionState , "STATE: %s", enemyIt->lastNode.c_str());
        }else{
            font.print3D( font3DPositionState , "STATE: NULL");
        }
        font.print3D( font3DPositionkickProb , "KICK PROB: %.1f",enemyIt->getKickProb() );		

    }

    //print enemy waypoints
    font.size = 20.f;
    font.print3D(XMVectorSet( 0.f, 0.f, 0.0f, 0.f ),"WP1");    
    font.print3D(XMVectorSet( -7.f, 0.f, -7.0f, 0.f ),"WP2");
    font.print3D(XMVectorSet( -7.f, 0.f, 7.0f, 0.f ),"WP3");
    font.print3D(XMVectorSet( 7.f, 0.f, -7.0f, 0.f ),"WP4");
    font.print3D(XMVectorSet( 7.f, 0.f, 7.0f, 0.f ),"WP5");
    //debug insideCone
    /*XMVECTOR playerPos = CAiManager::getInstance()->getPlayerController()->getOwner()->getPosition();
    if(CAiManager::getInstance()->getCurrentZoneController().front()->getOwner()->isInsideCone(playerPos,ENEMY_VISION_ANGLE,ENEMY_VISION_DISTANCE) 
        || CAiManager::getInstance()->getCurrentZoneController().front()->getOwner()->isInsideCircle(playerPos,ENEMY_HEARING_RADIUS)){
        y += font.printf( 10, y , "PLAYER IN SIGHT");
    }*/
    

    //print game over
    if( CAiManager::getInstance()->getPlayerController()->isDead() ){
        font.size = 80.f;
        font.printf( App.xres/2 - 180, App.yres / 2 - 60 ,"YOU DIED");
    }

}
