#ifndef _AICONTROLLER_H
#define _AICONTROLLER_H

// ai controllers using maps to function pointers for easy access and scalability. 
// we put this here so you can assert from all controllers without including everywhere
#include <assert.h>	
#include <string>
#include <map>
#include "entity3d.h"
#include "Shader.h"
#include "Angular.h"
#include "Life.h"

using namespace std;

// states are a map to member function pointers, to 
// be defined on a derived class.
class CAiController;

typedef void (CAiController::*statehandler)(float delta); 

class CAiController : public CLife {

  string state;
  // the states, as maps to functions
  map<string,statehandler>statemap;


  protected:
	  CEntity3D*	player;
	  //To be removed (just to simulate impact)
	  DWORD impactTime;



  public:
    virtual void init();	              // resets the controller
    void changeState(string);	          // state we wish to go to
    void update(float delta);	                      // recompute behaviour
    void addState(string, statehandler);
    string getState(){ return state; }
    virtual void updatePosition(XMVECTOR, float) = 0;    // Update the position of the IA
    virtual XMVECTOR getPosition() const = 0;       // Return with the actual state of the iaController
    virtual CEntity3D* getOwner() const = 0;        // Return with the owner of the e
};

#endif