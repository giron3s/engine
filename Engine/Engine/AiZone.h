#ifndef _INC_AI_ZONE_
#define _INC_AI_ZONE_

#include "McvPlatform.h"
#include "DrawUtils.h"
#include "Shader.h"
#include <vector>

class bt;
using namespace std; 

class CAiZone{
private:
	vector<bt *> aiControllers;

public:
	CAiZone(){}
	void addController(bt *aiController);
	vector<bt *> getControllers() const { return aiControllers; }
	void update(float);
	void destroy();
};
#endif