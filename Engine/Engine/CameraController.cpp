#include "McvPlatform.h"
#include "CameraController.h"
#include "Camera.h"
#include "Entity3D.h"
#include "Utils.h"
#include "Angular.h"
#include "IoStatus.h"
#include "AiManager.h"
#include "PlayerController.h"

CCameraDevController		cam_dev_controller;
CCameraCombatController     cam_combat_controller;
CCameraFollowController		cam_follow_controller;


CCameraDevController::CCameraDevController() : unitsPerPixel(0.01f){}

/**
* Update del cotrolador "FREE" de la c�mara. Se llama desde el update general de la app. 
* Actualiza la posici�n y rotaci�n de la c�mara seg�n el input que le llega desde el rat�n.
*/
void CCameraDevController::updateCamera(CCamera *cam, float delta) {

  //Movimiento lineal adelante/atras (left_clic + right_click)
  if (io.isPressed(io.MOUSE_LEFT) && io.isPressed(io.MOUSE_RIGHT)) {
    cam->strafe( cam->getPosition() - cam->getFront() * io.getMouse().dy * unitsPerPixel );
  
  //Rotacion (left_click)
  } else if (io.isPressed(io.MOUSE_LEFT)) {

      //obtener yaw y pitch a partir del front de la c�mara
      float cam_yaw, cam_pitch;
      getYawPitchFromVector(cam->getFront(),&cam_yaw,&cam_pitch);

      //actualizamos yaw y pitch a partir del 'dx' y 'dy' del rat�n
      cam_yaw = cam_yaw - pixelsToRads( io.getMouse().dx );
      cam_pitch = cam_pitch - pixelsToRads( io.getMouse().dy );
        
      //controlar que el pitch no sale del rango [pi/2,-pi/2]
      if (cam_pitch < -XM_PIDIV2) { cam_pitch = -3.14f/2.f; }
      else if (cam_pitch > XM_PIDIV2) { cam_pitch = 3.14f/2.f; }

      //actualizamos el front con los nuevos yaw y pitch
      cam->setFront( getVectorFromYawPitch(cam_yaw,cam_pitch) );
    } 
    //Strafe vertical y lateral (right_click)
    else if (io.isPressed(io.MOUSE_RIGHT)) {

      XMVECTOR new_cam_pos = cam->getPosition();

      //actualizamos la posici�n en el plano Up-Left
      new_cam_pos = new_cam_pos - cam->getUp() * io.getMouse().dy * unitsPerPixel;
      new_cam_pos = new_cam_pos - cam->getLeft() * io.getMouse().dx * unitsPerPixel;
        
      cam->strafe(new_cam_pos);
    }
}

void CCameraDevController::initInterpolation(CCamera *cam, XMVECTOR finalLerpPosition){}
void CCameraDevController::interpolate(CCamera *cam){}

CCameraCombatController::CCameraCombatController(): yaw_to_target(3.14f), pitch_to_target(0.7853f){}

void CCameraCombatController::init(CCamera* cam,CEntity3D* entity){
  targetEntity = entity;
  interpolationInitialTicks = 0;

  yaw_to_target = targetEntity->getYaw() + 3.14f;
  pitch_to_target = CAMERA_COMBAT_DEFAULT_PITCH;
}

void CCameraCombatController::updateCamera(CCamera* cam, float delta) {

    if(interpolating){
        interpolate(cam);
    }else{
        /*if ( io.isPressed(io.MOUSE_LEFT) && io.isPressed(io.MOUSE_RIGHT)) {

          cam_distance = cam_distance + io.getMouse().dy * unitsPerPixel;
          if (cam_distance < 2.f ) cam_distance = 2.f;

        } *//*else if ( io.isPressed(io.MOUSE_LEFT) ) {

          yaw_to_target = yaw_to_target - pixelsToRads( io.getMouse().dx );
          pitch_to_target = pitch_to_target + pixelsToRads( io.getMouse().dy );
      
          if ( pitch_to_target > 1.57f ) { pitch_to_target = 1.57f; }
          if ( pitch_to_target < 0.f ) { pitch_to_target = 0.f; }
      
        }*/ //else {
        yaw_to_target = targetEntity->getYaw() + 3.14f;
        pitch_to_target = pitch_to_target + pixelsToRads( io.getMouse().dy );
      
        if( pitch_to_target > 1.57f ) { pitch_to_target = 1.57f; }
        if( pitch_to_target < 0.f ) { pitch_to_target = 0.f; }
        //}


        //update cam_pos from new pitch and yaw
        XMVECTOR target_to_cam = getVectorFromYawPitch(yaw_to_target,pitch_to_target);
        XMVECTOR new_cam_pos = targetEntity->getPosition() + target_to_cam * CAMERA_COMBAT_DISTANCE;

        XMVECTOR cameraTarget = XMVectorSetY(targetEntity->getPosition(),CAMERA_TARGET_Y);
        cam->lookAt(new_cam_pos , cameraTarget);
    }
}



void CCameraCombatController::initInterpolation(CCamera *cam, XMVECTOR finalLerpPosition){
    interpolating = true;
    interpolationInitialTicks = GetTickCount();
    this->initialLerpPosition = cam->getPosition();
    this->finalLerpPosition = finalLerpPosition;
}

void CCameraCombatController::interpolate(CCamera *cam){
    
    //update time counter
    DWORD new_ticks = GetTickCount();
    float elapsed = ( new_ticks - interpolationInitialTicks ) / 1000.0f;
    float elapsedPercent = elapsed / CAMERA_INTERPOLATION_TIME;

    if(elapsed >= CAMERA_INTERPOLATION_TIME){

        pitch_to_target = CAMERA_COMBAT_DEFAULT_PITCH;
        interpolationInitialTicks = 0;
        interpolating = false;
        return;
    }

    XMVECTOR currentPos = (1-elapsedPercent) * initialLerpPosition + (elapsedPercent) * finalLerpPosition;
    XMVECTOR cameraTarget = XMVectorSetY(targetEntity->getPosition(),CAMERA_TARGET_Y);
    cam->lookAt(currentPos,cameraTarget);

}


CCameraFollowController::CCameraFollowController(){}

/**
*	Initialize follow camera position
*/
void CCameraFollowController::init(CCamera* cam,CEntity3D *entity){
    interpolationInitialTicks = 0;
    targetEntity = entity;
    distanceToTargetXZ = 4.f;

    XMVECTOR cameraTarget = XMVectorSetY(targetEntity->getPosition(),CAMERA_TARGET_Y);
    XMVECTOR newPos = cameraTarget - targetEntity->getFront() * distanceToTargetXZ;
    cam->lookAt( XMVectorSetY(newPos,3.f) , cameraTarget);
}

/**
*	updates Camera position based on Follow Controller
*/
void CCameraFollowController::updateCamera(CCamera* cam, float delta) {

    //calculate target movement in left camera axis and update camera position in left axis to keep target in x = 0
    XMVECTOR targetInCameraCoords = XMVector3Transform( targetEntity->getPosition() , cam->getView() );
    XMVECTOR newCamPos = cam->getPosition() - cam->getLeft() * XMVectorGetX(targetInCameraCoords);

    
    //update camera position from mouse movement
    //---------------------------------------------
    XMVECTOR cameraTarget = XMVectorSetY(targetEntity->getPosition(),CAMERA_TARGET_Y);
    XMVECTOR targetToCam = newCamPos - cameraTarget;
    float distanceToTarget = XMVectorGetX( XMVector3Length(targetToCam));    
    
    float yawToTarget, pitchToTarget;
    getYawPitchFromVector(targetToCam, &yawToTarget, &pitchToTarget);

    //update pitch and yaw from mouse input
    yawToTarget = yawToTarget - pixelsToRads( io.getMouse().dx );
    pitchToTarget = pitchToTarget + pixelsToRads( io.getMouse().dy );
      
    if ( pitchToTarget > 1.f ) { pitchToTarget = 1.f; }
    if ( pitchToTarget < 0.f ) { pitchToTarget = 0.f; }
    
    //update cam_pos from new pitch and yaw
    targetToCam = getVectorFromYawPitch(yawToTarget,pitchToTarget);
    newCamPos = cameraTarget + targetToCam * distanceToTarget;
    
    //update camera position from player position FAR/NEAR
    //--------------------------------------------------------
    XMVECTOR targetToCamXZ = XMVectorSetY(targetToCam * distanceToTarget, 0.0f);
    distanceToTargetXZ = XMVectorGetX( XMVector3LengthEst(targetToCamXZ));
    
    if( distanceToTargetXZ < CAMERA_FOLLOW_MIN_TARGET_XZ_DISTANCE ){

        newCamPos = newCamPos + XMVector3Normalize(targetToCamXZ) * (CAMERA_FOLLOW_MIN_TARGET_XZ_DISTANCE - distanceToTargetXZ);

    }else if( distanceToTargetXZ > CAMERA_FOLLOW_MAX_TARGET_XZ_DISTANCE ){

        newCamPos = newCamPos - XMVector3Normalize(targetToCamXZ) * (distanceToTargetXZ - CAMERA_FOLLOW_MAX_TARGET_XZ_DISTANCE);

    }else{
        newCamPos = newCamPos;
    }
    

    cam->lookAt( newCamPos , cameraTarget );

}

void CCameraFollowController::initInterpolation(CCamera *cam, XMVECTOR finalLerpPosition){}
void CCameraFollowController::interpolate(CCamera *cam){
    interpolating = false;
}
