#ifndef INC_MESH_H_
#define INC_MESH_H_

#include "McvPlatform.h"
class CVertexDeclaration;

class CRenderMesh {

public:

  //List of the primitive
  enum ePrimitiveType {
	  POINT_LIST , LINE_LIST , TRIANGLE_LIST , LINE_STRIP
  };

  CRenderMesh();
  //Render the mesh
  void render( ) const;
  //Load the mesh
  bool load( CDataProvider &dp );
  //Destroy the mesh
  void destroy( );

  typedef unsigned short TIndex;

  // Create a mesh given a vertex type
  template< class TVertex >
  bool createMesh( const TVertex* new_vertexs , unsigned new_nvertexs, const TIndex * new_indices
                 , unsigned       new_nindices, ePrimitiveType new_type) {
		
		return createRawMesh( new_vertexs , new_nvertexs, sizeof( TVertex ) , new_indices , new_nindices , new_type , TVertex::getDecl());
  }

  bool createRawMesh( const void *  new_vertexs , unsigned new_nvertexs, unsigned new_nbytes_per_vertex, const TIndex * new_indices
					 , unsigned    new_nindices , ePrimitiveType new_type, const CVertexDeclaration *avertex_decl);

private:
  // DirectX11 buffers
  ID3D11Buffer*             vb;
  ID3D11Buffer*             ib;
  D3D_PRIMITIVE_TOPOLOGY    primitive_type;
  const CVertexDeclaration* vertex_decl;

  unsigned                  nvertexs;
  unsigned                  nindices;
  unsigned                  bytes_per_vertex;

  // Declarado private para que no se pueda usar
  CRenderMesh( const CRenderMesh & );


  // -----------------------
  struct THeader {
    unsigned magic_begin;
    unsigned version;
    unsigned nvertexs;
    unsigned nfaces;
    unsigned nindices;
    unsigned vertex_type;
    unsigned bytes_per_vertex;
    unsigned bytes_per_index;
    unsigned magic_end;

    static const unsigned valid_magic = 0x33887755;
    static const unsigned current_version = 1;

    bool isValid() const {
      return magic_begin == valid_magic
          && magic_end == valid_magic
          && version == current_version;
    }
  };

};


#endif