#ifndef _RENDER_
#define _RENDER_

enum RenderType{
	PROJECTILE, PERCEIVE_AREA, NORMAL_DYNAMIC_MESH, NORMAL_STATIC_MESH, HIDDEN_STATIC_MESH
};

class CRenderMesh;
class CVertexShader;
class CPixelShader;
class CTexture;

class CRender{
private:
	//Type of the mesh
	RenderType     meshType;
	//Renderable mesh
	CRenderMesh    *mesh;
	//texture 
	CTexture       *texture;

	//Vertexshader and pixelshader
	CVertexShader  *vs;
	CPixelShader   *general_ps;
	CPixelShader   *mask_ps;

public:
	void init(RenderType meshType, CRenderMesh *mesh, CTexture *texture, CVertexShader *vs, CPixelShader *general_ps, CPixelShader *mask_ps );
	RenderType getMeshType() { return meshType; }
	void render(XMMATRIX world, bool maskVision);
	void destroy();
};
#endif