#ifndef _BT_ENEMY_BOSS_INC
#define _BT_ENEMY_BOSS_INC

#include "Bt.h"
#include "Entity3D.h"

class CProjectile;

class CBtEnemyBoss : public bt {
private:
  
  XMVECTOR initLerpPos, finalLerpPos;
  XMVECTOR pushDirection;

  bool projectileCreated;
  CEntity3D*   playerEntity;
  
  DWORD        prev_ticks;

public:
    void create(string enemyName, CEntity3D* owner, CEntity3D* playerEntity);
    
    bool conditionDamage();
    bool conditionDead();
    int actionDead(float delta);
    int actionImpact(float delta);
	int actionEscape(float delta);

	bool conditionIdle();
	int actionTaunt(float delta);
	int actionSpecialAttack(float delta);

    bool conditionFollowPlayer();
    int actionFollowPlayer(float delta);
    
	bool conditionCover();
	int actionCover(float delta);
	int actionIdleWar(float delta);

	int actionComboA1(float delta);
	int actionComboA3(float delta);
	int actionComboA2Uno(float delta);
	int actionComboA2Dos(float delta);
	int actionUncover(float delta);

    // Probability Function
    bool probabilityActionAreaCombat(bool zone);
    bool probabilityRest();
    bool probabilityCover();
    bool booleanProbability();
    bool detectPlayerAttack();
    std::vector<XMVECTOR> generateRandomPoint();


    //manage the damage/life
    void manageDamage(CProjectile *p);

	bool rotateToPoint(float delta,XMVECTOR point, int rotationMultiplier);

    //Update the position of the enemy 
    void updatePosition(XMVECTOR displacement, float delta); 
};

#endif