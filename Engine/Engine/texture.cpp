#include "McvPlatform.h"
#include "Texture.h"

CTexture::CTexture( )
  : texture_dx11( NULL )
{ }

void CTexture::destroy( ) {
  if( texture_dx11 )
    texture_dx11->Release(), texture_dx11 = NULL;
}

bool CTexture::load( const char *filename ) {
  assert( texture_dx11 == NULL );
  HRESULT hr = D3DX11CreateShaderResourceViewFromFile( App.d3dDevice, filename, NULL, NULL, &texture_dx11, NULL );
  if( FAILED( hr ) )
    return false;
  return true;
}

