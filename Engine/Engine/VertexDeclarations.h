#ifndef INC_VERTEX_DECLARATIONS_H_
#define INC_VERTEX_DECLARATIONS_H_

// ----------------------------------------
class CVertexDeclaration {
public:
  const D3D11_INPUT_ELEMENT_DESC *elems;
  unsigned                        nelems;
  CVertexDeclaration( const D3D11_INPUT_ELEMENT_DESC *desc, unsigned n );
};

extern CVertexDeclaration vtx_dcl_solid;
extern CVertexDeclaration vtx_dcl_textured;

// ----------------------------------------
struct CVertexSolid
{  
  XMFLOAT3 Pos;
  XMFLOAT4 Color;
  static const CVertexDeclaration *getDecl() { return &vtx_dcl_solid; }
};

struct CVertexTextured
{  
  XMFLOAT3 Pos;
  XMFLOAT2 Tex;
  static const CVertexDeclaration *getDecl() { return &vtx_dcl_textured; }
};

#endif
