#ifndef INC_MCV_APPLICATION_H_
#define INC_MCV_APPLICATION_H_

class CApplication {

public:
  int                     xres;
  int                     yres;
  //bool                    full_screen;
  bool                    has_focus;
  HWND                    hWnd;
  HINSTANCE               hInst;
  D3D_DRIVER_TYPE         driverType;
  D3D_FEATURE_LEVEL       featureLevel;
  ID3D11Device*           d3dDevice;
  ID3D11DeviceContext*    immediateContext;
  IDXGISwapChain*         swapChain;
  ID3D11RenderTargetView* renderTargetView;
  ID3D11DepthStencilView* depthStencilView;
  ID3D11Texture2D*        depthStencil;

  DWORD                   prev_ticks;

  CApplication();
  virtual ~CApplication() { }

  void loadConfig();
  bool createDevice();
  void destroyDevice();
  void mainLoop();
  
  void renderFrame();
  void updateFrame( );

  bool onDeviceCreated();
  void onUpdate( float delta );
  void onRender();
  void onDeviceDestroyed();

};

//#define App   CApplication::get()
extern CApplication App;

#endif
