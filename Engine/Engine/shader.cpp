#include "McvPlatform.h"
#include "Shader.h"
#include "VertexDeclarations.h"

CVertexShader *CVertexShader::current = NULL;

//--------------------------------------------------------------------------------------
// Helper for compiling shaders with D3DX11
//--------------------------------------------------------------------------------------
HRESULT CompileShaderFromFile( const char* szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut )
{
    HRESULT hr = S_OK;

    DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
    // Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
    // Setting this flag improves the shader debugging experience, but still allows 
    // the shaders to be optimized and to run exactly the way they will run in 
    // the release configuration of this program.
    dwShaderFlags |= D3DCOMPILE_DEBUG;
#endif

    // Pasar el nombre del char a wchar
    //WCHAR wname[ MAX_PATH ];
    //mbstowcs( wname, szFileName, MAX_PATH );

    ID3DBlob* pErrorBlob;
    hr = D3DX11CompileFromFile( szFileName, NULL, NULL, szEntryPoint, szShaderModel, 
        dwShaderFlags, 0, NULL, ppBlobOut, &pErrorBlob, NULL );
    if( FAILED(hr) )
    {
        if( pErrorBlob != NULL )
            OutputDebugString( (char*)pErrorBlob->GetBufferPointer() );
        if( pErrorBlob ) pErrorBlob->Release();
        return hr;
    }
    if( pErrorBlob ) pErrorBlob->Release();

    return S_OK;
}

// -------------------------------------------
CVertexShader::CVertexShader( ) 
  : vs( NULL ) 
  , vertex_layout( NULL )
  , vertex_decl( NULL )
  { }

bool CVertexShader::compile( const char *filename
              , const char *entry_point
              , const CVertexDeclaration *avtx_decl
              ) {
  HRESULT hr;

  vertex_decl = avtx_decl;

  ID3DBlob* pVSBlob = NULL;
  hr = CompileShaderFromFile( filename, entry_point, "vs_4_0", &pVSBlob );
  if( FAILED( hr ) )
  {
    MessageBox( NULL,
      "The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", "Error", MB_OK );
    return false;
  }

  // Create the vertex shader
  hr = App.d3dDevice->CreateVertexShader( pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), NULL, &vs );
  if( FAILED( hr ) )
  {	
    pVSBlob->Release();
    return false;
  }

  // Create the input layout
  hr = App.d3dDevice->CreateInputLayout( vertex_decl->elems
                                       , vertex_decl->nelems
                                       , pVSBlob->GetBufferPointer(),
          pVSBlob->GetBufferSize(), &vertex_layout );
  pVSBlob->Release();
  if( FAILED( hr ) )
    return false;

  return true;
}
  
void CVertexShader::destroy( ) {
  if( vs ) {
    vs->Release();
    vs = NULL;
  }
  if( vertex_layout )
    vertex_layout->Release();
  vertex_layout = NULL;

}

void CVertexShader::activate( ) {
  assert( vertex_layout );
  assert( vs );
  App.immediateContext->IASetInputLayout( vertex_layout );
  App.immediateContext->VSSetShader( vs, NULL, 0 );
  
  current = this;
}

// -----------------------------
CPixelShader::CPixelShader( ){ }

void CPixelShader::activate( ) 
{
  App.immediateContext->PSSetShader( ps, NULL, 0 );
}

bool CPixelShader::compile( const char *filename
  , const char *entry_point
  ) {

    HRESULT hr;
  ID3DBlob* pVSBlob = NULL;


    // Compile the pixel shader
    ID3DBlob* pPSBlob = NULL;
    hr = CompileShaderFromFile( filename, entry_point, "ps_4_0", &pPSBlob );
    if( FAILED( hr ) )
    {
      MessageBox( NULL,
        "The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", "Error", MB_OK );
      return false;
    }

    // Create the pixel shaderCreatePixelShader
    hr = App.d3dDevice->CreatePixelShader( pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &ps );
    pPSBlob->Release();
    if( FAILED( hr ) )
      return false;

    return true;
}

void CPixelShader::destroy( ) {
  if( ps ) {
    ps->Release();
    ps = NULL;
  }
}

// ------------------------------------------
ID3D11SamplerState* CShader::sampler_linear = NULL;

bool CShader::createSamplers( ) {
  HRESULT  hr;
  // Create the sample state
  D3D11_SAMPLER_DESC sampDesc;
  ZeroMemory( &sampDesc, sizeof(sampDesc) );
  sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
  sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
  sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
  sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
  sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
  sampDesc.MinLOD = 0;
  sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
  hr = App.d3dDevice->CreateSamplerState( &sampDesc, &sampler_linear );
  if( FAILED( hr ) )
    return false;

  return true;
}

void CShader::destroySamplers( ) {
  if( sampler_linear )
    sampler_linear->Release(), sampler_linear = NULL;
}
