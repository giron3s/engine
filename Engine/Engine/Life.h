#ifndef _INC_LIFE_H_
#define _INC_LIFE_H_
 

class CLife{
	float life;

public:		
	void applyDamage(float damage);
	void setLife( float life );

	float getLife(){ return life; }
	bool isDead(){ return life<=0; }

};

#endif