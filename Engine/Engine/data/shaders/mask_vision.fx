Texture2D txDiffuse : register( t0 );
SamplerState samLinear : register( s0 );

//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
cbuffer ConstantBufferCamera : register( b0 )
{
    matrix View;
    matrix Projection;
}

cbuffer ConstantBufferObject : register( b1 )
{
    matrix World;
}

//--------------------------------------------------------------------------------------
struct VS_MASK_OUTPUT
{
    float4 Pos : SV_POSITION;
    float4 Color : COLOR0;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_MASK_OUTPUT vs_mask_vision( float4 Pos : POSITION, float4 Color : COLOR ){
    VS_MASK_OUTPUT output = (VS_MASK_OUTPUT)0;
    output.Pos = mul( Pos, World );
    output.Pos = mul( output.Pos, View );
    output.Pos = mul( output.Pos, Projection );
    output.Color = Color;
    return output;
}


//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 ps_mask_vision( VS_MASK_OUTPUT input ) : SV_Target
{
	float4 textureColor = float4(1.0f,1.0f,0.0f,1.0f);
    return input.Color * textureColor;
}




//--------------------------------------------------------------------------------------
struct VS_INPUT_TEXTURED
{
    float4 Pos : POSITION;
    float2 Tex : TEXCOORD0;
};

struct PS_INPUT_TEXTURED
{
    float4 Pos : SV_POSITION;
    float2 Tex : TEXCOORD0;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
PS_INPUT_TEXTURED vs_textured_mask_vision( VS_INPUT_TEXTURED input )
{
    PS_INPUT_TEXTURED output = (PS_INPUT_TEXTURED)0;
    output.Pos = mul( input.Pos, World );
    output.Pos = mul( output.Pos, View );
    output.Pos = mul( output.Pos, Projection );
    output.Tex = input.Tex;
    
    return output;
}


//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 ps_textured_mask_vision( PS_INPUT_TEXTURED input) : SV_Target
{
	float4 textureColor = float4(1.0f,1.0f,0.0f,1.0f);
    return txDiffuse.Sample( samLinear, input.Tex ) * textureColor;
}
