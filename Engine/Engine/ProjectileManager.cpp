#include "McvPlatform.h"
#include "ProjectileManager.h"

//Initialize the instance pointer
CProjectileManager* CProjectileManager::pinstance = 0;

/**
 *  Get the instance of the projectile manager
 */
CProjectileManager* CProjectileManager::getInstance (){
  //First call, then create the instance
  if (pinstance == 0){
    pinstance = new CProjectileManager;
  }
  return pinstance;
}

/**
 * Add the new projectile to the list
 */
void CProjectileManager::addProjectile( CProjectile *projectile ) {
    projectiles.push_back( projectile );
}


/**
 * Update the projectile
 */
void CProjectileManager::update() {

  for(size_t i = 0; i < projectiles.size(); i++){
      CProjectile *projectile = projectiles.at(i);
      projectile->update();
  }
  removeProjectile( );
}


/**
 * Render the projectiles
 */
void CProjectileManager::render( ) {

  for(size_t i = 0; i < projectiles.size(); i++){
      CProjectile *projectile = projectiles.at(i);
      projectile->render();
  }
}

/**
 * Remove the projectile from the list
 */
void CProjectileManager::removeProjectile( ) {

    for(size_t i = 0; i < projectiles.size(); i++){
        //Remove those projectiles which not are active
        if(!projectiles.at(i)->isActive()){
            CProjectile* projectile = projectiles.at(i);
            projectile->destroy();
            projectiles.erase( projectiles.begin() + i );
        }
    }
}

/**
 *
 */
void CProjectileManager::destroy(){
    //Destroy the projectiles 
    for (size_t i = 0; i < projectiles.size(); i++ ) {
        delete projectiles[i];
    }
    projectiles.clear();

    //Destroy the instance
    pinstance = NULL;
    delete pinstance;
}


