#include "McvPlatform.h"
#include "entity3d.h"
#include "Render.h"
#include "Iostatus.h"
#include "EntityManager.h"
#include "GraphicsController.h"

//Initialize the instace
CEntityManager* CEntityManager::pInstance = 0;

/**
 * Return with the instance off the class
 */
CEntityManager* CEntityManager::getInstance(){
	if(pInstance == 0){
		pInstance = new CEntityManager;
	}
	return pInstance;
}


/**
 * Create the entity and add it to the list of the entities
 */
CEntity3D* CEntityManager::addEntity(){
	//Create the new entity
	CEntity3D *entity = new CEntity3D;

	//Save the new entity in the entityManager
	entities.push_back(entity);
	return entity;
}

/**
 * Render the list of the entities
 */
void CEntityManager::render(){
	//Mask is on/off
	bool maskVision = io.isPressed(io.RT); 

	//Get the entity's meshes
	vector<CEntity3D *>::iterator entityIter;
	for ( entityIter = entities.begin(); entityIter != entities.end();  ++entityIter) {
		CEntity3D *entity = ( *entityIter );
		map<string, CRender* > entityMeshes = entity->getMeshes();

		//Render the entity's mesh
		map<string, CRender* >::iterator mapIter;
		for (mapIter = entityMeshes.begin(); mapIter != entityMeshes.end(); ++mapIter){
			CRender* mesh = &( *mapIter->second );

			//Render the perceive areas
			if(mesh->getMeshType() == RenderType::PERCEIVE_AREA /*&& false*/){
				mesh->render(entity->world, maskVision);
			}
			//Render the dynamic meshes
			else if(mesh->getMeshType() == RenderType::NORMAL_DYNAMIC_MESH){
				mesh->render(entity->world, maskVision);
			}
			//Render the static meshes
			else if(mesh->getMeshType() == RenderType::NORMAL_STATIC_MESH){
				mesh->render(XMMatrixTranslationFromVector( entity->pos ), maskVision);
			}
			//Render the projectiles
			else if(mesh->getMeshType() == RenderType::PROJECTILE){
				mesh->render(XMMatrixTranslationFromVector( entity->pos ), maskVision);
			}
			//Render the static meshes
			else if(mesh->getMeshType() == RenderType::HIDDEN_STATIC_MESH && maskVision){
				mesh->render(entity->world, maskVision);
			}
		}
	}
}

void CEntityManager::destroy(){
	//Destroy the instance
	pInstance = NULL;
}