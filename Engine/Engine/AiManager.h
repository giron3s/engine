#ifndef _INC_AI_MANAGER_
#define _INC_AI_MANAGER_

#include "McvPlatform.h"
#include "Shader.h"
#include "Entity3d.h"
#include "BtEnemy.h"
#include "BtEnemyBoss.h"
#include <vector>
using namespace std; 

class CAiZone;
class CEntity3D;
class bt;
class CPlayerController;
class CProjectileManager;
class CCamera;

class CAiManager{
private:
	//Instance of the class
	static CAiManager* pInstance;

	//Player
	CPlayerController* player;
	//CComponent playerIa;

	//List of the zone
	vector <CAiZone *> iaZones;
	//CCompManager<CAi> enimigos;

	//Actual zone
	int currectZoneId; 

	//CEntity3D btEnemyEntity;

	CBtEnemy btEnemy;
	CBtEnemy btEnemy2;
	CBtEnemy btEnemy3;
	CBtEnemyBoss btEnemyBoss;

public:
	//TODO: chance to theprivate variable
	//CBtEnemy btEnemy;

	CAiManager();
	~CAiManager();
	static CAiManager* getInstance();

	void init(CCamera *);
	int  addZone();
	vector<bt *> getCurrentZoneController();
	CPlayerController* getPlayerController(){ return player; }
	void addAiControllerToZone(int zoneId, bt* iaController);
	XMVECTOR hasCollide(XMVECTOR nextEntityPosition, CEntity3D* entity, float collisionRadious, XMVECTOR collisionDisplacement);
	void update(float);
	void destroy();

private: 
	void computeHits();
	bool isCollided(XMVECTOR posEntity1, float radEntity1, XMVECTOR posEntity2, float radEntity2);
	XMVECTOR calculateCollisionVector(CEntity3D* entity1, XMVECTOR speedEntity1, CEntity3D* entity2);
};

#endif