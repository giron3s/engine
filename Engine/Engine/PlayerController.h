#ifndef PLAYER_CONTROLLER_H_
#define PLAYER_CONTROLLER_H_

#include "Font.h"
#include "AiConstants.h"
#include "AiController.h"
#include <vector>


class CEntity3D;
class CCamera;
class bt;
class CProjectile;

//TODO:
//colocar un font en esquina superior izquierda que indique el estado en el que estamos
class CPlayerController: public CAiController{
    private:


        DWORD				prev_ticks;
        bool				projectileCreated;
        bt*					targetedEnemy;
        CCamera*			cam;
        vector<bt*>         targetableEnemies;

        XMVECTOR initLerpPos, finalLerpPos;
        float initLerpAngle, finalLerpAngle;

    public:

        void deadState(float);

        void idleState(float);
        void walkState(float);
        void runState(float);

        //states from idle/walk
        //----------------------
        void basicIdleAttackState(float);
        void blockIdleState(float);
        void kickIdleState(float);
        void dodgeIdleState(float);
        

        void idleWarState(float);

        //states from idle_war
        //--------------------
        void orbitState(float);
        void blockState(float);
        void kickState(float);
        void dodgeState(float);

        void basicAttackState(float);

        void attackLeft1State(float);
        void attackLeft2State(float);
        void attackUp1State(float);
        void attackUp2State(float);
        void attackRight1State(float);
        void attackRight2State(float);
        void attackDown1State(float);
        void attackDown2State(float);
        
        //impact process states
        //---------------------
        void processPushState(float);
        void processShieldBreakState(float);


        //util functions
        //--------------
        void init(CEntity3D *pnj, CCamera *cam);
        bool targetEnemyEvaluation();
        void targetEnemy(bt* e);
        void manageDamage(CProjectile *p);
        
        bt* getTargetedEnemy(){ return targetedEnemy; }
        bool isAttacking();

        //Update the postion of the player
        void updatePosition(XMVECTOR movementDisplacement, float delta);

        // Return with the actual position 
        XMVECTOR getPosition() const { return player->pos; } 

        // Return with the owner
        CEntity3D* getOwner() const { return player; }
};


#endif