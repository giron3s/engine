#include "McvPlatform.h"
#include "Projectile.h"
#include "DrawUtils.h"

CProjectile::CProjectile(): projectileMesh(NULL), owner(NULL), active(false){

}

CProjectile::~CProjectile(){
}

/**
 *  Create the projectile
 */
void CProjectile::create(CEntity3D* owner, float ttl, float radious, ProjectileType type, float damage, float pushDistance){
	active = true;
	this->owner = owner;
	this->type = type;
	this->damage = damage;
	this->pushDistance = pushDistance;

	pos = XMVectorSet( 0,0,0,0 );
	projectileMesh = DrawUtils::createCircle(radious);

	this->ttl = ttl;					//Time to live
	this->radious = radious;			//Radious of the projectile
	birthTicks = GetTickCount();		//Birth time
}

/**
 *  Update the projectile
 */
void CProjectile::update(){
	//Calculate the livetime
	DWORD actualTicks = GetTickCount();
	float elapsed = ( actualTicks - birthTicks ) / 1000.0f;

	//If the projectile died, then set inactive
	if(ttl < elapsed){
		active = false;
	}
}

/**
 * Renderize the projectile
 */
void CProjectile::render(){

	ConstantBufferObject cb;
	cb.mWorld =  XMMatrixTranspose(XMMatrixTranslationFromVector( owner->getPosition() ));

	App.immediateContext->UpdateSubresource( g_pConstantBufferObject, 0, NULL, &cb, 0, 0 );
	projectileMesh->render();
}


/**
 *  Return if the point is inside the projectile or not
 */
bool CProjectile::isInside( XMVECTOR obj_position){
	bool isInside = false;

	//Calculate the distance between the two points
	XMVECTOR distance= owner->getPosition() - obj_position;
	XMVECTOR distanceLength =  XMVector3Length(distance);

	float lenght = XMVectorGetX(distanceLength);
	//If the absolute distance smaller than the 
	if(fabs(XMVectorGetX(distanceLength)) < radious){
		isInside = true;
	}
	return isInside;
}

/**
 * Destroy the object
 */
void CProjectile::destroy(){
	projectileMesh = NULL;
	owner = NULL;
}

//setters
void CProjectile::setActive(bool active){
	this->active = active;
}